<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Latest papers with code</title>
	<meta name="description" content="Recently papers with code and evaluation metrics" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="theme-color" content="#21cbce" />
	<link rel="manifest" href="/static/manifest.web.d401922cf4bd.json">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="/static/css/pwc.lib.a00c079aecd6.css" />
	<link rel="stylesheet" href="/static/js/jquery-ui-1.12.1/jquery-ui.min.b423dd67c9da.css" />
	<link rel="stylesheet" href="/static/css/style.e9a3b24151ee.css" />
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.8.1/css/all.css"
		integrity="sha384-Bx4pytHkyTDy3aJKjGkGoHPt3tvv6zlwwjc3iqN7ktaiEMLDPqLSZYts2OjKcBx1" crossorigin="anonymous">
	<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">


	<script type="text/x-mathjax-config">
		MathJax.Hub.Config({
  tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
});
</script>
	<script type="text/javascript" async
		src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML">
	</script>


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121182717-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-121182717-1');
	</script>
	<script>
		/**
		 * Function that captures a click on an outbound link in Analytics.
		 * This function takes a valid URL string as an argument, and uses that URL string
		 * as the event label. Setting the transport method to 'beacon' lets the hit be sent
		 * using 'navigator.sendBeacon' in browser that support it.
		 */
		var captureOutboundLink = function (url) {
			gtag('event', 'click', {
				'event_category': 'outbound',
				'event_label': url,

			});
		}
	</script>



	<!-- Open Graph protocol metadata -->
	<meta property="og:title" content="Latest papers with code">
	<meta property="og:description" content="Recently papers with code and evaluation metrics">
	<meta property="og:image" content="https://paperswithcode.com/static/index.jpeg">
	<meta property="og:url" content="http://paperswithcode.com/latest?page=2">



	<!-- Twitter metadata -->

	<meta name="twitter:card" content="summary_large_image">

	<meta name="twitter:site" content="@paperswithcode">
	<meta name="twitter:creator" content="@paperswithcode">
	<meta name="twitter:url" content="http://paperswithcode.com/latest?page=2">
	<meta name="twitter:domain" content="paperswithcode.com">
	<meta name="twitter:title" content="Latest papers with code">
	<meta name="twitter:description" content="Recently papers with code and evaluation metrics">

	<meta name="twitter:image:width" content="640">
	<meta name="twitter:image:height" content="320">
</head>

<body>



	<nav class="navbar navbar-expand-lg navbar-light bg-white header">
		<div class="header-logo">
			<a class="navbar-brand" href="/">
				<ion-icon name="barcode"></ion-icon>
			</a>
		</div>


		<div class="mobile-twitter">
			<a href="https://twitter.com/paperswithcode">
				<img src="/static/icons8-twitter-48.07f2def52776.png"
					style="height:30px;width:auto;position:relative;top:-1px;"></a>
			</a>
		</div>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse order-1" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item header-search">
					<form action="/search" method="get" id="id_global_search_form">
						<input type="text" name="q_meta" style="display:none" id="q_meta" />
						<input id="id_global_search_input" name='q' class="global-search" type="search"
							placeholder='Search for papers, code and tasks' />
						<button type="submit" class="icon"><img src="/static/query.bbb60654cba0.jpg"></button>
					</form>
				</li>

				<li class="nav-item">
					<div class="sign-up">
						<a href="/sota"><button class="btn-primary" style="position:relative;top:10px"><i
									class="fas fa-chart-line"></i> Browse state-of-the-art</button></a>
					</div>
				</li>



			</ul>
			<div class="order-3">
				<div class="navbar-nav">

					<ul class="navbar-nav ml-auto navbar-subscribe">



						<li class="nav-item nav-link-right"><a class="nav-link-right nav-textual-link"
								href="https://twitter.com/paperswithcode">
								<img src="/static/icons8-twitter-48.07f2def52776.png" class="navbar-twitter-icon">
								Follow</a>
						</li>

						<li class="nav-item nav-link-right"><a class="nav-link-right nav-textual-link"
								href="https://join.slack.com/t/paperswithcode/shared_invite/enQtNzE2NDQyMTAxNDEzLTMxNmY2NTc4ZWYzZGJhZGRmZGFkNzVhNTI1OTZhYzFlZWZiZDQ0M2M5ZTkyYzNhZmZhZmRlMjkxNGQxZGEwZjA">
								<img src="/static/slack-icon-min.fac6b299d18c.png" class="navbar-slack-icon">
								Discuss</a>
						</li>

						<li class="nav-item nav-link-right"><a class="nav-link-right nav-textual-link"
								style="position:relative;top:1px" href="/about">About</a></li>





						<li class="nav-item nav-link-right"><a class="nav-link-right nav-textual-link"
								style="position:relative;top:1px" href="/accounts/login/?next=/latest">Log
								In/Register</a></li>


					</ul>
				</div>
			</div>
		</div>


	</nav>

	<!-- Page modals -->
	<div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" id="emailModalLabel">Get the weekly digest</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="" method="post">
					<div class="modal-body">
						<div class="modal-body-info-text">
							You'll get the lates papers with code and state-of-the-art methods. <br><br />
							<span class="text-muted">
								Tip: you can also <a href="https://twitter.com/paperswithcode">follow us on <i
										class="fab fa-twitter" style="color:#00aced"></i> Twitter</a>
							</span>
						</div>

						<input type="hidden" name="csrfmiddlewaretoken"
							value="rY88PraRyvbS8hrGbs9ovo5sUU18lbRpWjW9fCQyyZj3tElCCtiS2I4Ajd4N6Hwy">
						<input placeholder="Enter your email" type="email" class="form-control pwc-email" name="address"
							id="id_address" max_length="100" required>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Subscribe</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Login -->
	<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="loginModalLabel">Join the community</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="login-modal-message">
					You need to <a href="/accounts/login/?next=/latest">log in</a> to edit.<br />
					You can <a href="/accounts/register?next=/latest">create a new account</a> if you don't have
					one.<br /><br />
					Or, discuss a change on <a
						href="https://join.slack.com/t/paperswithcode/shared_invite/enQtNTI3NDE2NjQ0ODM0LTdmNzNjODkwOGY0MjU4YzgzNDZhNGM1YWIzYmZhNzk5MTFkYWU4YWNjN2JjZDhlNjJiYjFkYjYwNjkzYzdiZDk"><i
							class="fab fa-slack"></i> Slack</a>.
				</div>
			</div>
		</div>
	</div>




	<div class="container">


		<div class="container content content-buffer">




			<div class="title">
				<div class="row">
					<div class="col-lg-6">
						<h1 class="home-page-title">Latest Research</h1>
					</div>
					<div class="col-lg-6">
						<div style="float: right;" class="btn-group btn-group-sm pull-right home-page-navigation"
							role="group" aria-label="Basic example">

							<a href="/" class="list-button">Trending</a>



							<a href="./latest" class="list-button-active">Latest</a>



							<a href="./greatest" class="list-button">Greatest</a>


							<span class="list-button-subscribe">
								<a href="" class="list-button" data-toggle="modal" data-target="#emailModal"><i
										class="far fa-envelope"></i> Subscribe</a>
							</span>
						</div>
					</div>
				</div>
			</div>


			<div class="infinite-container text-center">







				<div class="row infinite-item item">
					<!-- 634023 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/conv-mcd-a-plug-and-play-multi-task-module">
							<div class="item-image"
								style="background-image: url('/media/thumbnails/papergithubrepo/pgr-0000634023-a5af7569.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/conv-mcd-a-plug-and-play-multi-task-module">Conv-MCD: A
										Plug-and-Play Multi-task Module for Medical Image Segmentation</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">14 Aug 2019</span>




									• <a href="https://github.com/Bala93/Multi-task-deep-network"
										onclick="captureOutboundLink('https://github.com/Bala93/Multi-task-deep-network'); return true;"
										style="font-size:13px">Bala93/Multi-task-deep-network</a>





									• <img src="/static/frameworks/pytorch.28a00358044f.png">





									<p class="item-strip-abstract">For the task of medical image segmentation, fully
										convolutional network (FCN) based architectures have been extensively used with
										various modifications.</p>

									<div class="sota">

									</div>


									<p>

										<a href="/task/medical-image-segmentation"><span
												class="badge badge-primary">MEDICAL IMAGE SEGMENTATION</span></a>

										<a href="/task/multi-task-learning"><span class="badge badge-primary">MULTI-TASK
												LEARNING</span></a>

										<a href="/task/semantic-segmentation"><span class="badge badge-primary">SEMANTIC
												SEGMENTATION</span></a>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 4
									</span>

								</div>




								<div class="stars-accumulated text-center">
									14 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/conv-mcd-a-plug-and-play-multi-task-module"
										class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/conv-mcd-a-plug-and-play-multi-task-module#code"
										class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 633153 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/fairface-face-attribute-dataset-for-balanced">
							<div class="item-image" style="background-image: url('/static/thumbs/1908.04913.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/fairface-face-attribute-dataset-for-balanced">FairFace: Face
										Attribute Dataset for Balanced Race, Gender, and Age</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">14 Aug 2019</span>




									• <a href="https://github.com/joojs/fairface"
										onclick="captureOutboundLink('https://github.com/joojs/fairface'); return true;"
										style="font-size:13px">joojs/fairface</a>





									<p class="item-strip-abstract">Images were collected from the YFCC-100M Flickr
										dataset and labeled with race, gender, and age groups.</p>

									<div class="sota">

									</div>


									<p>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 41
									</span>

								</div>




								<div class="stars-accumulated text-center">
									14 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/fairface-face-attribute-dataset-for-balanced"
										class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/fairface-face-attribute-dataset-for-balanced#code"
										class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 633145 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/videonavqa-bridging-the-gap-between-visual">
							<div class="item-image" style="background-image: url('/static/thumbs/1908.04950.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/videonavqa-bridging-the-gap-between-visual">VideoNavQA: Bridging the
										Gap between Visual and Embodied Question Answering</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">14 Aug 2019</span>




									• <a href="https://github.com/catalina17/VideoNavQA"
										onclick="captureOutboundLink('https://github.com/catalina17/VideoNavQA'); return true;"
										style="font-size:13px">catalina17/VideoNavQA</a>





									<p class="item-strip-abstract">The goal of this dataset is to assess
										question-answering performance from nearly-ideal navigation paths, while
										considering a much more complete variety of questions than current
										instantiations of the EQA task.</p>

									<div class="sota">

									</div>


									<p>

										<a href="/task/embodied-question-answering"><span
												class="badge badge-primary">EMBODIED QUESTION ANSWERING</span></a>

										<a href="/task/question-answering"><span class="badge badge-primary">QUESTION
												ANSWERING</span></a>

										<a href="/task/scene-understanding"><span class="badge badge-primary">SCENE
												UNDERSTANDING</span></a>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 0
									</span>

								</div>




								<div class="stars-accumulated text-center">
									14 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/videonavqa-bridging-the-gap-between-visual"
										class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/videonavqa-bridging-the-gap-between-visual#code"
										class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 628928 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/reinforcement-learning-based-interconnection">
							<div class="item-image"
								style="background-image: url('/media/thumbnails/papergithubrepo/pgr-0000628928-b22ebaa0.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/reinforcement-learning-based-interconnection">Reinforcement Learning
										based Interconnection Routing for Adaptive Traffic Optimization</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">13 Aug 2019</span>




									• <a href="https://github.com/huckiyang/interconnect-routing-gym"
										onclick="captureOutboundLink('https://github.com/huckiyang/interconnect-routing-gym'); return true;"
										style="font-size:13px">huckiyang/interconnect-routing-gym</a>





									<p class="item-strip-abstract">In this work, we demonstrate the promise of applying
										reinforcement learning (RL) to optimize NoC runtime performance.</p>

									<div class="sota">

									</div>


									<p>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 1
									</span>

								</div>




								<div class="stars-accumulated text-center">
									13 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/reinforcement-learning-based-interconnection"
										class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/reinforcement-learning-based-interconnection#code"
										class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 628917 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/metric-learn-metric-learning-algorithms-in">
							<div class="item-image" style="background-image: url('/static/thumbs/1908.04710.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/metric-learn-metric-learning-algorithms-in">metric-learn: Metric
										Learning Algorithms in Python</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">13 Aug 2019</span>




									• <a href="https://github.com/scikit-learn-contrib/metric-learn"
										onclick="captureOutboundLink('https://github.com/scikit-learn-contrib/metric-learn'); return true;"
										style="font-size:13px">scikit-learn-contrib/metric-learn</a>





									<p class="item-strip-abstract">metric-learn is an open source Python package
										implementing supervised and weakly-supervised distance metric learning
										algorithms.</p>

									<div class="sota">

									</div>


									<p>

										<a href="/task/metric-learning"><span class="badge badge-primary">METRIC
												LEARNING</span></a>

										<a href="/task/model-selection"><span class="badge badge-primary">MODEL
												SELECTION</span></a>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 795
									</span>

								</div>




								<div class="stars-accumulated text-center">
									13 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/metric-learn-metric-learning-algorithms-in"
										class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/metric-learn-metric-learning-algorithms-in#code"
										class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 628923 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/attention-is-not-not-explanation">
							<div class="item-image" style="background-image: url('/static/thumbs/1908.04626.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/attention-is-not-not-explanation">Attention is not not
										Explanation</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">13 Aug 2019</span>




									• <a href="https://github.com/sarahwie/attention"
										onclick="captureOutboundLink('https://github.com/sarahwie/attention'); return true;"
										style="font-size:13px">sarahwie/attention</a>





									<p class="item-strip-abstract">We show that even when reliable adversarial
										distributions can be found, they don&#39;t perform well on the simple
										diagnostic, indicating that prior work does not disprove the usefulness of
										attention mechanisms for explainability.</p>

									<div class="sota">

									</div>


									<p>

										<a href="/task/calibration"><span
												class="badge badge-primary">CALIBRATION</span></a>

										<a href="/task/decision-making"><span class="badge badge-primary">DECISION
												MAKING</span></a>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 7
									</span>

								</div>




								<div class="stars-accumulated text-center">
									13 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/attention-is-not-not-explanation" class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/attention-is-not-not-explanation#code" class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 628924 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/easse-easier-automatic-sentence">
							<div class="item-image"
								style="background-image: url('/media/thumbnails/papergithubrepo/pgr-0000628924-e8604488.gif');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/easse-easier-automatic-sentence">EASSE: Easier Automatic Sentence
										Simplification Evaluation</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">13 Aug 2019</span>




									• <a href="https://github.com/feralvam/easse"
										onclick="captureOutboundLink('https://github.com/feralvam/easse'); return true;"
										style="font-size:13px">feralvam/easse</a>





									<p class="item-strip-abstract">We introduce EASSE, a Python package aiming to
										facilitate and standardise automatic evaluation and comparison of Sentence
										Simplification (SS) systems.</p>

									<div class="sota">

									</div>


									<p>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 7
									</span>

								</div>




								<div class="stars-accumulated text-center">
									13 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/easse-easier-automatic-sentence" class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/easse-easier-automatic-sentence#code" class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 628929 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/einconv-exploring-unexplored-tensor">
							<div class="item-image"
								style="background-image: url('/media/thumbnails/papergithubrepo/pgr-0000628929-3e98a017.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/einconv-exploring-unexplored-tensor">Einconv: Exploring Unexplored
										Tensor Decompositions for Convolutional Neural Networks</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">13 Aug 2019</span>




									• <a href="https://github.com/pfnet-research/einconv"
										onclick="captureOutboundLink('https://github.com/pfnet-research/einconv'); return true;"
										style="font-size:13px">pfnet-research/einconv</a>





									• <img src="/static/frameworks/pytorch.28a00358044f.png">





									<p class="item-strip-abstract">This raises the simple question of how many
										decompositions are possible, and which of these is the best.</p>

									<div class="sota">

									</div>


									<p>

										<a href="/task/model-compression"><span class="badge badge-primary">MODEL
												COMPRESSION</span></a>

										<a href="/task/architecture-search"><span class="badge badge-primary">NEURAL
												ARCHITECTURE SEARCH</span></a>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 4
									</span>

								</div>




								<div class="stars-accumulated text-center">
									13 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/einconv-exploring-unexplored-tensor" class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/einconv-exploring-unexplored-tensor#code" class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 626766 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/domain-specific-embedding-network-for-zero">
							<div class="item-image" style="background-image: url('/static/thumbs/1908.04174.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/domain-specific-embedding-network-for-zero">Domain-Specific
										Embedding Network for Zero-Shot Recognition</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">12 Aug 2019</span>




									• <a href="https://github.com/mboboGO/DSEN-for-GZSL"
										onclick="captureOutboundLink('https://github.com/mboboGO/DSEN-for-GZSL'); return true;"
										style="font-size:13px">mboboGO/DSEN-for-GZSL</a>





									• <img src="/static/frameworks/pytorch.28a00358044f.png">





									<p class="item-strip-abstract">In contrast to previous methods, the DSEN decomposes
										the domain-shared projection function into one domain-invariant and two
										domain-specific sub-functions to explore the similarities and differences
										between two domains.</p>

									<div class="sota">

									</div>


									<p>

										<a href="/task/zero-shot-learning"><span class="badge badge-primary">ZERO-SHOT
												LEARNING</span></a>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 3
									</span>

								</div>




								<div class="stars-accumulated text-center">
									12 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/domain-specific-embedding-network-for-zero"
										class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/domain-specific-embedding-network-for-zero#code"
										class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>







				<div class="row infinite-item item">
					<!-- 628936 -->


					<div class="col-lg-3 item-image-col">


						<a href="/paper/amazonqa-a-review-based-question-answering">
							<div class="item-image" style="background-image: url('/static/thumbs/1908.04364.jpg');">
							</div>
						</a>


					</div>


					<div class="col-lg-9 item-col">
						<div class="row">
							<div class="col-lg-9 item-content">

								<h1><a href="/paper/amazonqa-a-review-based-question-answering">AmazonQA: A Review-Based
										Question Answering Task</a></h1>


								<p style="padding-top:2px">

									<span class="author-name-text">12 Aug 2019</span>




									• <a href="https://github.com/amazonqa/amazonqa"
										onclick="captureOutboundLink('https://github.com/amazonqa/amazonqa'); return true;"
										style="font-size:13px">amazonqa/amazonqa</a>





									<p class="item-strip-abstract">Observing that many questions can be answered based
										upon the available product reviews, we propose the task of review-based QA.</p>

									<div class="sota">

									</div>


									<p>

										<a href="/task/information-retrieval"><span
												class="badge badge-primary">INFORMATION RETRIEVAL</span></a>

										<a href="/task/question-answering"><span class="badge badge-primary">QUESTION
												ANSWERING</span></a>

										<a href="/task/reading-comprehension"><span class="badge badge-primary">READING
												COMPREHENSION</span></a>

									</p>
							</div>

							<div class="col-lg-3 item-interact text-center">
								<div class="entity-stars">

									<span class="badge badge-secondary">
										<ion-icon name="star"></ion-icon> 10
									</span>

								</div>




								<div class="stars-accumulated text-center">
									12 Aug 2019
								</div>


								<div class="entity" style="margin-bottom: 20px;">

									<a href="/paper/amazonqa-a-review-based-question-answering"
										class="badge badge-light">
										<ion-icon name="document"></ion-icon> Paper
									</a>

									<br />



									<a href="/paper/amazonqa-a-review-based-question-answering#code"
										class="badge badge-dark">
										<ion-icon name="logo-github"></ion-icon> Code
									</a>

									<br />

								</div>
							</div>
						</div>
					</div>
				</div>



			</div>

			<div class="loading" style="display: none;">
				<div class="loader-ellips infinite-scroll-request">
					<span class="loader-ellips__dot"></span>
					<span class="loader-ellips__dot"></span>
					<span class="loader-ellips__dot"></span>
					<span class="loader-ellips__dot"></span>
				</div>
			</div>


			<a class="infinite-more-link" href="?page=3"></a>






		</div>

	</div>

	<div class="footer">
		Contact us on: <ion-icon name="mail" style="position:relative;top:2px"></ion-icon> <a
			href="mailto:hello@paperswithcode.com">hello@paperswithcode.com</a>. Papers With Code is a free resource
		supported by <a href="https://atlasml.io">Atlas ML</a>.
		<div class="legal-links">
			<a href="/site/terms">Terms</a>
			<a href="/site/privacy">Privacy</a>
			<a href="/site/cookies-policy">Cookies policy</a>
		</div>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://unpkg.com/ionicons@4.1.2/dist/ionicons.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="/static/js/pwc.lib.c937a4b7def8.js"></script>
	<script src="https://browser.sentry-cdn.com/4.6.4/bundle.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
		integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous">
	</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
		integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous">
	</script>



	<script>
		var infinite = new Waypoint.Infinite({
			element: $('.infinite-container')[0],
			onBeforePageLoad: function () {
				$('.loading').show();
			},
			onAfterPageLoad: function ($items) {
				$('.loading').hide();
			}
		});
	</script>




	<script>
		$(function () {
			$.widget("custom.catcomplete", $.ui.autocomplete, {
				_create: function () {
					this._super();
					this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
				},
				_renderMenu: function (ul, items) {
					var that = this,
						currentCategory = "";
					$.each(items, function (index, item) {
						var li;
						if (item.category != currentCategory) {
							ul.append("<li class='ui-autocomplete-category'>" + item.category +
								"</li>");
							currentCategory = item.category;
						}
						li = that._renderItemData(ul, item);
						if (item.meta !== null) {
							li.attr('data-qmeta', item.meta);
						}
						if (item.category) {
							li.attr("aria-label", item.category + " : " + item.label);
						}
					});
				}
			});

			$("#id_global_search_input").catcomplete({
				minLength: 2,
				source: function (request, response) {
					var term = request.term;

					$.get("/api/search-autocomplete/", {
						"q": term
					}, function (data) {
						let searchData = [];
						data.tasks.forEach((t) => {
							searchData.push({
								label: t,
								category: "Tasks",
								meta: null
							});
						});

						data.leaderboards.forEach((lb) => {
							searchData.push({
								label: lb.name,
								category: "Leaderboards",
								meta: lb.slug
							});
						});
						data.papers.forEach((p) => {
							searchData.push({
								label: p.title,
								category: "Papers",
								meta: null
							});
						});
						response(searchData);
					});
				},
				select: function (event, ui) {
					$("#id_global_search_input").val(ui.item.label);
					if (typeof gtag !== 'undefined') {
						gtag('event', 'SiteActions', {
							'event_category': 'Search',
							'event_label': ui.item.category,
						});
					}

					if (ui.item.meta === null) {
						$('#q_meta').val('');
						$('#q_meta').removeAttr('name');
					} else {
						if (!$('#q_meta').attr("name")) {
							$('#q_meta').attr('name', 'q_meta');
						}
						$('#q_meta').val(ui.item.meta);
					}
					$("#id_global_search_form").submit();
				}
			});

			if ($(window).width() < 1200 && $(window).width() > 992) {
				$("#id_global_search_input").attr("placeholder", "Search");
			}

			// Configure Sentry
			let sentryDsn = "https://be66a46136d24f1a998234ca50a0456a@sentry.io/1413936".trim();
			if (sentryDsn !== "") {
				Sentry.init({
					dsn: sentryDsn
				});
			}

			// Setup csrf token for ajax requests
			let getCookie = (name) => {
				var cookieValue = null;
				if (document.cookie && document.cookie !== '') {
					var cookies = document.cookie.split(';');
					for (var i = 0; i < cookies.length; i++) {
						var cookie = jQuery.trim(cookies[i]);
						// Does this cookie string begin with the name we want?
						if (cookie.substring(0, name.length + 1) === (name + '=')) {
							cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
							break;
						}
					}
				}
				return cookieValue;
			};
			let csrftoken = getCookie('csrftoken');

			function csrfSafeMethod(method) {
				// these HTTP methods do not require CSRF protection
				return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
			}

			$.ajaxSetup({
				beforeSend: function (xhr, settings) {
					if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
						xhr.setRequestHeader("X-CSRFToken", csrftoken);
					}
				}
			});
		});
	</script>

</body>

</html>