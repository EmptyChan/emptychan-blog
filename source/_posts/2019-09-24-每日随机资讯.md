---
title: 2019-09-24-每日随机资讯
tags: 资讯
thumbnail: /images/周二.png
date: 2019-09-24 20:54:45
---

# 娱乐
### [视频：杨幂邓伦同框录节目被偶遇 现身故宫观看文物很认真](http://video.sina.com.cn/p/ent/2019-09-23/detail-iicezzrq7946308.d.html)
> 概要: 视频：杨幂邓伦同框录节目被偶遇 现身故宫观看文物很认真
### [前排吃瓜?郑爽曝知名明星为事业运三年不近女色](https://ent.sina.com.cn/s/m/2019-09-23/doc-iicezueu7908798.shtml)
> 概要: 新浪娱乐讯 9月23日，郑爽在社交网站上爆料某知名明星，称其“为了事业运三年不近女色，现在发展挺好的。”。“爽妹子好耿直”。(责编：小5)。
### [组图：曹云金离婚后恢复黄金单身汉 与美女聚餐身材发福](http://slide.ent.sina.com.cn/star/slide_4_704_322447.html)
> 概要: 组图：曹云金离婚后恢复黄金单身汉 与美女聚餐身材发福
### [孙怡自拍小脸蛋脏兮兮 笑侃：洗吧洗吧还能要吧](https://ent.sina.com.cn/s/m/2019-09-24/doc-iicezueu8090310.shtml)
> 概要: 新浪娱乐讯 9月24日，孙怡. 在微博晒出一张自拍，并配文：. (责编：七七). 
### [组图：袁咏仪夫妇游凤凰古城 张智霖酒吧唱《新不了情》引欢呼](http://slide.ent.sina.com.cn/y/slide_4_704_322490.html)
> 概要: 组图：袁咏仪夫妇游凤凰古城 张智霖酒吧唱《新不了情》引欢呼
### [被赞为“旺剧锦鲤”的女明星这次失效了，来数数她的新剧抄了几部](https://new.qq.com/omn/20190922/20190922A0C07100.html)
> 概要: 杨紫真的是流量贵人。嗯，《莫格利》中的女主同样是被歹徒绑架到无人的马路上。这个打破杨紫锦鲤体质的“异数”，也只能怨她自己。
### [50岁邓文迪太牛了，与富豪新男友及闺蜜“美国第一女儿”游罗马](https://new.qq.com/omn/20190924/20190924A0A0IW00.html)
> 概要: 近日，邓文迪与新男友一同到意大利罗马旅游，媒体公开了他们的合照，坐在邓文迪左边的“小卷毛”就是他。话说，邓文迪与传媒大亨默克多虽然离婚多年，但她并未离开上流社会，仍与以前的名媛们保持良好的关系。这次，邓文迪与新男友到罗马游玩，原来身边有伊万卡及她的丈夫贾瑞纳陪伴！
### [《演员请就位》铁过张一山？杨紫最牛发小儿牛骏峰请求出战！](https://new.qq.com/omv/video/r3000rve3z6)
> 概要: 《演员请就位》铁过张一山？杨紫最牛发小儿牛骏峰请求出战！
### [贾宝玉香菱再聚首！欧阳奋强陈剑月重游故地，仍有二爷丫鬟既视感](https://new.qq.com/omv/video/y09305ylp5s)
> 概要: 贾宝玉香菱再聚首！欧阳奋强陈剑月重游故地，仍有二爷丫鬟既视感
### [国庆四片本周六集体点映，《海上钢琴师》《星际探索》《航海王》确认引进](https://new.qq.com/omn/20190924/20190924A0MXOW00.html)
> 概要: 四部国庆档热门影片均。点映时间分布如下。关于档期，有消息称有望11月上映，但尚未官宣。
# 动漫
### [PS4游戏《神田川 JET GIRLS》2020年1月16日发售](https://news.dmzj.com/article/64742.html)
> 概要: Marvelous在本日（24日）宣布了将于2020年1月16日发售PS4平台新作动作竞速类游戏《神田川 JET GIRLS》。本作的通常版售价7980日元，限定版售价13800日元。
### [时崎狂三主角的《Date·A·Bullet》动画化企划进行中](https://news.dmzj.com/article/64723.html)
> 概要: 轻小说《Date·A·Bullet》宣布了动画化企划进行中的消息。本作中依然将由真田麻美为狂三配音。
### [【同人】晴空与阴空同人-我未曾对阴空先生开口的过去2](https://news.dmzj.com/article/64741.html)
> 概要: 我关掉台灯，将日记本合上从桌子里空白纸张上撕下一小张纸写下给她的留言放在桌子上面。而后我操控她的身体回到床铺上平躺好解除对她的控制。
### [哆啦A梦，静香到底有几个爸爸？真相已经不是整容能解决的问题了](https://new.qq.com/omn/20190924/20190924A0LSHT00.html)
> 概要: 从小到大《哆啦a梦》此部动漫作品就一直是我比较喜欢的，相信应该也是许多小伙伴们的美好童年时光，自从上世纪70年代连载以来，可谓是伴随着很多代人的成长，几十年的时间里，哆啦A梦也是不断在更新和创作中，……
### [奥特曼：守护地球谁功劳最大？是初代还是诺亚或是迪迦？](https://new.qq.com/omn/20190924/20190924A0FMN400.html)
> 概要: 奥特曼：守护地球谁功劳最大？是初代还是诺亚或是迪迦？
### [推进城再次出现新情况，所有囚犯被释放，黄猿：跟我去加入革命军](https://new.qq.com/omn/20190924/20190924A0LPCL00.html)
> 概要: 推进城再次出现新情况，所有囚犯被释放，黄猿：跟我去加入革命军
### [海贼王：萨博的“死亡消息”另有隐情，薇薇是钦定的伊姆继承人？](https://new.qq.com/omn/20190924/20190924A0M3PO00.html)
> 概要: 海贼王：萨博的“死亡消息”另有隐情，薇薇是钦定的伊姆继承人？
### [地灵曲速看35：绝境相逢，天启圣女陷入危机](https://new.qq.com/omv/video/y0930eiiveb)
> 概要: 地灵曲速看35：绝境相逢，天启圣女陷入危机
### [不存在的西行02 八戒召回还奇经 肉贵三藏别有用心](https://new.qq.com/omv/video/r30005laq0n)
> 概要: 不存在的西行02 八戒召回还奇经 肉贵三藏别有用心
# 科技
### [如何通过conda从源代码或二进制文件安装基于PyTorch(包括caffe2)的DensePose](https://www.ctolib.com/Johnqczhang-densepose_installation.html)
> 概要: 如何通过conda从源代码或二进制文件安装基于PyTorch(包括caffe2)的DensePose
### [PEInfo 用于读取指定PE文件相关信息](https://www.ctolib.com/zmbilx-PEInfo.html)
> 概要: PEInfo 用于读取指定PE文件相关信息
### [thor 是一个C++库，为深度学习提供了巨大的工具类、算法和可视化功能](https://www.ctolib.com/jinfagang-Thor.html)
> 概要: thor 是一个C++库，为深度学习提供了巨大的工具类、算法和可视化功能
### [TensorFlow - 端到端机器学习平台-用于Ruby](https://www.ctolib.com/ankane-tensorflow.html)
> 概要: TensorFlow - 端到端机器学习平台-用于Ruby
### [用云开发数据库实现列表触底自动加载功能丨云开发101](https://www.tuicool.com/articles/AN7BBbM)
> 概要: 用云开发数据库实现列表触底自动加载功能丨云开发101
### [.NET Core 3正式发布](https://www.tuicool.com/articles/Jfyme2v)
> 概要: Announcing .NET Core 3.0 | .NET Blog
### [这些年的体验技术部（二） · 前端工程 - 与云共舞，未来已来](https://www.tuicool.com/articles/a6JzI3b)
> 概要: 蚂蚁金服体验技术部，是阿里巴巴经济体内，一支以用户体验、大前端和创新产品为核心竞争力的年轻团队。「那些年的体验技术部」开源了 Ant Design、AntV、Egg.js、Umi 等一系列项目。下面的系列文章，会描绘体验技术部正在做的和未来想做的事情，期待你的想法与心动。
### [国产动画电影崛起中的王微和追光](https://www.tuicool.com/articles/rMviqe7)
> 概要: 作者：符绩勋。就算《小门神》前期剧本让我看十遍，我也不懂得怎么判断它的票房。还有一点很重要，很长期地看，好的内容公司必须能沉淀IP。
### [著名分析师 Benedict Evans：人脸识别与AI伦理（二）](https://www.tuicool.com/articles/QzEZzqR)
> 概要: 神译局是36氪旗下编译团队，关注科技、商业、职场、生活等领域，重点介绍国外的新技术、新观点、新风向。承诺不开发“不好的东西”，在成立道德委员会的情况下，有一套流程来决定什么是不好的东西。其次，任何公司的人都可以决定某个人脸识别的用例（或者任何一种机器学习项目）是邪恶的，所以他们不会做那样的系统。
### [Jenkins插件漏洞分析](https://www.tuicool.com/articles/32Yvqif)
> 概要: Jenkins是一个广泛使用的开源自动化服务器，它允许DevOps开发人员高效、可靠地构建、测试和部署软件。包含在默认建议插件列表中的凭据插件用于存储加密的凭据。此插件将加密的凭据存储在$jenkins_home/credentials.xml中。
### [企业号运营难题怎么破？这些误区不能踩](https://www.tuicool.com/articles/NvAVFbb)
> 概要: 微信公众号面临七年之痒？企业号运营本就较难，如今如何破局？但这是目前很多企业的现状，他们的新媒体小编要写文章、做运营、做BD、做活动、做设计、还要兼职抖音快手小红书等等，一个新媒体小编扛下了整个公司新媒体的KPI。
### [出海成功的，为什么是抖音而不是微信？](https://www.tuicool.com/articles/rEfaEbM)
> 概要: 本文来自微信公众号。微信出海没成功，被吐槽不够重视本地化。我们梳理了抖音和微信的出海发展时间线，发现它们其实有诸多相似之处。
# 小说
### [间客](https://m.qidian.com/book/1223147/catalog)
> 作者：猫腻

> 标签：东方玄幻

> 简介：世界上有两件东西能够深深地震撼人们的心灵，一件是我们心中崇高的道德准则，另一件是我们头顶上灿烂的星空——康德当许乐从这行字上收回目光，第一次真正看到尘埃后方那繁若芝麻的群星时，并没有被震撼，相反他怒了：大区天空外面的星星这么刺眼，谁能受得了？天天被这些光晃着，只怕会变成矿道上那些被大灯照成痴呆的野猫！于是许乐放弃了成为一名高贵女性战舰指挥官辅官的梦想，开始在引力的作用下，堕落，堕落，堕落成了看门房的外乡穷小子，出卖身体的可怜男子，从事繁琐工作的男保姆……在波澜壮阔的大时代里，露着白牙，眯眼傻笑，披着莫名的光辉，一步一步地迈向谁也不知道的远方。…………许乐，东林大区公民，从一颗荒凉的半废弃星球上离开，脑海里拥有一些希奇古怪的知识，身体里拥有这个世界谁也不曾接触过的力量，并不浑沌，一味荒唐知足地进入了这个最无趣也是最有趣的世界。间客的人生，一定很精彩。

> 章节总数：共987章

> 状态：完本
### [仙道狂神](http://book.zongheng.com/book/368957.html)
> 作者：红色键盘

> 标签：奇幻玄幻

> 简介：老书《医道天下》解封中，喜欢的朋友请到熊猫看书或者百度书城搜索支持，或者加群 65168352 谢谢大家！作为青楼中一名奇葩的大茶壶，张扬深知泡妞三原则：“仙女怎么泡？一哄二骗床上抱！”“妖女怎么泡？一撩二弄三推倒！”“魔女怎么泡？神油皮鞭加镣铐！”只可惜，想法是美好的，现实却是残酷的。可怜的张扬身体里竟然住进了一位霸气无双女王范的腹黑小萝莉，从此，他被迫流氓的日子就在这个仙道世界中轰轰烈烈地展开了！群 142497683 欢迎大家加入！

> 章节末：第644章 天命所归

> 状态：完本
# 论文
### [SpatialNLI: A Spatial Domain Natural Language Interface to Databases Using Spatial Comprehension](https://paperswithcode.com/paper/spatialnli-a-spatial-domain-natural-language)
> 日期：28 Aug 2019

> 标签：READING COMPREHENSION

> 代码：https://github.com/VV123/SpatialNLI

> 描述：A natural language interface (NLI) to databases is an interface that translates a natural language question to a structured query that is executable by database management systems (DBMS). However, an NLI that is trained in the general domain is hard to apply in the spatial domain due to the idiosyncrasy and expressiveness of the spatial questions.
### [Improving Deep Transformer with Depth-Scaled Initialization and Merged Attention](https://paperswithcode.com/paper/improving-deep-transformer-with-depth-scaled)
> 日期：29 Aug 2019

> 标签：MACHINE TRANSLATION

> 代码：https://github.com/bzhangGo/zero

> 描述：The general trend in NLP is towards increasing model capacity and performance via deeper neural networks. However, simply stacking more layers of the popular Transformer architecture for machine translation results in poor convergence and high computational overhead.
