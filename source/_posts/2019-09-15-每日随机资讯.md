---
title: 2019-09-15-每日随机资讯
tags: 资讯
thumbnail: /images/周日.png
date: 2019-09-15 20:25:59
---

# 娱乐
### [陈庭欣彩排胃抽筋幸好无碍 自称最近很倒霉](https://ent.sina.com.cn/tv/zy/2019-09-14/doc-iicezueu5674809.shtml)
> 概要: 新浪娱乐讯 北京时间9月14日消息，据香港媒体报道，陈庭欣前天为节目《香港唱好演唱会》彩排时突然胃抽筋，要保安人员用担架床送她到休息室，经休息后无大碍才继续晚上演出。穿低胸装亮相的陈庭欣晚上脸色好转不少，她坦言最近很倒霉，之前滑水被滑水板撞到脸部，眼肚附近位置瘀黑一大片，要靠化妆遮盖出镜，幸好没伤及眼睛。陈庭欣称胃抽筋后已立即跟家人报平安，完秀后会考虑去做身体检查，并笑道。
### [组图：郭富城一家现身机场紧抱女儿 跳舞卖萌逗宝宝开心](http://slide.ent.sina.com.cn/y/slide_4_704_321749.html)
> 概要: 组图：郭富城一家现身机场紧抱女儿 跳舞卖萌逗宝宝开心
### [视频：杨幂生日获绯闻男生大胆表白 曾多次发文：我最爱的大幂幂](http://video.sina.com.cn/p/ent/2019-09-14/detail-iicezzrq5772475.d.html)
> 概要: 视频：杨幂生日获绯闻男生大胆表白 曾多次发文：我最爱的大幂幂
### [组图：张国立baby黄明昊身披古色纱幔 秒变“时尚弄潮儿”开大秀](http://slide.ent.sina.com.cn/z/v/slide_4_704_321750.html)
> 概要: 组图：张国立baby黄明昊身披古色纱幔 秒变“时尚弄潮儿”开大秀
### [组图：毛不易穿长款黑衬衫变民国小公子 口罩遮面快步走低调有型](http://slide.ent.sina.com.cn/y/slide_4_704_321760.html)
> 概要: 组图：毛不易穿长款黑衬衫变民国小公子 口罩遮面快步走低调有型
### [《爸爸去哪儿》萌娃近照，他12岁身高180，逆天身高没谁了！](https://new.qq.com/omn/20190914/20190914A0934E00.html)
> 概要: 在参加节目的时候天天还是一个刚刚6岁的孩子，甚至一度因为分到的房间有家禽的味道，而不愿意入住，在张亮的怀抱中不停的哭鼻子，但是如今多年过去之后他已经长大成为了一个12岁的大男孩了，之间的变化简直就是判若两人了呢。从他的相关近照中可以非常清楚的看到，现在的他已经不再是当年的那个“萌娃”，而是身高接近180cm的帅气大男孩，要知道按道理来讲他比石头哥哥小一些应该个子再稍微矮一些才对的，但是谁也没有想到，他的身高却像逆天一样的让人不服不行。原来天天不光爸爸张亮是模特出身，就连妈妈寇静也曾是一位职业模特，所以说他天生就携带有高个子的基因，再加上如今生活条件的提高，每天都能喝到足够的牛奶，自然而的在身高方面也就显现的比同龄人优秀许多许多了。
### [胡军父亲胡宝善中秋节离世享年84岁 系著名歌唱家曾登春晚](https://new.qq.com/omn/ENT20190/ENT2019091400195800.html)
> 概要: 腾讯娱乐讯 2019年6月，《我爱这蓝色的海洋》（胡宝善、王传流词，胡宝善曲）入选中宣部“庆祝中华人民共和国成立70周年优秀歌曲100首”。胡宝善有一个非常幸福的家庭，夫人王亦满原是北京空军文工团话剧演员。
### [还记得火遍全网的“淘宝奶奶”吗？生活照曝光，我们被骗了好久！](https://new.qq.com/omn/20190915/20190915A0CPTQ00.html)
> 概要: 如今随着科技越来越发达，想必现在很多网友们购物的话，首选就是网络购物了。毕竟现在网购的平台非常多，关于物流和网店各种的东西也发展的越来越成熟，让大家免受了逛街的痛苦，可以享受在家躺着就能买东西的轻松。也是因为这个行业越来越吃香，现在有很多的颜值高的人，都想要去做平面模特，拍照片来赚钱。
### [李嘉欣在公公离世后终于自由？退隐十年被曝录节目，瘦瘦高高好美](https://new.qq.com/omn/20190915/20190915A05X9900.html)
> 概要: 提起李嘉欣，大家都耳熟能详，超级幸运命好的女明星。年轻时没有嫁入豪门，凭借靓丽外表姣好的演技，在圈中混得风声水气。感情上，也是桃花不断，追逐者多到排满大街...最终，她与许晋亨相识并开展了一段波折的豪门姻缘，经历几番波折，她还是如愿嫁进豪门，成了许家儿媳，财富地位由此稳固。
### [千玺和小凯不和？千玺和所有人打招呼独忽略大哥，小凯反应绝了！](https://new.qq.com/omn/20190915/20190915A0GJ2Z00.html)
> 概要: 几年前，三个男孩组合就出现在我们的视野中。说真的，很喜欢TFBOYS这个组合，三个帅气的小男孩，多才多艺，一路走到现在，别提有多励志了！如今，昔日的小男孩已经是三个大小伙了，当然，也更加的出名了。
# 动漫
### [【CAST】《ID:INVADED》追加声优公开 2020年播出](https://www.acgmh.com/28611.html)
> 概要: 《ID:INVADED》 是苍井启负责作画监督的原创科幻悬疑类动画，动画预定于2020年播出。从PV中可以看到，故事讲述身为名侦探的主人公酒井户围绕着死亡的迷之少女展开调查。
### [【动画化】《新樱花大战》动画化决定 2020年播出](https://www.acgmh.com/28605.html)
> 概要: 《新樱花大战》是日本世嘉发行的游戏系列，近日宣布动画化决定。本作将以《新樱花大战 the Animation》为题，预计在2020年开始播出。这次的《新樱花大战》是1996年发售的游戏《樱花大战》的续作。
### [【每日话题】萌菌作者问题发言引发争议-这两个有那么像？](https://news.dmzj.com/article/64568.html)
> 概要: 由于舰C、舰R、舰B都使用了战舰拟人化（娘化）的题材，所以这几款游戏的粉丝之间一直存在着各种各样的矛盾。最近，《萌菌物语》的作者石川雅之突然在推特上吐槽：“很难用言语表达啊。总之就想问下‘这样好么?’”
### [《无限之住人-IMMORTAL-》追加角色发表 10月10日播出](https://news.dmzj.com/article/64566.html)
> 概要: 沙村广明原作的动画《无限之住人-IMMORTAL-》的追加角色发表了。
### [动画《碧蓝幻想》第二季PV2公布 10月4日开播](https://news.dmzj.com/article/64571.html)
> 概要: 动画《碧蓝幻想》第二季PV2公布，10月4日开播，同时官网还公开将在9月28日举行第二季第1话+第2话的先行放送会，之后还有预定：声优小野友树和东山奈央、监督梅本唯、制作人铃木健太登台参与的谈话部分。
### [受日本奥运新规影响 少年漫画杂志封面问题引发讨论](https://news.dmzj.com/article/64570.html)
> 概要: 少年漫画杂志封面问题引发讨论：穿泳衣的写真女星教坏小朋友？日本奥运新规范越来越夸张……
### [一部看得人直冒冷汗的番剧，评分8.1，绝对不是简单的丧尸片](https://new.qq.com/omn/20190915/20190915A0EH9X00.html)
> 概要: 一部看得人直冒冷汗的番剧，评分8.1，绝对不是简单的丧尸片
### [海贼王：路飞四档真的伤不到凯多吗？索隆开启左眼告诉你答案](https://new.qq.com/omn/20190915/20190915A0H1IN00.html)
> 概要: 海贼王：路飞四档真的伤不到凯多吗？索隆开启左眼告诉你答案
### [七武海甚平和凯多三灾谁更强？大妈对此给出解释：别小看那个男人](https://new.qq.com/omn/20190915/20190915A0GW9V00.html)
> 概要: 七武海甚平和凯多三灾谁更强？大妈对此给出解释：别小看那个男人
### [八仙过海各显神通，《龙珠最强之战》各大排行榜榜首火热争夺中！](https://new.qq.com/omn/20190914/20190914A0DH9700.html)
> 概要: 八仙过海各显神通，《龙珠最强之战》各大排行榜榜首火热争夺中！
### [这居然是同一个人？日本cosplay年轻人晒化妆前后对比](https://new.qq.com/omn/20190914/20190914A0DM2600.html)
> 概要: 这居然是同一个人？日本cosplay年轻人晒化妆前后对比
### [假面骑士中最有钱的五个骑士，零一家开集团，而他的父亲竟是首相](https://new.qq.com/omn/20190914/20190914A0DNJE00.html)
> 概要: 假面骑士中最有钱的五个骑士，零一家开集团，而他的父亲竟是首相
# 科技
### [PMS：支持多应用的统一权限管理系统，用flask+vue实现](https://www.ctolib.com/fish2018-openpms.html)
> 概要: PMS：支持多应用的统一权限管理系统，用flask+vue实现
### [WMZBanner - 最好用的轻量级轮播图+卡片样式+自定义样式,链式编程语法](https://www.ctolib.com/wwmz-WMZBanner.html)
> 概要: WMZBanner - 最好用的轻量级轮播图+卡片样式+自定义样式,链式编程语法
### [拥有地面真实分割掩模和生成因子的多目标图像数据集](https://www.ctolib.com/deepmind-multi_object_datasets.html)
> 概要: 拥有地面真实分割掩模和生成因子的多目标图像数据集
### [基于react hooks实现的状态管理工具](https://www.ctolib.com/yangbo5207-moz.html)
> 概要: 基于react hooks实现的状态管理工具
### [用户流失严重、烧钱没有尽头，电影票订阅服务MoviePass宣布关闭](https://www.tuicool.com/articles/imYj63u)
> 概要: 【猎云网（微信号：）】9月14日报道（编译：小猪配齐）。MoviePass通知用户，它计划关闭这项服务，因为它对MoviePass进行资本重组的努力迄今尚未成功。此外，据外媒报道，MoviePass上个月至少解雇了7名员工，员工总数降至12人左右。
### [数据分析与大数据，如何引爆制造业？](https://www.tuicool.com/articles/IfYfIfB)
> 概要: 大数据作为新一代信息技术的代表，已开始在工业设计、研发、制造、销售、服务等环节取得应用，并成为推动互联网与工业融合创新的重要因素。面对大数据的浪潮，传统企业要主动把握大数据的发展方向，深入挖掘大数据的价值，分析需求偏好、改善生产工艺以及提升企业的内部管理水平等。通过大数据来分析当前需求变化和组合形式。
### [天猫、虾米音乐、故宫、奥利奥纷纷加入月饼大战，形式本身就是最大的内容](https://www.tuicool.com/articles/UfeaqeV)
> 概要: 又一年中秋临近，年年被黑的五仁月饼终于松了口气，对它一年一度的吐槽在今年似乎被多个品牌共同引发的月饼大战所忽略。在今年的战局中，不仅传统面点公司和餐饮企业一如既往发力，互联网公司、网红餐厅，甚至故宫本尊也纷纷跨界加入，花式跨界、口味创新、黑科技，形式之多令消费者眼花缭乱。除了前文反复探讨的创新月饼，设计力的方法逻辑体现在各个领域、各个形态的产品中。
### [比“炒鞋”更疯狂的“炒盲盒”来了，背后公司挂牌...](https://www.tuicool.com/articles/yEjEbiq)
> 概要: 本文转载自微信公众号。中证君在网购平台上发现，隐藏款人偶玩具价格不菲，热门的品种都在千元以上。咸鱼数据显示，一位上海闲鱼用户转卖盲盒一年就赚10万元之多。
### [JavaScript历史学习总结](https://www.tuicool.com/articles/qUjqqmV)
> 概要: JavaScript历史学习总结
### [增强学习在推荐系统有什么最新进展？](https://www.tuicool.com/articles/RrqIfyF)
> 概要: 前阵子正好写了一篇专栏分析google在youtube应用强化的两篇论文。在短期目标下，容易不停的给用户推荐已有的偏好。RL中还有on-policy的方法，和off-policy的区别在于更新Q值的时候是沿用既定策略还是用新策略。
### [“丧命式”吃播爆火背后，是成年人羞于启齿的一面](https://www.tuicool.com/articles/rYJvmuz)
> 概要: “我们不是因为饿而吃，而是因为空虚、叛逆、悲伤。我们想寻求慰藉，或是喜悦。”。大胃王密子君这样的吃播火了后，一大批人渴望通过这样的方式赚钱。
### [PM面试题·你最喜欢的产品是什么？](https://www.tuicool.com/articles/fiAjUfu)
> 概要: “即刻最大的优点是有信息整合的功能，它能汇总微博、知乎、B站、豆瓣等网站的内容，按专题形式进行展示，减少用户获取信息的成本并提高效率。“即刻能够吸引用户的一点是相比于微博，参与感更强。
# 小说
### [甲武战神](http://book.zongheng.com/book/385267.html)
> 作者：风停雨_91

> 标签：科幻游戏

> 简介：(完结）老风新书《甲武战神II》热血归来，求各种支持！。一个穿越到未来的普通青年能做什么，武装，机甲，战机？一个觉醒了第八脑域的战士能做什么，狂野格斗，弧线射击，一击必杀？一个改装大师能做什么，机甲进化，增幅基点，转换磁场？一个拥有超级机甲的格斗王能做什么，热血激斗，纵横星域，无人能挡？一个小人物被迫卷入了一场毁天灭地的阴谋中，他能做什么，挣扎求存，逆行而上，创造传说？本书慢热属长篇，请耐心观看，求推荐收藏.

> 章节末：第二十章 狂野的血腥

> 状态：完本
### [大宋小王爷](http://book.zongheng.com/book/665166.html)
> 作者：翔钧

> 标签：历史军事

> 简介：王冕阴差阳错的穿越到了南唐，他可不想去当一个亡国奴。机缘巧合之下，他结识了赵匡胤。赵匡胤对王冕非常赏识，并从南唐国主李煜的手中，要下了他。王冕来到了大宋王朝后，积极辅佐赵匡胤，加快了赵匡胤统一全国的时间……

> 章节末：第309章 全家其乐融融（大结局）

> 状态：完本
# 论文
### [Guided Dialog Policy Learning: Reward Estimation for Multi-Domain Task-Oriented Dialog](https://paperswithcode.com/paper/guided-dialog-policy-learning-reward)
> 日期：28 Aug 2019

> 标签：

> 代码：https://github.com/truthless11/GDPL

> 描述：Dialog policy decides what and how a task-oriented dialog system will respond, and plays a vital role in delivering effective conversations. Many studies apply Reinforcement Learning to learn a dialog policy with the reward function which requires elaborate design and pre-specified user goals.
### [CAMEL: A Weakly Supervised Learning Framework for Histopathology Image Segmentation](https://paperswithcode.com/paper/camel-a-weakly-supervised-learning-framework)
> 日期：28 Aug 2019

> 标签：MULTIPLE INSTANCE LEARNING

> 代码：https://github.com/ThoroughImages/CAMEL

> 描述：Histopathology image analysis plays a critical role in cancer diagnosis and treatment. To automatically segment the cancerous regions, fully supervised segmentation algorithms require labor-intensive and time-consuming labeling at the pixel level.
