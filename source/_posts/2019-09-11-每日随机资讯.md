---
title: 2019-09-11-每日随机资讯
tags: 资讯
thumbnail: /images/周三.png
date: 2019-09-11 21:27:22
---

# 娱乐
### [组图：徐冬冬金属烟熏妆夸张恐怖 网友：吓哭隔壁小孩了](http://slide.ent.sina.com.cn/star/slide_4_704_321606.html)
> 概要: 组图：徐冬冬金属烟熏妆夸张恐怖 网友：吓哭隔壁小孩了
### [组图：奚梦瑶被传怀孕后踩超细高跟开工 小腹平坦完全不像孕妇](http://slide.ent.sina.com.cn/star/w/slide_4_704_321575.html)
> 概要: 组图：奚梦瑶被传怀孕后踩超细高跟开工 小腹平坦完全不像孕妇
### [视频：沙溢带老婆胡可深夜撒狗粮 一路紧搂步调一致超默契](http://video.sina.com.cn/p/ent/2019-09-11/detail-iicezzrq5032521.d.html)
> 概要: 视频：沙溢带老婆胡可深夜撒狗粮 一路紧搂步调一致超默契
### [组图：“港姐之母”陈紫莲家人为其设灵 灵堂尊逝者遗愿布满花圈](http://slide.ent.sina.com.cn/star/slide_4_704_321619.html)
> 概要: 组图：“港姐之母”陈紫莲家人为其设灵 灵堂尊逝者遗愿布满花圈
### [防弹少年团连续21周打入公告牌 再次刷新夺冠记录](https://ent.sina.com.cn/y/yrihan/2019-09-11/doc-iicezueu4970336.shtml)
> 概要: 新浪娱乐讯 韩国偶像组合防弹少年团的《MAP OF THE SOUL ： PERSONA》连续21周打入了美国Billboard200专辑榜。吕东垠/文 版权所有Mydaily禁止转载。(责编：肉圆)。
### [章子怡出席活动孕味十足，汪峰小心翼翼牵她上场](https://new.qq.com/omn/20190911/20190911A0H7FH00.html)
> 概要: 近日，章子怡出席了从影二十周年纪念展。从照片中我们可以看到章子怡穿着一身宽松的黑色半身裙，黑色高跟鞋，梳着高高的丸子头。章子怡看起来还是气质十足，表现也十分大方，不愧是国际章。
### [被曝限制出境电影疑用AI换脸，好友程青松透露范冰冰最新状态](https://new.qq.com/omn/20190911/20190911A0OVOL00.html)
> 概要: 税务风波后范冰冰复出屡次被阻，她将参演的新电影《355》已经开拍，至今作为主演之一的范冰冰都未进组，开机现场都是其他主演拿着范冰冰的照片合影，电影《355》官方晒出了范冰冰大拳击以及开车的画面，力证范冰冰继续出演。范冰冰被限制出境的传闻甚嚣尘上，范冰冰本人及工作室都没有回应，而她本人也一直在国内工作，本来于8月份就要进组的范冰冰至今还没有出国安排，难道她真的要用AI换脸技术完成自己这部复出大作的拍摄？被曝限制出境，范冰冰却懒理传闻依然在国内做公益，乐此不疲坚持献爱心，无论是否被限制出境，这份爱心是无人能及的，不少观众还是期待范冰冰的复出新作的，从程青松分享的公益善举来看，范冰冰应该还没有出国，还在国内。
### [“安陵容”陶昕然晒一家三口合照，女儿小何陶长相神似爸爸](https://new.qq.com/omn/20190911/20190911A0DH7900.html)
> 概要: “安陵容”陶昕然晒一家三口合照，女儿小何陶长相神似爸爸
### [幼年易烊千玺怎么能这么可爱！穿黄色小蜜蜂服装的大佬真不多见](https://new.qq.com/omv/video/a0925gk9c63)
> 概要: 幼年易烊千玺怎么能这么可爱！穿黄色小蜜蜂服装的大佬真不多见
### [陈赫喜迎二胎？一家三口聚餐张子萱肚子凸起十分明显](https://new.qq.com/omv/video/c0925gfgza8)
> 概要: 陈赫喜迎二胎？一家三口聚餐张子萱肚子凸起十分明显
# 动漫
### [声优小笠原早纪病情好转 宣布继续开始工作](https://news.dmzj.com/article/64523.html)
> 概要: 在5月的时候宣布因治疗舌癌而休假的声优小笠原早纪，在9日宣布了继续开始工作的消息。小笠原早纪表示：这次因为病情好转，再次开始工作。在休假中给不少相关人士带来了麻烦，在此深表歉意。在治疗期间，来自粉丝们的问候真的鼓励了我，在今后将继续努力，争取更上...
### [动画《无限之住人-IMMORTAL-》新PV公开](https://news.dmzj.com/article/64527.html)
> 概要: 根据沙村广明原作制作的《无限之住人-IMMORTAL-》公开了第二弹PV。在这段PV中，收录了OP主题曲《SURVIVE OF VISION》。
### [卡普空起诉光荣侵权案二审宣判！赔偿额上升至1.56亿日元](https://news.dmzj.com/article/64526.html)
> 概要: 在2014年7月4日，卡普空因认为光荣特库摩涉嫌侵犯专利权，起诉了光荣特库摩，并要求光荣特库摩赔偿经济损失9亿8323万1115日元。
### [濑尾公治《Hitman》公开第一话全裸版！](https://news.dmzj.com/article/64522.html)
> 概要: 在本日（11月）发售的《周刊少年Magazine》41号（讲谈社）上，公开了由濑尾公治创作的《Hitman》的第一话全裸版。
### [爆笑漫画：么么想要快速减肥，呆头就带着么么去医院截肢了](https://new.qq.com/omn/20190911/20190911A0OMR700.html)
> 概要: 爆笑漫画：么么想要快速减肥，呆头就带着么么去医院截肢了
### [斗罗大陆：剑斗罗和骨斗罗的实力谁更强？武魂殿令牌告诉你答案](https://new.qq.com/omn/20190911/20190911A0P6C200.html)
> 概要: 斗罗大陆：剑斗罗和骨斗罗的实力谁更强？武魂殿令牌告诉你答案
### [影视剧中最神秘的四个人，你知道几个？](https://new.qq.com/omn/20190911/20190911A0ON1L00.html)
> 概要: 影视剧中最神秘的四个人，你知道几个？
### [《龙珠》在日本动漫界地位如何？对后来日本漫画带来哪些影响？](https://new.qq.com/omn/20190911/20190911A0OO3M00.html)
> 概要: 《龙珠》在日本动漫界有着举足轻重的作用，长时间占据日本漫画销量冠军的宝座，有人说它是热血漫的鼻祖，即便不是鼻祖也是先河了。而且集英社都可以说是靠着《龙珠》给养活了，作者鸟山明本人在80年代也是多次荣……
### [斗罗大陆：唐三喜提“蓝银囚笼”，预选赛上技能发动，凯瑞全场](https://new.qq.com/omn/20190911/20190911A0LK8F00.html)
> 概要: 斗罗大陆：唐三喜提“蓝银囚笼”，预选赛上技能发动，凯瑞全场
### [火影忍者：为什么雏田比小樱更受欢迎，这几个原因绝对不能少](https://new.qq.com/omn/20190911/20190911A0P7BY00.html)
> 概要: 火影忍者：为什么雏田比小樱更受欢迎，这几个原因绝对不能少
# 科技
### [基于pjsip的简单音频通话](https://www.ctolib.com/barry-ran-pjsip-audio-stream.html)
> 概要: 基于pjsip的简单音频通话
### [Android JNI NDK的简单用法](https://www.ctolib.com/wanglongsoft-jni_learn.html)
> 概要: Android JNI NDK的简单用法
### [KubeSphere 是在 Kubernetes 之上构建的以应用为中心的多租户容器管理平台](https://www.ctolib.com/kubesphere-kubesphere.html)
> 概要: KubeSphere 是在 Kubernetes 之上构建的以应用为中心的多租户容器管理平台
### [ElasticDL 是一个基于 TensorFlow 2.0 和 Kubernetes 实现弹性深度学习](https://www.ctolib.com/sql-machine-learning-elasticdl.html)
> 概要: ElasticDL 是一个基于 TensorFlow 2.0 和 Kubernetes 实现弹性深度学习
### [巨资投入无人驾驶车辆研发，到底有什么好处呢？](https://www.tuicool.com/articles/ZRjU3aN)
> 概要: 在这张分类表里，L0-L1是纯粹的人工驾驶，意味着司机对驾驶负全责。无论在哪级，无人驾驶车辆都存在盲区，这和人开车一样。
### [8 款特别奇葩的网站，无聊时光必备](https://www.tuicool.com/articles/my6zInQ)
> 概要: 坐稳了，发车了(。它里面包含有上百种模板，有各种炫酷的表情包，人的体型。同时它还有筛选功能，可以选择磁力链接和百度网盘，还可以选择文件类型，同时可以选择下载热度，创建时间和文件大小。
### [Axure RP8 教程：实现高逼格的面板切换效果](https://www.tuicool.com/articles/NnmU73j)
> 概要: 如何利用Axure RP8 教程实现高逼格的面板切换效果呢？一起来文中看看~ 最后想到通过添加另一个动态面板，命名为【控制面板】，设置宽高与内容面板一致，为其内容添加一个长高度的热区，即能使面板出现滚动条。
### [一场生日 Party 的精华，是组织能力](https://www.tuicool.com/articles/JBZbamq)
> 概要: 杭州，奥体中心，9 月的天气依然闷热。在马云的不长的演讲中，善良、责任成为重复最多的两个词。2005 年，由于「独孤九剑」的审核体系过于复杂，阿里将九个词汇浓缩成六个：客户第一、团队合作、拥抱变化、激情、诚信、敬业。
### [为什么要使用SpringIOC？](https://www.tuicool.com/articles/bURbEfu)
> 概要: 通过new关键字生成了具体的对象，这是一种硬编码的方式，违反了面向接口编程的原则。IOC容器实现了开发中对象粒度的一种组件化，将每个对象当做组件一样放进容器当中。
### [猎云网2019年度教育产业项目路演火热征集中](https://www.tuicool.com/articles/ayA3eiz)
> 概要: 【猎云网（微信：）北京】9月11日报道。凭借多年的项目接触经验，猎云网计划将路演现场，搬到10月17日的FUS猎云网2019年度教育产业峰会。依靠海量接触创业项目的优势经验，猎云网将会对对报名的项目，提前进行遴选，一般来说，能上路演的项目一般都会经过组织方的筛选和指导，对投资人来说，这是一批可投性很强的项目，项目的可投性可以得到保证。
### [怎样不在朋友圈浪费时间？数学模型给你答案](https://www.tuicool.com/articles/mYR3ym6)
> 概要: 之后研究者用巴西大选期间30个媒体账号的Facebook信息流数据，来验证上述模型的假设是成立的。这里作者再引入一条假设：即每次刷朋友圈的成本固定是1，也是一条新鲜事的收益。
### [从天使到魔鬼，短视频内容只有一步之遥](https://www.tuicool.com/articles/zI3Yvu7)
> 概要: 文 | 快抖情报站。”办公司自制爆米花“原视频已被发布者删除，不过其账号“办公室小野”仍在正常运营，据钛媒体报道，发生上述惨剧后，小野又在抖音上传了类似视频，用酒精灯做牛轧糖，继续收割粉丝注意力，令人瞠目结舌。短视频上危险的视频同样会形成“破窗效应”，如果放任不管，将会带来更多的模仿者，造成更大的危害。
# 小说
### [英雄无敌之大农场](http://book.zongheng.com/book/56646.html)
> 作者：猪猪头

> 标签：科幻游戏

> 简介：这里没有外挂，这里相对公正，这里是梦想的世界。新书  异界之唯一吸血鬼  http://book.zongheng.com/book/206528.html希望大家支持。 （  群   104097715            ）

> 章节末：第五百二十七章 （大结局）

> 状态：完本
### [机械天才](http://book.zongheng.com/book/36519.html)
> 作者：娃娃脸

> 标签：科幻游戏

> 简介：“白金型机甲、多功能运输舰、太空巡航舰、星途战舰、巨无霸航母……无论是最新款，还是最陈旧的古董机械，对我来说，都如同驾驶自动档的电动摩托车一般简单。”	“你们听——是英雄的声音。”……新书《兵皇》火热出炉，故事更加精彩，战斗更加热血，每天更新7000字，链接在封面下边，请大家多多支持。

> 章节末：新书《兵皇》火热出炉，敬请支持！

> 状态：完本
# 论文
### [Establishing Strong Baselines for the New Decade: Sequence Tagging, Syntactic and Semantic Parsing with BERT](https://paperswithcode.com/paper/establishing-strong-baselines-for-the-new)
> 日期：14 Aug 2019

> 标签：PART-OF-SPEECH TAGGING

> 代码：https://github.com/emorynlp/bert-2019

> 描述：This paper presents new state-of-the-art models for three tasks, part-of-speech tagging, syntactic parsing, and semantic parsing, using the cutting-edge contextualized embedding framework known as BERT. For each task, we first replicate and simplify the current state-of-the-art approach to enhance its model efficiency.
### [InstaBoost: Boosting Instance Segmentation via Probability Map Guided Copy-Pasting](https://paperswithcode.com/paper/instaboost-boosting-instance-segmentation-via)
> 日期：21 Aug 2019

> 标签：DATA AUGMENTATION

> 代码：https://github.com/GothicAi/InstaBoost

> 描述：Instance segmentation requires a large number of training samples to achieve satisfactory performance and benefits from proper data augmentation. To enlarge the training set and increase the diversity, previous methods have investigated using data annotation from other domain (e.g. bbox, point) in a weakly supervised mechanism.
