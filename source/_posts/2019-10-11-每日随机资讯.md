---
title: 2019-10-11-每日随机资讯
tags: 资讯
thumbnail: /images/周五.png
date: 2019-10-11 20:43:20
---

# 娱乐
### [视频：陈思诚《唐探3》首曝海报 背景藏“名侦探”细节超惊喜](http://video.sina.com.cn/p/ent/2019-10-11/detail-iicezuev1416720.d.html)
> 概要: 视频：陈思诚《唐探3》首曝海报 背景藏“名侦探”细节超惊喜
### [《十年》导演钟澍佳赞娜扎演技 曝鹿晗拍戏不离组](https://ent.sina.com.cn/v/m/2019-10-11/doc-iicezuev1488998.shtml)
> 概要: 近日，由钟澍佳。钟澍佳：我很看重一个戏，我在看剧本的时候它给我的感觉。钟澍佳：在接拍新作品时我会优先考虑曾经合作过多、印象非常好的演员，当我接下这个项目后我跟制片人聊过演员的问题，我的第一人选就是窦骁跟娜扎，然后我就去问他们“我要开新戏了，你们有没有档期”，接着就促成了合作，一切都很自然。
### [组图：蔡依林蓝唇写真造型 粉色长发湿漉漉披脑后歪嘴邪笑](http://slide.ent.sina.com.cn/y/slide_4_704_323640.html)
> 概要: 组图：蔡依林蓝唇写真造型 粉色长发湿漉漉披脑后歪嘴邪笑
### [组图：张翰着红色风衣配黑色高领亮相 怀抱小孩秒变带娃“奶爸”](http://slide.ent.sina.com.cn/z/v/slide_4_704_323670.html)
> 概要: 组图：张翰着红色风衣配黑色高领亮相 怀抱小孩秒变带娃“奶爸”
### [《唐探3》剧组为刘昊然庆生 王宝强陈思诚送蛋糕](https://ent.sina.com.cn/m/c/2019-10-11/doc-iicezzrr1544473.shtml)
> 概要: 新浪娱乐讯 昨日（10月10日）是刘昊然。的生日，今天刘昊然工作室在微博分享了一则视频，并配文。“分享一个小彩蛋～第二次和秦风一起过生日了，感谢唐人街探案剧组的全体同仁陪老秦一同成长。
### [这个小姐姐气质独特，颜值不输女明星](https://new.qq.com/omn/20191011/20191011A0EJNR00.html)
> 概要: 这个小姐姐气质独特，颜值不输女明星
### [陈小春被问：你的前女友哪点比应采儿好，6个字回答是我一年的笑点](https://new.qq.com/omv/video/k3007vsp09b)
> 概要: 陈小春被问：你的前女友哪点比应采儿好，6个字回答是我一年的笑点
### [实力派男神因自卑错过陶虹？大方祝福徐峥，与现任妻子因戏生情](https://new.qq.com/omv/video/f3006n11gaw)
> 概要: 实力派男神因自卑错过陶虹？大方祝福徐峥，与现任妻子因戏生情
### [网友发帖售卖吴亦凡潘玮柏用过的筷子等？引发一众网友纷纷辣评](https://new.qq.com/omv/video/q30070ktts8)
> 概要: 网友发帖售卖吴亦凡潘玮柏用过的筷子等？引发一众网友纷纷辣评
### [徐璐录节目脸直接怼镜头上，相机瞬间没滤镜，网友不淡定了](https://new.qq.com/omv/video/q3007eddr3e)
> 概要: 徐璐录节目脸直接怼镜头上，相机瞬间没滤镜，网友不淡定了
# 动漫
### [动画《棒球大联盟2nd》第二季宣布将于2020年4月开播](https://news.dmzj.com/article/64925.html)
> 概要: 根据满田拓也原作制作的TV动画《棒球大联盟2nd》宣布了将于2020年4月开始播出的消息。
### [【同人】干物妹小埋同人——太平与叶](https://news.dmzj.com/article/64926.html)
> 概要: 叶的脸颊染上了桃色，然而一直没能开口，看着这样的她，再怎么迟钝也会明白了，再怎么怀疑也会相信了。
### [WAVE《少女与战车》武部沙织1/7比例手办开订](https://news.dmzj.com/article/64928.html)
> 概要: WAVE根据《少女与战车》中的角色武部沙织制作的1/7比例手办目前已经开订了。本作采用了武部沙织身着比基尼风格的围裙，托着战车造型的情人节蛋糕时的既可爱又充满跃动感的造型。手办全高约250mm，预计将于2019年12月发售，售价17380日元，约...
### [日本网友投票哪个角色让自己萌发了性的意识](https://news.dmzj.com/article/64919.html)
> 概要: rocketstaff株式会社发布了在MangaKing中对用户进行的“哪个角色让自己萌发了性的意识”的问卷调查的结果。其中，《草莓100%》中的西野司排名第一，另外《鲁邦三世》中的峰不二子和《海贼王》中的娜美分别排名第二和第三。
### [COS大神动漫仿妆，千寻和波妞还好，白娘子满分还原](https://new.qq.com/omn/20191011/20191011A0LIGE00.html)
> 概要: COS大神动漫仿妆，千寻和波妞还好，白娘子满分还原
### [overlord：14卷更新时间确定，王国袭击魔导国，骨王启动歼灭战](https://new.qq.com/omn/20191011/20191011A0N6PT00.html)
> 概要: overlord：14卷更新时间确定，王国袭击魔导国，骨王启动歼灭战
### [火影：鸣人骗了这个人一辈子，网友：说好一起努力你却走了捷径！](https://new.qq.com/omn/20191011/20191011A0L9BI00.html)
> 概要: 火影：鸣人骗了这个人一辈子，网友：说好一起努力你却走了捷径！
### [进击的巨人：所谓“王血”只是尤弥尔的主观判断，关键时刻不管用](https://new.qq.com/omn/20191011/20191011A0N69V00.html)
> 概要: 在《进击的巨人》的故事中，王族血统可以说是一份非常关键的力量，希斯特利亚拥有这份力量之后甚至可以仅仅依靠着触碰纸张就感受到尤弥尔的记忆，这份力量还是使用“坐标”之力的发动条件之一，所以说是拥有王族血……
### [海贼王：路飞距离四皇差距有多大，仅凭这一点他就无法超越](https://new.qq.com/omn/20191011/20191011A040GK00.html)
> 概要: 海贼王：路飞距离四皇差距有多大，仅凭这一点他就无法超越
### [还好凹凸曼没有嘴，不然迪迦就要吐了！网友：迪迦要口吐芬芳了](https://new.qq.com/omv/video/x3007muxrcl)
> 概要: 还好凹凸曼没有嘴，不然迪迦就要吐了！网友：迪迦要口吐芬芳了
# 科技
### [docx2md: Go实现的微软word格式转换为markdown格式的工具](https://www.ctolib.com/mattn-docx2md.html)
> 概要: docx2md: Go实现的微软word格式转换为markdown格式的工具
### [SQL模板字符串，防止SQL注入](https://www.ctolib.com/hyperdivision-prepare-sql.html)
> 概要: SQL模板字符串，防止SQL注入
### [Vearch 是一个用于深度学习向量高效相似性搜索的分布式系统](https://www.ctolib.com/vearch-vearch.html)
> 概要: Vearch 是一个用于深度学习向量高效相似性搜索的分布式系统
### [μPlot - 一个超快速的微小时间序列图表📈](https://www.ctolib.com/leeoniya-uPlot.html)
> 概要: μPlot - 一个超快速的微小时间序列图表📈
### [图灵奖得主力推：PyTorch1.3 今天发布](https://www.tuicool.com/articles/UBfqman)
> 概要: 今天凌晨，PyTorch 开发者大会在旧金山开幕，会上发布了最新版本 PyTorch1.3。本次更新最大的亮点在于对移动设备的支持、挑战传统张量的「命名张量」，以及更好的性能改进。如今在 PyTorch 1.3 中，我们可以通过命名直接访问张量维度。
### [如何穿透流量营销的3大壁垒：碎片化、粉尘化、雾霾化？](https://www.tuicool.com/articles/ZBJRNfe)
> 概要: 流量越来越贵，获客越来越难，常常花了钱还是没有增长。面对艰难的现实环境，如何打破层层壁垒，触达更多的用户？这个案例就是典型以内容营销的方式去解决碎片化和粉尘化的问题，也就是不管你是通过电视，还是互联网，只要把想要传达的品牌信息放在内容当中，总是可以触达到你。
### [闲鱼循环工厂：重塑废物的一万种可能！](https://www.tuicool.com/articles/Rfy6VfF)
> 概要: 越来越多的老厂房被改造成了艺术街区或办公室，比如北京的798、上海杜月笙的粮仓、深圳的浮法玻璃厂和杭州的锅炉厂等。在这股暗潮中我们应该看到，借垃圾分类调侃“我是什么垃圾”的年轻人其实是在呐喊。从公益的角度看，闲鱼天然是为循环经济而生，所以扩大阿里电商版图的同时，闲鱼让阿里的三大战略（农村、教育、公益）生态变得更加丰满起来。
### [是时候了解阿里投资的这家美国体育电商了](https://www.tuicool.com/articles/nuuAzai)
> 概要: 文 | 零售资本论，作者 | 财报分析师。因此，与UA一起拿下MLB装备特许权的Fanatics，从某种意义上也继承了Majestic的资源，但在这种新型的合作模式中，Fanatics获得了更为广泛的权利，而MLB也可以获得更多的收益。在新合同中，Fanatics成为MLS特许球衣和产品的独家供应商，权利从之前的销售拓展至制造并销售联赛面向球迷销售的球衣及产品。
### [对话天图投资潘攀：复盘投资奈雪与背后的新品牌时代](https://www.tuicool.com/articles/6nUjymr)
> 概要: 作者/李曌。潘攀：我们内部对团队没什么争议，主要对品类有争议。潘攀：上市首先要看缘分，但我了解他们当前没有考虑过，毕竟公司成立才4年，现在最重要的还是要做好业务，思考怎么让消费者体验更佳。
### [Docker多阶段构建的理解与使用](https://www.tuicool.com/articles/QfmYJj2)
> 概要: 在构建镜像的过程中可能会区分为编译镜像以及运行镜像，我们在编译环境中进行二进制运行文件的构建编译工作，然后将运行文件放置在运行环境中构建体积较小的运行镜像，在这个过程中，我们可能会使用到多阶段构。
### [为什么说网红是提前消费的兴奋剂？](https://www.tuicool.com/articles/jaeaaqa)
> 概要: 图片来源@视觉中国 喜欢的网红对于某一产品的的测评视频，尽管这个广告视频长达30分钟，粉丝也会坚持看完，然后进行留言、点赞、分享、及购物的行为。第一次出现超前消费可能是想要获得网红推荐的一款Tom Ford 粉底液，刷卡的时候告诉自己，就这一次。
### [阿里腾讯砸钱，能否逆转东南亚支付格局？](https://www.tuicool.com/articles/EJ3mI32)
> 概要: Photo by Katherine McCormack on Unsplash，作者：阿熊。目前东南亚的手机支付，可以总结为三大特点：高度分散、层出不穷、财阀控制。还有跨国发展的手机支付，比如新加坡的GrabPay，中国的支付宝和微信支付。
# 小说
### [黄金时代里的名侦探公平](http://book.zongheng.com/book/797054.html)
> 作者：晚来天欲雪

> 标签：二次元

> 简介：杜公平只是一个普通高中生，一朝他被发现具有名侦探的能力，世界开始变得不一样，美女、破案，恋爱、失恋、忠诚、战斗……，不同的世界开始向他慢慢展示不一样的全部面貌。

> 章节末：27.21	终章

> 状态：完本
### [王者之证](http://book.zongheng.com/book/791542.html)
> 作者：叶仔仔

> 标签：评论文集

> 简介：主角叶城身为异魔岛四大组织中的金牌杀手，为了执行一件任务卧底在学校内，扮成一名高三学生保护司木家族千金司木浅。而叶城自身也存在着诸多不为人知的秘密。

> 章节末：第四十七章 天罚之主，君临天下！

> 状态：完本
# 论文
### [HARE: a Flexible Highlighting Annotator for Ranking and Exploration](https://paperswithcode.com/paper/hare-a-flexible-highlighting-annotator-for)
> 日期：29 Aug 2019

> 标签：DOCUMENT RANKING

> 代码：https://github.com/OSU-slatelab/HARE

> 描述：Exploration and analysis of potential data sources is a significant challenge in the application of NLP techniques to novel information domains. We describe HARE, a system for highlighting relevant information in document collections to support ranking and triage, which provides tools for post-processing and qualitative analysis for model development and tuning.
### [Visual Semantic Reasoning for Image-Text Matching](https://paperswithcode.com/paper/visual-semantic-reasoning-for-image-text)
> 日期：6 Sep 2019

> 标签：IMAGE RETRIEVAL

> 代码：https://github.com/KunpengLi1994/VSRN

> 描述：Image-text matching has been a hot research topic bridging the vision and language areas. It remains challenging because the current representation of image usually lacks global semantic concepts as in its corresponding text caption.
