---
title: 2019-10-31-每日随机资讯
tags: 资讯
thumbnail: /images/周四.png
date: 2019-10-31 21:02:12
---

# 娱乐
### [新裤子乐队获年度最受欢迎乐队 彭磊险些“手滑”](https://ent.sina.com.cn/y/yneidi/2019-10-31/doc-iicezzrr6324987.shtml)
> 概要: 新浪娱乐讯 亚洲新歌榜2019年度盛典在京举行，新裤子乐队获得“年度最受欢迎乐队”荣誉， 。今年大热的综艺让乐队官微在入驻不久迅速积聚大量的粉丝，人气的背后是他们源源不断的创作活力。在接受颁奖时，彭磊......
### [师徒情深！大鹏柳岩沈阳路演不忘与赵本山聚餐](https://ent.sina.com.cn/m/c/2019-10-30/doc-iicezuev6055100.shtml)
> 概要: 新浪娱乐讯  10月30日晚，大鹏 通过微博晒和赵本山 、柳岩 、 导演聚餐合影，并配文道：“就好我师父这口儿酸菜锅！”照片中，大鹏手拿相机对镜自拍，露出灿烂的笑容，赵本山坐在身后，红光满面，精神气儿......
### [草彅刚主演电影饰演变性人 称将面临巨大挑战](https://ent.sina.com.cn/m/f/2019-10-31/doc-iicezzrr6229572.shtml)
> 概要: 新浪娱乐讯 据日本媒体Modelpress报道，草彅刚接拍Netflix《全裸导演》导演内田英治执导的新片《午夜天鹅》，称接受从未面临过的巨大挑战，影片于十月下旬开拍，上映日期还未确定。该片剧本......
### [TOP前女友曝MONSTAX成员Shownu与有夫之妇交往](https://ent.sina.com.cn/y/yrihan/2019-10-31/doc-iicezzrr6247966.shtml)
> 概要: 新浪娱乐讯 韩瑞熙今天在社交网站发文称MONSTAX成员Shownu曾与有夫之妇交往。 　　韩瑞熙今天在社交网站公布了几张截图，表示这是她的同性恋人郑多恩收到的网友爆料，该网友表示妻子曾与Shownu......
### [视频：2019亚洲新歌榜年度盛典 李艾胡海泉开场主持](http://video.sina.com.cn/p/ent/2019-10-31/detail-iicezzrr6309979.d.html)
> 概要: 视频：2019亚洲新歌榜年度盛典 李艾胡海泉开场主持
### [18岁出道的吴奇隆49岁了：岁月没有饶过我，也把最好的给了我](https://new.qq.com/omn/20191031/20191031A097Z900.html)
> 概要: 今天，2019年10月31日，曾经的“霹雳虎”吴奇隆已经49岁了。从18岁出道到今天，他似乎总是伤痕累累，但好在，上帝在给予他苦难的同时，还是帮他写好了“苦尽甘来”的人生剧本......
### [何洁现身机场无名指钻戒惹人注目，和刁磊结婚了？](https://new.qq.com/omn/20191031/20191031A0MVRS00.html)
> 概要: 何洁今日现身机场，她也是许久没有露面了，她穿着一件黑色的宽松大卫衣，手里抱着一只可爱的熊猫公仔，穿着黑色的紧身牛仔裤，带着黑色口罩，看起来非常休闲的样子。何洁今日还是素颜出镜，没有化妆，发型也没有怎么......
### [陈冲发长文讲述与许知远的友情：我们为同一种精神而欣喜](https://new.qq.com/omn/ENT20191/ENT2019103100902200.html)
> 概要: 腾讯娱乐讯 在腾讯新闻出品的《十三邀》第四季第2期中，许知远飞到旧金山，与57岁的陈冲在海边漫谈、探访她最爱的书店。31日晚，陈冲在微博发长文回忆了两人友情的建立过程和交浅言深的精神世界碰撞，尤其是文......
### [马蓉骂王宝强“废物”后现身机场，一身名牌气质雍容华贵](https://new.qq.com/omn/20191031/20191031A0AXB900.html)
> 概要: 马蓉骂王宝强“废物”后现身机场，一身名牌气质雍容华贵
### [郎朗妻子吉娜不化浓妆超惊艳！素颜曝光如邻家少女，撞脸宋慧乔](https://new.qq.com/omn/20191031/20191031A0E0UN00.html)
> 概要: 长得漂亮，就是很容易成为大家关注的对象，比如这位！ 吉娜和和郎朗自从结婚以后，就一直受到大家关注！不仅身材好，而且颜值非常高，尤其是一双大眼惹人羡慕，但是那浓厚的假睫毛实在有煞风景。确实，她自从受到大......
# 动漫
### [【同人】剃须，然后捡到女高中生同人——《剃须》](https://news.dmzj.com/article/65238.html)
> 概要: 这句感谢的话就像春风一样，让我的心里暖意洋洋。啊，不对，现在是夏天，应该是燥热才对，我的脸烫了起来。
### [【每日话题】全国起床困难户地图公布 你今天赖床了吗？](https://news.dmzj.com/article/65227.html)
> 概要: 今天是10月的最后一天，距离立冬还有8天时间。随着冬季的临近，天气越来越冷，起床也成了一大难题。那么屏幕前的你，能否抵御温暖被窝的诱惑呢？
### [TV动画《神田川JET GIRLS》因制作问题宣布第五话延期](https://news.dmzj.com/article/65223.html)
> 概要: 根据TV动画《神田川JET GIRLS》官方消息，原定在11月5日深夜播出的《神田川JET GIRLS》第5话，因制作上的问题需要延期播出。所以在11月5日将播出第4.5话《总集篇》，在总集篇中，将会回顾一下之前播出的前几集的内容。另外本篇的第5...
### [多次延期的手机游戏《爱相随 EVERY》正式开服](https://news.dmzj.com/article/65237.html)
> 概要: 科乐美推出的恋爱模拟游戏《爱相随 EVERY》，在经历了多次长时间的延期后，于本日（10月31日）开服。为纪念开服，游戏正在举办开服纪念抽卡活动和新手登录奖励活动。
### [动物狂想曲，一集一个ED透露富裕本色，虎狼明争暗斗显示良苦用心](https://new.qq.com/omn/20191031/20191031A0NHYF00.html)
> 概要: 在动物狂想曲的校园中不同种族的动物之间有着太多的明争暗斗，在好强的路易学长光辉的背后被迫笼罩在这一伟岸形象阴影下的动物们就更是如此。在路易因伤痛而倒下后，这个矛盾就像洪水决堤般涌出，就连被路易亲自选……
### [《白蛇：缘起》一部充满诚意的作品，值得它应有的评价](https://new.qq.com/omn/20191031/20191031A0NINK00.html)
> 概要: 最近又看了一遍《白蛇：缘起》，我觉得它的剧情结构其实问题不大，整体剧情流畅，且铺垫足够，高潮部分也把控的挺好。要说美中不足还是时长有点短，且影片中的时间跨度小，音乐的氛围烘托不够。但整体质量比起这两……
### [泰迦凹凸曼十一月剧透，网友表示这些怪兽都看吐了！杰顿：怎么又有我？](https://new.qq.com/omv/video/v3015j05y04)
> 概要: 泰迦凹凸曼十一月剧透，网友表示这些怪兽都看吐了！杰顿：怎么又有我？
### [厨神小当家：满怀期待却分外失望，是曾经看的人变了？还是番变了？](https://new.qq.com/omn/20191031/20191031A0N7FI00.html)
> 概要: 《厨神小当家》曾经80 90比较熟知的一部动漫，曾经的名字为《中华小当家》当然这个名字其实也是错的，《中华小当家》的真名是《中华一番》只是当时引进这部番的中国台湾公司：“统一”企业，刚好是做方便面等……
### [慎勇亲身证道，没有必然的黑深残只有不努力的勇者大人，帅气的背后是付出](https://new.qq.com/omn/20191031/20191031A0NBN000.html)
> 概要: 慎重的勇者和不聪明的女神大人又又又一次用胜利证明了慎重的好处，大家看到圣哉一通流畅的操作洋溢起了欢乐的气氛，但慎重勇者的欢乐显然和《为美好世界献上祝福》的欢乐不尽相同，这里的欢乐，都是由这位慎重的勇……
### [假面骑士王权盖茨能力揭开：可以召唤历代二骑，使用所有二骑的武器！](https://new.qq.com/omn/20191031/20191031A0MW1V00.html)
> 概要: 虽说假面骑士时王的故事已经结束有一段时间了，但是关于假面骑士时王各种外传还没有上映。要知道在假面骑士里除去tv剧之外，还会有各种外传与之相配合，构造出一个完整的假面骑士故事。最好的体现就是前段时间刚……
# 科技
### [wujian100_open让IC设计和开发应更快，更简单，更可靠](https://www.ctolib.com/T-head-Semi-wujian100_open.html)
> 概要: wujian100_open让IC设计和开发应更快，更简单，更可靠
### [ARM-X Firmware Emulation Framework](https://www.ctolib.com/therealsaumil-armx.html)
> 概要: ARM-X Firmware Emulation Framework
### [bert4keras - 更清晰、更轻量级的keras版bert](https://www.ctolib.com/natureLanguageQing-bert4keras.html)
> 概要: bert4keras - 更清晰、更轻量级的keras版bert
### [gnome-shell 的日夜壁纸](https://www.ctolib.com/b1izzard-34-Dynamic-gnome-wallpapers.html)
> 概要: gnome-shell 的日夜壁纸
### [学习 Druid（十二）：使用 Prometheus 监控 Druid 集群](https://www.tuicool.com/articles/Q3Yn2eE)
> 概要: 更新至 Druid 0.15.1 版本  截止目前，Druid 官方并不支持 Prometheus，且使用 Prometheus 监控 Druid 集群面临以下几个问题：  Druid 使用 Push......
### [谈谈互联网产品里的游戏化现象](https://www.tuicool.com/articles/qIZVVzn)
> 概要: 我在2016年底的时候开始关注游戏理念在互联网产品中的应用，关注的原因是因为多年养成的跨界思考习惯与坚持底层原理的普适性原则。这两点原因让我持续的寻找不同行业成熟的底层逻辑，并期望从中获取灵感以应用到......
### [美团 iOS 端开源框架 Graver 在动态化上的探索与实践](https://www.tuicool.com/articles/uyiqUn6)
> 概要: 近些年，移动端动态化技术可谓是“百花齐放”，其中的渲染性能也是动态化技术一直在探索、研究的课题。美团的开源框架 Graver 也为解决动态化框架的渲染性能问题提供了一种新思路：关于布局，我们可以采用“......
### [深挖嘉楠耘智招股书：近一年亏损4.2亿，有望成为全球区块链第一股？](https://www.tuicool.com/articles/3YFRjaV)
> 概要: 图片来源@unsplash  文｜一本财经，作者｜棘轮、比萨  10月29日，嘉楠耘智向美国SEC递交了招股书，计划募资不超过4亿美元。  这个曾折戟A股借壳、新三板、港交所的矿机企业，开始了第四次I......
### [PEpper：一款针对可执行程序的开源恶意软件静态分析工具](https://www.tuicool.com/articles/2ye2u2y)
> 概要: 今天给大家介绍的是一款名叫PEpper的工具，这是一款开源的脚本工具，广大研究人员可以利用该工具来对可执行程序进行恶意软件静态分析。  工具安装  eva@paradise:~$ git clone ......
### [Python之父宣布退休，但Python仍在吞噬世界](https://www.tuicool.com/articles/2yiMfei)
> 概要: 编者按：本文来自微信公众号“  InfoQ  ”（ID：infoqchina），作者InfoQ，36氪经授权发布。  10月30日，Python之父Guido大牛宣布退休，离开Dropbox。  他发......
### [面试官: 两个Redis实例怎么快速对比哪些数据不一致](https://www.tuicool.com/articles/MfEzIrV)
> 概要: 最近又在翻 黄老师的 《Redis 设计与实现》，想到几道面试题 结合实际生产过程中的一些步骤作为总结  问题  如上图如何能快速的从两个Redis实例怎么快速对比哪些数据不一致？  什么是数据不一致......
### [暴风创始人被抓 公司全部高管辞职 深交所急了：快找人](https://www.tuicool.com/articles/QZjAR3B)
> 概要: 暴风集团，就是那个推出过暴风影音、暴风电视的公司，过去一度是股妖王，股价疯涨，但是今年爆发了危机，创始人冯鑫  7  月底被抓。现在这家公司已经无人管理了，因为老板被抓之后，公司高管全都辞职了。  根......
# 小说
### [网游之驱魔圣枪](http://book.zongheng.com/book/358424.html)
> 作者：陨落之夜

> 标签：科幻游戏

> 简介：我上一世只是一个默默无名为了生活而奋斗的人，老天给了我重生的机会，那我必要走上人类的巅峰！！“感激我将你从永恒的折磨中解救出来吧！！”

> 章节末：第七十一章 结尾

> 状态：完本
### [浮生(《霸王别姬》般清美凄凉的伶人传奇！)](http://book.zongheng.com/book/792790.html)
> 作者：阿幂

> 标签：历史军事

> 简介：那一年，她签下一纸契约，卖入云卿班；只供衣食，打死无论，从此踏进了梨园。七年勤勉如日，终于被她一步登上天蟾楼；甫一出台，便使众生倾倒，风流尽占。奈何戏子贱籍，纵然惊华动天，也难逃宿命；更何况她有违祖训，以女充男，且心性卓然。只见官家阔少纠缠于先，奸人宵小构陷在后；更有同门子弟觊觎嫉妒，倾慕之人负义欺瞒。任台上挥袖似水，绰约如梅,玉梨风骨；也难掩惊艳身姿与绝伦画谱下的无底凄凉。要到何时，她才能在戏锁重楼中看见生天，又何处，是她落幕时唯一能启的归程……

> 章节末：第四十四章

> 状态：完本
# 论文
### [Megatron-LM: Training Multi-Billion Parameter Language Models Using Model Parallelism](https://paperswithcode.com/paper/megatron-lm-training-multi-billion-parameter)
> 日期：17 Sep 2019

> 标签：LANGUAGE MODELLING

> 代码：https://github.com/NVIDIA/Megatron-LM

> 描述：Recent work in unsupervised language modeling demonstrates that training large neural language models advances the state of the art in Natural Language Processing applications. However, for very large models, memory constraints limit the size of models that can be practically trained.
### [RLBench: The Robot Learning Benchmark & Learning Environment](https://paperswithcode.com/paper/rlbench-the-robot-learning-benchmark-learning)
> 日期：26 Sep 2019

> 标签：FEW-SHOT LEARNING

> 代码：https://github.com/stepjam/PyRep

> 描述：We present a challenging new benchmark and learning-environment for robot learning: RLBench. The benchmark features 100 completely unique, hand-designed tasks ranging in difficulty, from simple target reaching and door opening, to longer multi-stage tasks, such as opening an oven and placing a tray in it.
