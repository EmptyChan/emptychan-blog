---
title: 2019-10-14-每日随机资讯
tags: 资讯
thumbnail: /images/周一.png
date: 2019-10-14 20:33:31
---

# 娱乐
### [视频：外媒曝麦当娜与小36岁伴舞一同现身 疑似新恋情](http://video.sina.com.cn/p/ent/2019-10-14/detail-iicezzrr2118513.d.html)
> 概要: 视频：外媒曝麦当娜与小36岁伴舞一同现身 疑似新恋情
### [韩星朴基雄否认与《新入史官具海玲》制作人恋爱](https://ent.sina.com.cn/v/j/2019-10-14/doc-iicezuev2016151.shtml)
> 概要: 新浪娱乐讯 韩国艺人朴基雄今天被爆料正在与MBC电视剧。朴基雄曾出演《新入史官具海玲》，因此他与该剧制作PD交往的消息迅速引起了人们的关注。(责编：肉圆)。
### [组图：吴谨言戴链条眼镜抿嘴卖萌超可爱 穿百褶裙秒变学生妹](http://slide.ent.sina.com.cn/star/slide_4_704_323872.html)
> 概要: 组图：吴谨言戴链条眼镜抿嘴卖萌超可爱 穿百褶裙秒变学生妹
### [组图：台风少年团姚景元戴口罩难掩清秀眉眼 穿着休闲如邻家少年](http://slide.ent.sina.com.cn/y/slide_4_704_323976.html)
> 概要: 组图：台风少年团姚景元戴口罩难掩清秀眉眼 穿着休闲如邻家少年
### [组图：确认恋情！大卫·哈伯和莉莉·艾伦街头热吻相拥超甜蜜](http://slide.ent.sina.com.cn/star/h/slide_4_704_323930.html)
> 概要: 组图：确认恋情！大卫·哈伯和莉莉·艾伦街头热吻相拥超甜蜜
### [吴京老婆谢楠戴口罩低调现身 清瘦窈窕美腿吸睛](https://new.qq.com/omv/video/y3008637qyj)
> 概要: 吴京老婆谢楠戴口罩低调现身 清瘦窈窕美腿吸睛
### [杨颖这组时尚大片很好看，长发迷人，气场十足](https://new.qq.com/omv/video/p30087ketlp)
> 概要: 杨颖这组时尚大片很好看，长发迷人，气场十足
### [胡歌刘涛再续前缘：好演员，总会让我们念念不忘](https://new.qq.com/omv/video/f3008oo6khy)
> 概要: 胡歌刘涛再续前缘：好演员，总会让我们念念不忘
### [评分9.0，《犯罪现场》票房轻松破亿，古天乐又有钱建学校了](https://new.qq.com/omn/20191014/20191014A09QBS00.html)
> 概要: 3年近20部电影，一年6部电影，古天乐作为影视界的劳模显然是公认的，今年的第6部电影《犯罪现场》顺利开局，猫眼评分9.0，上映两日轻松破亿，这下古仔又有钱建学校了。在影片中古天乐饰演犯罪团伙的头目汪新元，是一些干着小勾当盗贼的偶像，又是四个兄弟的大哥，作为一个抢劫犯罪团伙的头目，古天乐饰演的汪新元有着穷凶极恶的一面，那就是肆意践踏法制，危害普通人的生命安全，但是又有着善意温暖的一面，在面对手下教训无辜人士时又是警告又是愤怒的。对于口碑票房这样的表现，古天乐也是可以多建几所学校了！
### [柴碧云 x SCYL 从古都来的潇洒利落girl](https://new.qq.com/omv/video/b30082yef3n)
> 概要: 柴碧云 x SCYL 从古都来的潇洒利落girl
# 动漫
### [【制作决定】《Assault Lily BOUQUET》动画制作决定](https://www.acgmh.com/28685.html)
> 概要: “Assault Lily”系列是株式会社“Azone International”与“acus”在2013年推出的1/12可动人偶与手办模型的多样化内容企划，武器与美少女的结合，以战斗为主题、以私立路德比克女学院发生的故事的轻小说系列、舞台剧系列等，也在多个媒体领域展开。2019年10月官方宣布该作品动画制作决定，动画将由SHAFT负责制作。原作：尾花泽轩荣（acus）
### [国庆抽奖活动获奖名单公布！](https://news.dmzj.com/article/64960.html)
> 概要: 国庆七天乐抽奖活动中奖名单公布！
### [P站美图推荐——FF14特辑](https://news.dmzj.com/article/64967.html)
> 概要: 明天国服FF14的5.0版本暗影之逆焰就要开服了，你是否已经做好请假/排队的准备了呢？
### [【原创】日常青春剧——《夕阳下殉情》1](https://news.dmzj.com/article/64968.html)
> 概要: 这个时间从这里望出去，春天能看到零稀飘扬在空中的樱花，夏天能一窥扭曲的阳炎，秋天则能望见澄澈高远的青空。而现在火红的夕阳笼罩着房内，冬天快到了，太阳落下得越发早了。
### [白人欲穿黑人角色服装参加Cosplay比赛遭拒绝](https://news.dmzj.com/article/64962.html)
> 概要: 在即将于月底在伦敦举办的COS大会“EuroCosplay”中，一名想要扮演《LOL》中的黑人角色“派克”参加决赛的法国女性Livanart，突然遭到了主办方的禁止出场的处罚。原因是因为COS黑人角色属于“黑脸”行为，会助长人种歧视。
### [奥兹幻境·绿野仙踪潮流特展](https://new.qq.com/omn/20191014/20191014A0M8V300.html)
> 概要: 奥兹幻境·绿野仙踪潮流特展
### [国漫一周看点75 时年半路截杀唐三 三藏一行身陷迷阵](https://new.qq.com/omv/video/t3008q9mmwl)
> 概要: 国漫一周看点75 时年半路截杀唐三 三藏一行身陷迷阵
### [刀剑神域：爱丽丝和亚丝娜和桐人同居差距巨大，一个苦涩一个甜蜜](https://new.qq.com/omn/20191014/20191014A0LXPP00.html)
> 概要: 刀剑神域：爱丽丝和亚丝娜和桐人同居差距巨大，一个苦涩一个甜蜜
### [海贼世界旧时代10大强者，鹤中将上榜，卡普屈居第二](https://new.qq.com/omn/20191014/20191014A0M0QC00.html)
> 概要: 海贼世界旧时代10大强者，鹤中将上榜，卡普屈居第二
### [王者cos有多牛？高配VS低配有差距，躲过西施，看到貂蝉笑喷](https://new.qq.com/omn/20191014/20191014A0KSNN00.html)
> 概要: 王者cos有多牛？高配VS低配有差距，躲过西施，看到貂蝉笑喷
### [《鬼灭之刃》在日本有多火？来看看网友们分享的书店照片](https://new.qq.com/omn/20191014/20191014A0LY1N00.html)
> 概要: 今天看到了一位日本网友分享的图，那是他在某家书店里拍到的画面，属于《鬼灭之刃》漫画的那一排书架上基本已经空了，就独留下一本以富冈义勇这个角色为封面的一卷单行本。这位网友调侃道，福冈被大家欺负了。其实……
# 科技
### [SMPL模型（一种人体三维模型） 的一个C++实现](https://www.ctolib.com/YeeCY-SMPLpp.html)
> 概要: SMPL模型（一种人体三维模型） 的一个C++实现
### [AutoFitColorTextView 固定边界，内容字体大小自适应并且可以指定同一字符串不同内容不同颜色的 TextView](https://www.ctolib.com/wangfeng19930909-AutoFitColorTextView.html)
> 概要: AutoFitColorTextView 固定边界，内容字体大小自适应并且可以指定同一字符串不同内容不同颜色的 TextView
### [统计 EMNLP-IJCNLP 2019 接收的论文列表并提供arXiv链接地址](https://www.ctolib.com/roomylee-EMNLP-2019-Papers.html)
> 概要: 统计 EMNLP-IJCNLP 2019 接收的论文列表并提供arXiv链接地址
### [基于Redis存储的Bloom Filter](https://www.ctolib.com/I-Guitar-RedisBloomFilter.html)
> 概要: 基于Redis存储的Bloom Filter
### [农业产值超500亿美元，加州是如何炼成的？](https://www.tuicool.com/articles/Ez6j2mY)
> 概要: 图片来源@unsplash. 发展历程回顾：农业趋于多样化、现代化，乳制品产值居第一. 加州农业早已由单一传统产业转变为生产门类多样化、现代化的农业产业。
### [最前线｜阿里王帅双11前夕高调回应二选一：良币驱逐劣币](https://www.tuicool.com/articles/VrEV7bE)
> 概要: 要入冬了，今年“二选一"戏码来得格外早。此外，10月2日，拼多多方面还表示，“二选一”是商业竞争的最初级手段，本质上是通过逼迫品牌只能选择单一渠道，来影响消费者的选择。拼多多的“强硬”态度也许来自于百亿补贴以来的乐观业绩和股价。
### [中国互联网出海，为何不愿强攻欧洲市场？](https://www.tuicool.com/articles/Vv6Vbaq)
> 概要: 编者按：本文来源创业邦专栏热点微评，作者王新喜。在没有抗衡势力的欧洲，基本被来自美国的互联网企业攻陷。中国互联网出海的战略很明显：目标地一般是先易后难的，从不涉及文化属性的项目开始，从避开硅谷大厂的主力战场开始。
### [前端周报：Vue 3 公开代码库；NPM 发布 6.12.0；Sass 推出模块系统](https://www.tuicool.com/articles/Y3Ebyae)
> 概要: 前端周报专注大前端领域内容，以对外文资料的搜集为主，帮助开发者了解一周前端热点，分为新闻热点、深度阅读、开源项目 & 工具等栏目。欢迎关注【前端之巅】微信公众号（ID: frontshow），及时获取前端周报内容。Sass 模块系统是解决命名冲突的更强大的方案，新增 @use 和 @forward 规则，而 @import 将被 Sass 逐渐放弃。
### [账号体系设计：账号与登录方式](https://www.tuicool.com/articles/juiqAjZ)
> 概要: 笔者对用户注册、登录和认证等账号功能设计进行了分析，结合相关流程总结了自己的思考和理解，与大家分享。有的产品的使用并非一定要登录，但是如果用户并未登录，我们如何去查看用户行为数据呢？2. 登录换绑流程（以第三方登录为例）。
### [消息称ofo已还清蚂蚁金服欠款 能涅槃重生么？](https://www.tuicool.com/articles/3em2Ar7)
> 概要: 欢迎关注“创事记”微信订阅号：sinachuangshiji。消息人士透露，这两笔借款的规定归还时间为2020年2月。另有接近ofo的人士告诉长庚君，以目前的盈利状况，ofo想要破产清算恐怕都难。
### [打造健身行业版“淘宝”，看氢练如何改变传统健身模式？](https://www.tuicool.com/articles/faaemmU)
> 概要: 【猎云网（微信：）北京】10月14日报道（文/盛佳莹）。氢练无疑想通过新模式来入局健身行业。但线上一对多的优势，有利于健身教练打造个人IP，通过优质的服务内容来赢得用户，从而让健身教练回归专业。
### [再见，烧钱时代](https://www.tuicool.com/articles/e2yIVjr)
> 概要: 编者按：本文来源创业邦专栏深响，作者 丁直仁。王冉则更直白的将过去几年互联网创投出现过热现象的原因归结为疯狂的资本：只要有一个疯子跳出来给了一个高估值（特别是如果这个疯子还有很响亮的名字和很令人羡艳的成功案例），所有的投资人都会觉得这就是一个现实中的“可比”标杆，所有的创业者都觉得你要不给我同样甚至更高的估值你就是占我便宜。无论中外，小米、Uber等明星公司上市后的表现，都引发了市场对过去互联网创投模式的反思。
# 小说
### [仙焰](http://book.zongheng.com/book/53441.html)
> 作者：有道

> 标签：武侠仙侠

> 简介：他从小就聪明绝顶，却锋芒内敛，凭着对修仙的执着去争夺那一丝机缘，且看他如何在弱肉强食，实力为尊的残酷修仙界立足、发展，如何踏遍千山万水、历尽重重劫难、不断攀升，最终获得仙缘，仙名留千古。    本书将会为大家展开一幅场面宏大的修仙画卷，作者志在写出修仙的苦与乐，没有白日飞升，只有步步登仙，以平凡、质朴的语言，为大家送上一本好的古典仙侠书。焰子四门vip群号：167154915（新群招人中），焰子三门群号：145186291（普通群、未满招人中），焰子门群号：15393058（普通群已满），焰子二门群号126803044（高级群已满）有道新书《超凡记》，已于创世中文网开始连载，望支持！

> 章节末：第一二五三章 我们结丹吧

> 状态：完本
### [五行战天](http://book.zongheng.com/book/451695.html)
> 作者：大地瓜

> 标签：奇幻玄幻

> 简介：这里没有绚目的魔法，没有华丽的斗气，有的只是天地间无穷的战力，少年易天身怀五行体质，却被当作家族的废物。当他五行相生，体质大成时，看他如何闯五域，破九霄，踏上封天之路，身怀一腔热血，成就战天传说。

> 章节末：第一百六十六章 封号战天（大结局）

> 状态：完本
# 论文
### [Finding Generalizable Evidence by Learning to Convince Q&A Models](https://paperswithcode.com/paper/finding-generalizable-evidence-by-learning-to)
> 日期：12 Sep 2019

> 标签：QUESTION ANSWERING

> 代码：https://github.com/ethanjperez/convince

> 描述：We propose a system that finds the strongest supporting evidence for a given answer to a question, using passage-based question-answering (QA) as a testbed. We train evidence agents to select the passage sentences that most convince a pretrained QA model of a given answer, if the QA model received those sentences instead of the full passage.
### [Using Clinical Notes with Time Series Data for ICU Management](https://paperswithcode.com/paper/using-clinical-notes-with-time-series-data)
> 日期：12 Sep 2019

> 标签：MORTALITY PREDICTION

> 代码：https://github.com/kaggarwal/ClinicalNotesICU

> 描述：Monitoring patients in ICU is a challenging and high-cost task. Hence, predicting the condition of patients during their ICU stay can help provide better acute care and plan the hospital's resources.
