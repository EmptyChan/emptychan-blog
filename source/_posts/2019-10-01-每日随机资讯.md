---
title: 2019-10-01-每日随机资讯
tags: 资讯
thumbnail: /images/周二.png
date: 2019-10-01 18:44:08
---

# 娱乐
### [组图：网友晒陈赫张子萱带女儿出游照 安安正面曝光与爸爸神似](http://slide.ent.sina.com.cn/star/slide_4_704_323069.html)
> 概要: 组图：网友晒陈赫张子萱带女儿出游照 安安正面曝光与爸爸神似
### [《我和我的祖国》票房破5亿 仅用了两天不到时间](https://ent.sina.com.cn/m/c/2019-10-01/doc-iicezueu9516617.shtml)
> 概要: 影片口碑情况：微博大V推荐度98%，豆瓣8.2，猫眼9.7，淘票票9.5。（数据来源：猫眼）。(责编：隐)。
### [刘烨看大阅兵:威武！雄壮！帅！祝祖国生日快乐！](https://ent.sina.com.cn/s/m/2019-10-01/doc-iicezueu9502611.shtml)
> 概要: 祝祖国生日快乐！祝福祖国永远繁荣昌盛！”。
### [贾斯汀·比伯与海莉举行婚礼 现场邀请154名宾客](https://ent.sina.com.cn/m/f/2019-10-01/doc-iicezzrq9584830.shtml)
> 概要: 新浪娱乐讯 贾斯汀·比伯和海莉·鲍德温（此前已改名海莉·比伯）夫妻于当地时间本周一在美国南卡罗莱纳举办了婚礼。迎宾派对上给现场宾客放映了《恋恋笔记本》。
### [组图：奶茶妹妹章泽天素颜外出无惧镜头 粉衣彩裙甜美时尚](http://slide.ent.sina.com.cn/star/slide_4_704_323083.html)
> 概要: 组图：奶茶妹妹章泽天素颜外出无惧镜头 粉衣彩裙甜美时尚
### [刘恺威否认恋情，绯闻女友颜值身材不输杨幂，坦言心里只有小檽米](https://new.qq.com/omv/video/l3003v74vde)
> 概要: 刘恺威否认恋情，绯闻女友颜值身材不输杨幂，坦言心里只有小檽米
### [张恒夸赞郑爽纯真，谁注意到一旁郑爽的反应？真甜](https://new.qq.com/omv/video/a3003h70ubg)
> 概要: 张恒夸赞郑爽纯真，谁注意到一旁郑爽的反应？真甜
### [撞脸李咏开始走红，自爆4年赚近千万，至今不忘李咏恩情！](https://new.qq.com/omn/20191001/20191001A0CBED00.html)
> 概要: 现在这个年代，很多人会学会去模仿，特别是当年芒果台的《百变大咖秀》就是一个以模仿为主流的综艺节目，也在当时带火了很多人，当然模仿是一种形式，而有时候撞脸却更能让大家对他的印象加深。这里就有很多模仿出名的例子，有很多人模仿小沈阳，李健等等。而今天我们要介绍的是一个跟李咏撞脸的人，他的名字叫那威（真名吴亚军）。
### [长得性感妩媚，穿小短裙被老公说了几年，如今7岁儿子被养成这样](https://new.qq.com/omn/20191001/20191001A0BE6E00.html)
> 概要: 在娱乐圈里，长得漂亮的明星们，其实有不少，但是气质出众，有性感气质的女明星，也不是那么多。而今天要说的这个则是一个女主播，她就是冉莹颖。冉莹颖老公也是这样想的，并且希望冉莹颖不要再穿小短裙和长靴了，担心冉莹颖老的时候，膝盖会痛。
### [90后小花于正旗下，清新自然身材好，差点机遇才能红](https://new.qq.com/omn/20191001/20191001A0AV9Q00.html)
> 概要: 90后小花于正旗下，清新自然身材好，差点机遇才能红
# 动漫
### [aniplex+《超平和busters》premium box手办开订](https://news.dmzj.com/article/64842.html)
> 概要: aniplex+根据“超平和busters”制作的以埼玉县秩父市为舞台的动画作品《我们仍未知道那天所看见的花的名字》、《心灵想要大声呼喊》、《知晓天空之蓝的人啊》中的主人公面码、顺和葵制作的手办套装目前已经开订了。
### [漫画《落第忍者乱太郎》即将完结](https://news.dmzj.com/article/64840.html)
> 概要: 由尼子骚兵卫创作的漫画《落第忍者乱太郎》宣布了将于11月30日发售单行本最终卷65卷，本部作品也就此完结。
### [AI创作手冢治虫新作漫画！2020年2月公开](https://news.dmzj.com/article/64841.html)
> 概要: 株式会社Kioxia（旧名：东芝存储）宣布了将于2020年2月公开由AI技术与高速大容量快闪存储器结合创作的手冢治虫的新作漫画。希望通过这次的尝试，让手冢的漫画在时隔30年后复活。
### [雨中寻人，暗自神伤](https://new.qq.com/omn/20191001/20191001A0CDVY00.html)
> 概要: 雨中寻人，暗自神伤
### [鸣人从小被视为吊车尾，木叶四个家族排斥过他，四代：实在太过分](https://new.qq.com/omn/20191001/20191001A0CA1500.html)
> 概要: 鸣人从小被视为吊车尾，木叶四个家族排斥过他，四代：实在太过分
### [细田守/新海诚还是宫崎骏？浅谈霓虹高票房动画电影](https://new.qq.com/omn/20191001/20191001A0CE1K00.html)
> 概要: 自国产动画电影《哪吒魔童降世》票房破亿，国产动画崛起口号打响，其高票房成就可谓一时轰动，放眼霓虹，你所想象到高票房动画电影是哪几部呢？脑中一瞬间划过的是谁的名字，宫崎骏，新海诚，还是冈妈，亦或者大河……
### [海贼王：曾经的死敌，罗杰杀掉洛克斯，白胡子还与他成好朋友](https://new.qq.com/omn/20190929/20190929A02VR000.html)
> 概要: 海贼王：曾经的死敌，罗杰杀掉洛克斯，白胡子还与他成好朋友
### [海贼王：卡普战力再添实绩，没有战国当初那一掌，青雉就是新元帅](https://new.qq.com/omn/20191001/20191001A0B73U00.html)
> 概要: 海贼王：卡普战力再添实绩，没有战国当初那一掌，青雉就是新元帅
### [“地错”迎来最后一集，第三季同时宣布制作，还真就是制作组亲儿子！](https://new.qq.com/omn/20191001/20191001A0CDYY00.html)
> 概要: “地错”迎来最后一集，第三季同时宣布制作，还真就是制作组亲儿子！
# 科技
### [一个用 RC 小车、树莓派、Arduino和开源软件实现的小规模的自动驾驶项目](https://www.ctolib.com/hamuchiwa-AutoRCCar.html)
> 概要: 一个用 RC 小车、树莓派、Arduino和开源软件实现的小规模的自动驾驶项目
### [single-spa 是微前端技术的核心库，用于注册和发布应用](https://www.ctolib.com/CanopyTax-single-spa.html)
> 概要: single-spa 是微前端技术的核心库，用于注册和发布应用
### [API Logger - 用于查看api日志的小型laravel软件包，可用于调试](https://www.ctolib.com/aungwinthant-apilogger.html)
> 概要: API Logger - 用于查看api日志的小型laravel软件包，可用于调试
### [Autosize 是一个小的独立脚本，可以自动调整文本区域的高度以适合文本](https://www.ctolib.com/jackmoore-autosize.html)
> 概要: Autosize 是一个小的独立脚本，可以自动调整文本区域的高度以适合文本
### [AuthCov：一款功能强大的开源Web应用程序授权爬行和扫描工具](https://www.tuicool.com/articles/MNjIZvE)
> 概要: AuthCov：一款功能强大的开源Web应用程序授权爬行和扫描工具
### [斑马：如何走向更开放的下一站](https://www.tuicool.com/articles/JNfMFbR)
> 概要: 作者丨王瑞。重组斑马也让阿里在车联网上有了更多的想象空间。错过手机时代的阿里，并不想错过车联网风口。
### [回看主旋律电影三十年——写给祖国的情书](https://www.tuicool.com/articles/ZVjmyer)
> 概要: 本文来自微信公众号。主旋律电影正式提出之后三年，中国电影市场也进入发展提速阶段，并且随着市场打开，全球电影产品尤其是好莱坞电影迅速涌入，政府对于主流意识形态的控制也更进一步重视，内外因交汇之下，主旋律电影的发展升级也更加迅速，并逐步开始注重市场导向因素，影片风格样式更加多样化，其中也有一些影片取得了不错的市场成绩。2000年之后的主旋律电影发展进入更宽阔的赛道，但也不得不承认，在市场化的进程中，也有很大部分依然固守原有创作思维的主旋律影片几乎被排挤在市场之外。
### [1949—2019：家国记忆与日常图景](https://www.tuicool.com/articles/r6BrmmV)
> 概要: 本文来自微信公众号。（比如艺术家张晓刚的“血缘-大家庭”系列）。“我被摄入镜头的中国家庭体现的强烈好奇心、极大的热忱和友善所感动。
### [写作工具 Ulysses 更新，支持最新 iPadOS 特性](https://www.tuicool.com/articles/u22Qnef)
> 概要: 在 10 月到来之前，写作工具. Ulysses 目前支持 Mac 以及 iPad 和 iPhone 设备，这款 Markdown 编辑器的文本库可以进行跨设备同步，而且用户能够以不同的格式（包括 Markdown、HTML、富文本、PDF、ePub、DOCX）导出一份或多份文本，或者发布内容到 Medium、Wordpress 和 Ghost 搭建的博客。半年之前，Ulysses 就已经在 iOS 12 系统里实现了拆分视图浏览，属于开发者自己给出的解决方案。
### [阿里最强 AI 芯片背后，是中国芯片换道超车的开始吗？](https://www.tuicool.com/articles/eA7bAfu)
> 概要: 在拍立淘商品库里，每天要新增 10 亿商品图片。为了这些巨量的图片被准确识别，并给用户正确的反馈，阿里巴巴内部需要 GPU 算力识别 1 小时。成本暂且不提，这个速度对于追求效率阿里似乎还是有些慢了。
### [激荡七十年：奔腾的时代洪流与变迁的个体命运](https://www.tuicool.com/articles/iM7zAnV)
> 概要: 核心要点。下海后的陈东升在多个赛道快速布局：1993年创立第一个全国性的股份拍卖公司中国嘉德，1994年和弟弟陈平一起创立了中国最早的物流和快递企业之一宅急送，1996年创立泰康保险，正式踏入保险业。在前辈陈东升眼中，互联网企业家们的最大贡献是引入了创始人、估值、融资制度。
### [Zig 0.5.0 发布，想要替换 C 的编程语言](https://www.tuicool.com/articles/fqUjUjb)
> 概要: Zig 0.5.0 发布了。与 C 语言竞争而非依赖它，Zig 标准库不依赖于 libc。调试模式下优化了快速编译时间，并在不确定行为发生时使用堆栈跟踪崩溃。
# 小说
### [道逆乾坤](http://book.zongheng.com/book/385335.html)
> 作者：杨家少郎

> 标签：奇幻玄幻

> 简介：【完本】一个满怀屈辱的绝世少年，一场弱者走上强者巅峰的心路历程，金陵岂是池中物，一遇风云化成龙！铁血时代、九次夺元、分身遍布、步步擂台、天地棋局。待看杨辰怎样道逆乾坤，覆天道，成人欲，炼分身，控混沌大势！

> 章节末：【新书】花都掌教

> 状态：完本
### [国士](http://book.zongheng.com/book/316229.html)
> 作者：布丁熊掌

> 标签：历史军事

> 简介：时间正是十三世纪的中叶，南宋王朝在蒙古帝国汹涌的怒涛面前摇摇欲坠。泣血难定中原的孟珙、孤军奋战的曹友闻、丹心守孤城的杜杲......所有的挣扎在历史的滚滚车轮中虚弱无力。国家养士三百年，现在是士子报国的时候了！穿越重生的大宋宰相之子郑云鸣，在这个风云激荡的时代里究竟会选择苟且的逃亡，成为乱世枭雄的野心，还是力挽大厦倾颓的国士？以半壁河山对抗史上最强的铁骑风暴，历史的大戏正在缓缓拉开序幕......

> 章节末：第八十五回 山河重整待后生（10）

> 状态：完本
# 论文
### [Compositional Embeddings Using Complementary Partitions for Memory-Efficient Recommendation Systems](https://paperswithcode.com/paper/compositional-embeddings-using-complementary)
> 日期：4 Sep 2019

> 标签：RECOMMENDATION SYSTEMS

> 代码：https://github.com/facebookresearch/dlrm

> 描述：Modern deep learning-based recommendation systems exploit hundreds to thousands of different categorical features, each with millions of different categories ranging from clicks to posts. To respect the natural diversity within the categorical data, embeddings map each category to a unique dense representation within an embedded space.
### [Visualizing Trends of Key Roles in News Articles](https://paperswithcode.com/paper/visualizing-trends-of-key-roles-in-news)
> 日期：12 Sep 2019

> 标签：

> 代码：https://github.com/kasinxc/Visualizing-Trend-of-Key-Roles-in-News-Articles

> 描述：There are tons of news articles generated every day reflecting the activities of key roles such as people, organizations and political parties. Analyzing these key roles allows us to understand the trends in news.
