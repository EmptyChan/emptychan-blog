---
title: 2019-09-22-每日随机资讯
tags: 资讯
thumbnail: /images/周日.png
date: 2019-09-22 18:53:46
---

# 娱乐
### [不愧是母女，看到两人的长相，粉丝：想妈妈了就照照镜子](https://new.qq.com/omn/20190922/20190922A0CBM900.html)
> 概要: 戚薇lucky不愧是母女，看两人动作爱好，粉丝：想妈妈了就照照镜子.很多人多戚薇很熟悉了，戚薇是一个出名的女演员，之前出演了不少的影视作品，很多人对戚薇都是很熟悉的，但是知道戚薇自生活中是怎样的呢？戚薇和lucky两人的小动作也是很相似的，两人翻白眼的角色还有表情，不得不说两人真不愧是母女，其中在一档节目中主持人为lucky长大后想做什么。回答和戚薇回答的是一模一样。
### [谭咏麟回应被赶出茶餐厅传闻：连我自己也不知道](https://ent.sina.com.cn/s/h/2019-09-22/doc-iicezzrq7612940.shtml)
> 概要: 新浪娱乐讯 北京时间9月22日消息，据香港媒体报导，曾出席撑警集会的谭咏麟（阿伦）前天（9月20日）光顾湾仔一间咖喱小厨时，疑被老板娘赶走，现场食客见状立刻拍手叫好，事件在网上疯传。昨天（9月21日），谭咏麟就被“赶”传闻亲自在微博回应，他写道。(责编：漠er凡)。
### [蕾哈娜晒照引歌手前男友留言 曾对她使用暴力](https://ent.sina.com.cn/y/youmei/2019-09-22/doc-iicezzrq7549225.shtml)
> 概要: 新浪娱乐讯 据外媒报道，Rihanna在社交网站上晒出自家内衣品牌的宣传照，由她自己出镜，穿上内衣裤睡在床上，身边有一盏灯。几小时后，曾于10年前争执后对Rihanna施行家暴的男歌手旧爱Chris Brown忍不住留言。(责编：漠er凡)。
### [宝蓝首曝退出T-ara原因 消失两年：我就是拖油瓶](https://ent.sina.com.cn/y/yrihan/2019-09-22/doc-iicezzrq7629454.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，韩国女团T-ara在2017年历经成员退出、汰换的重大转折，33岁前队长宝蓝便是其中一位离开的人，她近年来几乎消失在萤光幕前，9月20日出现在真人秀《摩登家庭》，她与妹妹宇蓝接受前辈采访，首次谈到退出团体的心情，对于未来充满担忧，甚至觉得自己是拖油瓶。访谈间泄露不安，也让粉丝心疼表示希望她好好的，为她的演员梦加油打气。(责编：漠er凡)。
### [视频：李晨更博晒照2字配文一出再引热议](http://video.sina.com.cn/p/ent/2019-09-22/detail-iicezzrq7627403.d.html)
> 概要: 视频：李晨更博晒照2字配文一出再引热议
### [马德钟将与欧阳震华合演新剧 演律师法院听审取经](https://ent.sina.com.cn/v/h/2019-09-22/doc-iicezueu7536975.shtml)
> 概要: 马德钟透露10月开拍无线新剧饰演律师，他说。“对上一次跟Bobby（欧阳震华）合作已经是《陀枪师姐》，起码都15、20年前，好期待再跟他合作。”。(责编：漠er凡)。
### [《快本》试踩员工劳保鞋，杜江的小细节暴露人品，难怪嗯哼教得好](https://new.qq.com/omn/20190922/20190922A0CE3I00.html)
> 概要: 每周六的黄金时段是《快乐大本营》的播出时间，作为芒果台的王牌综艺，邀请了人气正旺的杜江。但是一个杜江的一个小细节暴露了人品，随后登上了热搜。节目中的一些小细节就能看出嗯哼是一个家教特别好的孩子，不管有什么问题都会发问，时时刻刻不忘说“谢谢”。
### [于正旗下干净的女孩子李一桐，看起来就十分的着人喜欢](https://new.qq.com/omn/20190922/20190922A0CHA600.html)
> 概要: 于正旗下干净的女孩子李一桐，看起来就十分的着人喜欢
### [因撑港警被赶出茶餐厅？谭咏麟的回应，大气！](https://new.qq.com/omn/20190922/20190922A06J9400.html)
> 概要: 据香港文汇网、橙新闻等港媒报道，香港知名歌手谭咏麟之前出席撑警集会引“乱港”分子不满。，“赶谭咏麟离开”并不属实，实则是当时店内人多需要排队，谭咏麟主动选择离开。
### [原以为甜馨就够“整容式成长”当看到奥莉后，超模气质显露无疑](https://new.qq.com/omn/20190922/20190922A0C7UR00.html)
> 概要: 最近芒果卫视播出了一档明星亲子互动节目，一路成年大家应该都有听说，被称为是成人版的爸爸去哪儿。关于亲子互动类的节目，其实早在第一季的爸爸去哪儿开播后，当时在全国的反响都非常的不错，所以后来除了爸爸去哪儿这一档节目后，其他的平台也有效仿和买了一些其他类似亲子节目的版权，而其中最有水花的，应该就是浙江卫视的爸爸回来了。其中neinei的成长，除了爸爸回来了，还有后续参加的爸爸去哪儿。
# 动漫
### [【PV】机战动画《苍穹的法芙娜 The Beyond》剧场版 4-6话PV公开 2019年11月8日先行上映](https://www.acgmh.com/22870.html)
> 概要: 《苍穹之战神》是由XEBEC制作的原创机战动画，动画的第一季曾于2004年播出，后又相继制作了OVA与剧场版，2015年推出了第二季动画《苍穹之法芙娜 EXODUS》。2017年官方宣布将制作全新动画《苍穹的法芙娜 The Beyond》，全12话，并作为剧场版于日本影院先行上映。
### [【STAFF】《织田肉桂信长》制作阵容公开 2020年一月播出](https://www.acgmh.com/28614.html)
> 概要: 《织田肉桂信长》改编目黑川うな著作同名漫画作品，2019年7月宣布动画化决定。动画预定于2020年一月播出。武田信玄、伊达政宗、上杉谦信等人气武将们居然也都转生为小狗!!充满历史趣味、让人心跳不已的知名武将犬的狗狗爆笑喜剧，欢乐登场!! ​​​​. 
### [秋元康×索尼音乐×ANIPLEX 偶像企划《22/7》新视觉绘 PV公开 2020年一月播出](https://news.dmzj.com/article/64693.html)
> 概要: 《22/7》是由秋元康与ANIPLEX、索尼音乐联手组成综合企划的虚拟偶像，由8名虚拟角色和11位声优组成。2019年7月首次 Live 演出中，官方宣布动画化决定，动画预定于2020年一月播出。
### [动画电影《超人：红色之子》首曝剧照 苏联超人亮相](https://news.dmzj.com/article/64700.html)
> 概要: DC新作动画电影《超人：红色之子》曝光了一张苏联超人的剧照，这位前苏联的超级士兵正式登场亮相，胸前标志性的“S”也换成了镰刀与锤子，象征着共产主义。
### [《名侦探柯南》新动画《大怪兽哥美拉VS假面超人》公布！](https://news.dmzj.com/article/64698.html)
> 概要: 动画将于2020年1月播映，后续情报有待公开。
### [《青春期笨蛋不做兔女郎学姐的梦》系列最新刊2020年2月7日发售决定！](https://news.dmzj.com/article/64697.html)
> 概要: 《青春期笨蛋不做兔女郎学姐的梦》系列最新刊2020年2月7日发售决定！
### [千仞雪初见唐三时，有谁注意她说的第一句话？简直是狂妄又真实！](https://new.qq.com/omn/20190922/20190922A0CDY200.html)
> 概要: 嗨，大家好，我是喜欢《斗罗大陆》的朵朵。小伙伴们，昨天更新的最新剧情大家都有看吗？是不是觉得有点水呢？哈哈哈哈！哎，生活嘛水一水更好过嘛，就像七怪一样，不同样被绿的找不到亲爹亲妈嘛！虽然剧情很水，但……
### [新华社摄影部2019年09月22日白班发稿目录](https://new.qq.com/omn/20190922/20190922A0CM5R00.html)
> 概要: 新华社摄影部2019年09月22日白班发稿目录
### [3个细节证明干比谷已死，一方通行入圈套，让人倒吸一口凉气](https://new.qq.com/omn/20190922/20190922A0CH3V00.html)
> 概要: 3个细节证明干比谷已死，一方通行入圈套，让人倒吸一口凉气
### [如果飞段学会了八门遁甲，并开启了死门，他最后会因此死亡吗？](https://new.qq.com/omn/20190922/20190922A0CHUC00.html)
> 概要: 如果飞段学会了八门遁甲，并开启了死门，他最后会因此死亡吗？
### [超妖孽的古风动漫美男子，相思的路长长，相爱的路茫茫](https://new.qq.com/omn/20190922/20190922A0CK5P00.html)
> 概要: 超妖孽的古风动漫美男子，相思的路长长，相爱的路茫茫
### [动漫壁纸：帅气杀生丸，等你来领走！](https://new.qq.com/omn/20190922/20190922A0CBVU00.html)
> 概要: 动漫壁纸：帅气杀生丸，等你来领走！
# 科技
### [Espresso是一个开源的，模块化的，可扩展的端到端神经自动语音识别（ASR）工具箱](https://www.ctolib.com/freewym-espresso.html)
> 概要: Espresso是一个开源的，模块化的，可扩展的端到端神经自动语音识别（ASR）工具箱
### [PubLayNet：大规模文档图像版面分割数据集](https://www.ctolib.com/ibm-aur-nlp-PubLayNet.html)
> 概要: PubLayNet：大规模文档图像版面分割数据集
### [AI Image Creator 是一款可以扩充图片库至40+倍的工具，常用于扩充对象识别的训练集](https://www.ctolib.com/vabaly-ai-image-creator.html)
> 概要: AI Image Creator 是一款可以扩充图片库至40+倍的工具，常用于扩充对象识别的训练集
### [RecyclerView 悬浮吸顶 Header，支持阴影、点击事件与状态绑定](https://www.ctolib.com/smuyyh-StickyHeaderRecyclerView.html)
> 概要: RecyclerView 悬浮吸顶 Header，支持阴影、点击事件与状态绑定
### [如何查询同时包含多个指定标签的文章](https://www.tuicool.com/articles/3eIVfeB)
> 概要: 如何查询同时包含多个指定标签的文章
### [摩拜、饿了么并购后,「胡玮炜、张旭豪」们的下一站](https://www.tuicool.com/articles/V7RfY3b)
> 概要: 在互联网寡头时代，追求网络垄断效应成巨头常用手段，此背景下诞生的一波又一波的并购潮，让人目不暇接。曾在摩拜项目中，被质疑是“花瓶”的胡玮炜，事实上在公司早期组建团队、研发产品以及公司品宣中，都起到了核心的作用。已经让我财务自由，出局也算创业成功”，张旭豪曾表示，对于出局，他已经坦然接受。
### [湖北第一聪明的成功学](https://www.tuicool.com/articles/ji2uiqV)
> 概要: 编者按：本文来自微信公众号华商韬略，作者颜宇，创业邦经授权转载。在程炳浩的开心网掀起全民偷菜风潮的同时，陈一舟不甘落后的参与进来。在开心汽车上市后的一天，80后张一鸣进入了新财富500富人榜前十，在他前面的是排名第九的湖北人雷军，同为湖北籍的周鸿祎排名42。
### [原创Google 抢到量子计算的“霸权”？](https://www.tuicool.com/articles/jA3INra)
> 概要: 量子力学，这个曾经经典物理学天空上的那朵乌云，已经不再只停留在理论阶段，而是对各行各业都带来了实际的影响，量子计算机，就是其中最值得期待的应用之一。这款量子计算机展现出来的算力优势相当恐怖，论文表示，这款计算机可以再 3 分 20 秒内完成计算，而目前世界排名第一的超级计算机“Summit”需要大约一万年的时间。Google 的研究人员表示，这意味着量子计算机可以执行以前不可能完成的计算，“量子霸权”已经实现。
### [阿里淘宝产品面试经验分享：7天走完全流程（意向书已发）](https://www.tuicool.com/articles/yE7v2iq)
> 概要: 阿里淘宝产品面试经验分享：7天走完全流程（意向书已发)
### [从零开始入门 K8s：详解 K8s 容器基本概念](https://www.tuicool.com/articles/uEFfY3y)
> 概要: 容器是什么？容器的生命周期是怎么样的？与 VM 相比，容器的优劣势分别是什么？
### [进击的 Java，云原生时代的蜕变](https://www.tuicool.com/articles/yy6NJj3)
> 概要: 进击的 Java，云原生时代的蜕变
### [加密货币项目融资 形式真的很重要吗？](https://www.tuicool.com/articles/Abq22mU)
> 概要: 金色财经 比特币9月22日讯很多加密项目在融资的时候，似乎更重视融资形式，从早期的初始代币发行（ICO），到近期的初始交易所产品（IEO）和STO。然而虽然每种融资形式都获得了一定的市场份额，而且成功实施代币销售的加密项目数量和IEO/STO数量都出现了显著增加，但二级市场的需求其实并未出现较大增加，人们也不禁想要问：加密项目融资的形式真的这么重要吗？对于那些正在募集资金的加密货币项目来说，现在可能需要做出一些艰难的决定，他们会坚持使用常规的 IEO/STO 方式来融资，还是会另辟蹊径寻找其他更具创新性的策略？
# 小说
### [调戏文娱](https://m.qidian.com/book/1010283730/catalog)
> 作者：幼儿园一把手

> 标签：娱乐明星

> 简介：风属于天的，我借来吹吹，却吹起人间烟火。……新书《和谐游戏》已发布，欢迎品尝。

> 章节总数：共513章

> 状态：完本
### [三少爷的剑](http://book.zongheng.com/book/694365.html)
> 作者：古龙

> 标签：武侠仙侠

> 简介：《三少爷的剑》是古龙的重要经典代表作之一，属于“江湖人”系列，也是古龙最终完成的“江湖人”系列中唯一的一部。在本书中他娴熟地操纵剧情，道尽江湖人的悲哀与苦衷，深化了“人在江湖，身不由己”这一主题，结局有力而深邃，令人回味无穷。天下第一的剑客，神剑山庄三少爷谢晓峰，厌倦了江湖，决定抛弃自己的身份与地位，隐姓埋名，化身为“没用的阿吉”，在社会的最底层打拼。然而人在江湖，身不由己，即使是做一个“没用的阿吉”，也会遇上无法预计的事，最终他不得不重现江湖。

> 章节末：第四十七章 淡泊名利

> 状态：完本
# 论文
### [Scientific Statement Classification over arXiv.org](https://paperswithcode.com/paper/scientific-statement-classification-over)
> 日期：29 Aug 2019

> 标签：

> 代码：https://github.com/dginev/arxiv-statement-classification

> 描述：We introduce a new classification task for scientific statements and release a large-scale dataset for supervised learning. Our resource is derived from a machine-readable representation of the arXiv.org collection of preprint articles.
### [GAN-Tree: An Incrementally Learned Hierarchical Generative Framework for Multi-Modal Data Distributions](https://paperswithcode.com/paper/gan-tree-an-incrementally-learned)
> 日期：11 Aug 2019

> 标签：

> 代码：https://github.com/maharshi95/GANTree

> 描述：Despite the remarkable success of generative adversarial networks, their performance seems less impressive for diverse training sets, requiring learning of discontinuous mapping functions. Though multi-mode prior or multi-generator models have been proposed to alleviate this problem, such approaches may fail depending on the empirically chosen initial mode components.
