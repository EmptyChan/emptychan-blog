---
title: 2019-09-08-每日随机资讯
tags: 资讯
thumbnail: /images/周日.png
date: 2019-09-08 20:57:18
---

# 娱乐
### [梁静茹宣布离婚前演唱到一半掩面痛哭“紧急冲下台”](https://new.qq.com/omn/20190908/20190908A0A41J00.html)
> 概要: 8月15日，天后级歌手梁静茹被曝与丈夫赵元同（Tony）离婚，当时梁本人没有发声，赵元同立即否认事件，称只是夫妻小矛盾没有大问题，而梁静茹的经纪人公司则表示“两人没有离婚”。最关键的爆料是梁静茹好友范玮琪通过经纪人透露，原来梁静茹夫妇在两年前就谈过离婚，但后来具体有没有办成，就没有人知道。此话一出，网友都谴责范玮琪利用朋友炒作，曝光闺蜜隐私，不过梁静茹并没有因此生气，还特意发布了与范玮琪的合照，此举被视为支持范玮琪的爆料。
### [你有明信片待查收！王源开学前大采购打卡家具店](https://ent.sina.com.cn/y/yneidi/2019-09-08/doc-iicezueu4299437.shtml)
> 概要: 9月8日，王源发微博晒出自己的照片，并配文称“终于把这几天的收获布置妥当了。”。照片中的王源在家具店和购买的“收获”合影，床垫被子大件小件一应俱全，王源带着口罩蹲在战利品里直视镜头，寸头利落精神。(责编：麦子七)。
### [周震南敷眼膜自比游乐王子 粉丝：你怎么这个亚子](https://ent.sina.com.cn/y/yneidi/2019-09-08/doc-iicezueu4315420.shtml)
> 概要: 新浪娱乐讯 9月8日，R1SE成员周震南。9月8日，R1SE-周震南发微博晒照片，并配文称“快乐的游乐王子”。(责编：麦子七)。
### [李敖逝世一年半 长女李文曝其遗愿](https://ent.sina.com.cn/s/h/2019-09-08/doc-iicezueu4302936.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，跨足文学界与政治界的李敖。后事要求低调简单处理的他，身后反倒非常不平静，长女李文与儿子李戡因种种问题爆发战火，甚至走上法庭，李敖过世1年半，两人诉讼还没有了结，而李文日前出庭时，见同父异母弟弟聘用的是爸爸的御用律师，想起了父亲，因此曝光李敖“遗愿秘辛”，怒批李敖生前托付的愿想还没有被实践。李敖长女李文与弟弟李戡之间有多场官司正在进行中，日前李文出庭，虽还没有见到李戡，但却看到对方的代理人，正是李敖生前的御用律师，因此想起了父亲，难过透露李敖逝世1年半，李戡除了用父亲留下的资源及御用律师来攻击、污蔑她外，“几乎关于老爸的遗愿（还没有完的书册/大全集后篇、出版集团、李敖两岸文物大陆北京展）及李家人必需成立的李敖文化基金会、李敖博物馆，所有可以将李敖自由思想发扬光大的志业，对方一件也都没做”。
### [组图：张馨予时尚大片挑战多种风格 白色长裙优雅黑色look冷艳](http://slide.ent.sina.com.cn/star/w/slide_4_704_321354.html)
> 概要: 组图：张馨予时尚大片挑战多种风格 白色长裙优雅黑色look冷艳
### [视频：张歆艺曝儿子看熊猫不想回家 萌宝头发浓密肉嘟嘟](http://video.sina.com.cn/p/ent/2019-09-08/detail-iicezzrq4356335.d.html)
> 概要: 视频：张歆艺曝儿子看熊猫不想回家 萌宝头发浓密肉嘟嘟
### [金晨董又霖分手？恋情曝光才六天，两人就发文互相内涵，太快了吧](https://new.qq.com/omn/20190908/20190908A0C6JN00.html)
> 概要: 9月2日恋情曝光当天，董又霖还发文“像一阵夏天的风”疑似回应这段“来得就像龙卷风”的恋爱。但是没想到爱情走得也这么快，如今两人都在社交平台发文，疑似宣布分手，引来了大家的黑人问号。董又霖的粉丝毫不顾忌两人短暂的恋爱，直指金晨找人拍然后曝光，再火速分手，拿着恋情为自己增加热度。
### [林志颖妻子带老三参加生日会，4岁凯凯跪地逗小朋友玩哥哥力爆棚](https://new.qq.com/omn/20190908/20190908A0FGB600.html)
> 概要: 除此之外，陈若仪还晒出了一张老三儿子Kyson陪小baby玩耍的照片。虽说Kyson今年只有四岁大，但从陈若仪晒出的照片来看，小小年纪的他已经很懂得照顾小朋友了，简直哥哥力爆棚，想必这点也是从哥哥KIMI那学来的。除了林志颖老婆陈若仪外，欧弟妻子郑云灿也有现身此次生日聚会，而从她晒出的妈妈们的合照来看，陈若仪的身材可以说超级抢镜了，跪地叉腰的陈若仪一双大长腿又白又直，小蛮腰也很是引人注意，这身材完全看不出像是三个孩子的妈妈！
### [群星拍摄“豪门夜宴图”，李冰冰景甜baby时尚界咖位谁更高？](https://new.qq.com/omn/20190908/20190908A0CHDA00.html)
> 概要: 群星拍摄“豪门夜宴图”，李冰冰景甜baby时尚界咖位谁更高？
### [又炫富了？李湘晒劳斯莱斯幻影豪车，做直播卖卫生巾大圈粉丝钱](https://new.qq.com/omn/20190908/20190908A0BVCG00.html)
> 概要: 这位前湖南卫视当家主持一姐，当真有名的“大富婆”，目前在集主持人、出品人、制片人、导演、360娱乐总裁、深圳卫视副总监、快乐星文化传媒董事长等众多头衔之外，李湘又收获了一个新的身份——网络带货主播。前两天，在某APP直播平台就发现李湘在自己的直播间，跟随直播卖货大潮，竟然在镜头前奋力的卖起卫生巾来了，让人大跌眼镜。隔天就有“李湘卖卫生巾圈粉丝钱”的消息报道，或许今天下午李湘发的这条博文正是对此事的侧面回应。
# 动漫
### [《Fate/Grand Order 绝对魔兽战线 巴比伦尼亚》PV主视觉图公开 2019年十月播出！](https://news.dmzj.com/article/64447.html)
> 概要: 《Fate/Grand Order 绝对魔兽战线 巴比伦尼亚》改编自TYPE-MOON发行的同名游戏，于2018年7月FGO三周年特别直播当中，宣布了新TV和剧场版化的消息。《绝对魔兽战线 巴比伦尼亚》将会改编FGO第七章内容，动画将由Clove...
### [第31号王妃：贫穷女孩的逆袭之路](https://news.dmzj.com/article/64448.html)
> 概要: 本周要介绍的是根据作者桃巴的同名小说《第31号王妃》改编的漫画，描述的是边境出身的乡下姑娘·菲莉娅作为地位最为低等的王妃候补逆袭成为正式王妃的奋斗之路。
### [日系治愈俗套的报恩情节，你有中招吗？](https://news.dmzj.com/article/64438.html)
> 概要: 《蝉男》在短暂的生命中陪你度过夏日，治愈你的悲伤，一起拥抱世界的美好。
### [经典延续《樱桃小丸子》将出续作](https://news.dmzj.com/article/64445.html)
> 概要: 漫画杂志《RIBON》为纪念樱桃子逝世一周年，10月3号出版的杂志会刊登《樱桃小丸子》新作“小丸子搞万圣节派对”，共23页，当中有彩色页，故事来自樱桃子为电视动画写的剧本，该集已于1995年10月29日播映。
### [火影博人传：大筒木浦式轻松秒杀手鞠，佐助还在异世界发呆中](https://new.qq.com/omn/20190908/20190908A0G7RM00.html)
> 概要: 火影博人传：大筒木浦式轻松秒杀手鞠，佐助还在异世界发呆中
### [《魔道祖师》动画大屏合集，国内的很有排场，海外的也毫不逊色！](https://new.qq.com/omn/20190907/20190907A0F0R100.html)
> 概要: 《魔道祖师》动画大屏合集，国内的很有排场，海外的也毫不逊色！
### [《鬼灭之刃》声优豪华，没想到连新人的经历也如此“传奇”](https://new.qq.com/omn/20190908/20190908A0FUK200.html)
> 概要: 《鬼灭之刃》声优豪华，没想到连新人的经历也如此“传奇”
### [叶罗丽：几乎花了七年，文茜才知道想要什么，舒言不是她的唯一](https://new.qq.com/omn/20190908/20190908A0FZ7W00.html)
> 概要: 叶罗丽：几乎花了七年，文茜才知道想要什么，舒言不是她的唯一
### [好想见你，比谁都喜欢着你](https://new.qq.com/omv/video/x09249d9w36)
> 概要: 好想见你，比谁都喜欢着你
### [为什么以前老师课多不觉累，现在课少觉得很累，听佛祖怎么说？](https://new.qq.com/omv/video/v14286vtgfe)
> 概要: 为什么以前老师课多不觉累，现在课少觉得很累，听佛祖怎么说？
# 科技
### [Vizer：方框、蒙版可视化工具](https://www.ctolib.com/lufficc-Vizer.html)
> 概要: Vizer：方框、蒙版可视化工具
### [GGEditor 阿里巴巴开源的基于 G6 和 React 的可视化图编辑器](https://www.ctolib.com/alibaba-GGEditor.html)
> 概要: GGEditor 阿里巴巴开源的基于 G6 和 React 的可视化图编辑器
### [🎉 Design patterns for humans 中文版](https://www.ctolib.com/guanguans-design-patterns-for-humans-cn.html)
> 概要: 🎉 Design patterns for humans 中文版
### [PyTorch Image Retrieval - 用于图像检索任务的PyTorch框架](https://www.ctolib.com/leeesangwon-PyTorch-Image-Retrieval.html)
> 概要: PyTorch Image Retrieval - 用于图像检索任务的PyTorch框架
### [云原生自动机器学习系统 Katib 的设计与实现](https://www.tuicool.com/articles/6JzM7nR)
> 概要: Experiment Controller 会根据自身的状态和关于并行的定义，通过 Katib Manager 提供的 GRPC 接口，让 Manager 通过 Suggestion 提供的 GRPC 接口获取超参数取值，然后再转发给 Experiment Controller。Trial 被创建后，与 Experiment Controller 类似，Trial Controller 同样会通过 Katib Manager 在 Katib DB 中创建一个 Trial 对象。而如果企业希望自己的 超参数搜索与模型结构搜索系统是云原生的，同时非常注重代码的零侵入性 ，Katib 是更好的选择。
### [2022年浙江将建成5G基站8万个](https://www.tuicool.com/articles/J7Rb6bv)
> 概要: 在5G应用示范方面，厉敏表示，浙江在工业互联网、社会治理、智慧交通、智慧医疗、高清视频、车联网等多个领域开展了数百个试点应用，并呈现向广度和深度拓展的态势。此前，浙江出台了《关于加快推进5G产业发展的实施意见》(下称《意见》)，明确5G发展目标，同时聚焦网络建设、产业发展、融合应用三大方面。该省提出，争取到2022年，5G网络的覆盖面和建设水平领先全国，5G产业的规模位居国内的第一方阵，5G在经济社会各领域得到广泛的应用和深度的融合，达到国际领先的水平，构建优良的5G产业生态，成为全国5G网络建设先行区和具有国际重要影响力的5G产业发展的集聚区、5G创新应用的示范区。
### [NBA是如何一步步变成一家「科技公司」的？](https://www.tuicool.com/articles/Jvu2AbE)
> 概要: 勇士队的库里、杜兰特和伊戈达拉等都纷纷跨界投资科技公司。除了勇士队的球员，NBA球员做投资的情况非常多。本文（含图片）为合作媒体授权创业邦转载，不代表创业邦立场，转载请联系原作者。
### [KDE Usability & Productivity: Week 87](https://www.tuicool.com/articles/yuY7jqe)
> 概要: KDE Usability & Productivity: Week 87
### [中国芯片争论：买关键技术还是自己重新研发？](https://www.tuicool.com/articles/RNneMfy)
> 概要: 今年3月，华为子公司海思半导体的芯片在中国福州举行的华为中国生态伙伴大会上展出。芯片设计服务公司芯原微电子创始人兼首席执行官戴伟民指出，“过去两年，中国行业监管机构加大了支持芯片行业发展的力度。“全球半导体供应链是相互关联的，”华大半导体的郝立超表示，“如果所有人都联合起来对付美国，那么美国的芯片产业也将难逃厄运。”。
### [知料 | 诺基亚翻盖机重出江湖，有人愿意用回功能机吗？](https://www.tuicool.com/articles/JBV7Vbv)
> 概要: 价格便宜只是原因之一，据传音早前招股书显示，传音功能手机在2018年的平均售价为65.95元，智能手机均价也只有454.38元，远低于全球平均价格。而另一方面，或许是因为日本传统功能机发展较早，在智能手机潮尚未席卷全球的时代，日本最大运营商NTT DOCOMO就主导了一个以i-mode为核心功能机软硬件封闭的移动生态，用户可以用功能机上网、NFC支付等。此外，还有许多新品牌也专注于功能手机市场，法国运营商orange SA就计划将智能功能机推广到非洲及中东市场。
### [TWINT：一款Twitter信息爬取工具](https://www.tuicool.com/articles/yaeQrmI)
> 概要: TWINT：一款Twitter信息爬取工具
### [Parallelism in Crystal](https://www.tuicool.com/articles/y2uYfqr)
> 概要: If the awaiting fiber was in a thread that was sleeping (no runnable fibers available) ; the same pipe used to deliver new fibers is used to enqueue the fiber in the sleeping thread ; waking it up .that will pop fibers from the runnable queue or ; if empty ; will wait until a fiber is sent through the pipe of that worker thread .The strongest guarantee would be similar to serializing its access (think of a Mutex around every method) ; a weaker guarantee would be that access is not serialized but will always lead to consistent state (think that every call will produce a consistent final state ; but there is no guarantee which one will be the one used) ; and finally ; the void guarantee that is allowing interleaved manipulation of the state .
# 小说
### [帝妃天下：皇上，宠上身](http://book.zongheng.com/book/791439.html)
> 作者：北国婖婖

> 标签：评论文集

> 简介：悬剑北门，弃宫而去；乐晓婖这个花痴女穿越到古代，成了皇帝的才人。区区才人，区区皇宫，怎能拦得住她花痴的心？邂逅美男，撩拨正太，活的轻松潇洒，却不知早已陷入一场宫廷阴谋，天地为之变色！她竭尽全力，帮助那至尊之人坐稳皇位，本以为可与他比翼双飞，却不知，她不过是他心中那人的替身……

> 章节末：第五十二章 大结局

> 状态：完本
### [松本清张经典推理大全集](http://book.zongheng.com/book/792815.html)
> 作者：松本清张

> 标签：悬疑灵异

> 简介：本套装包含： 《十万分之一的偶然》 《富士山禁恋》 《一个背叛日本的日本人》 《死之枝》 《苍白的轨迹：箱根温泉杀人手稿》 《D之复合》《死亡螺旋》 《交错的场景》 《隐花平原》 《时间的习俗》

> 章节末：时间的习俗_18

> 状态：完本
# 论文
### [LXMERT: Learning Cross-Modality Encoder Representations from Transformers](https://paperswithcode.com/paper/lxmert-learning-cross-modality-encoder)
> 日期：20 Aug 2019

> 标签：LANGUAGE MODELLING

> 代码：https://github.com/airsplay/lxmert

> 描述：Vision-and-language reasoning requires an understanding of visual concepts, language semantics, and, most importantly, the alignment and relationships between these two modalities. We thus propose the LXMERT (Learning Cross-Modality Encoder Representations from Transformers) framework to learn these vision-and-language connections.
### [Feature Robustness in Non-stationary Health Records: Caveats to Deployable Model Performance in Common Clinical Machine Learning Tasks](https://paperswithcode.com/paper/feature-robustness-in-non-stationary-health)
> 日期：2 Aug 2019

> 标签：LENGTH-OF-STAY PREDICTION

> 代码：https://github.com/MLforHealth/MIMIC_Generalisation

> 描述：When training clinical prediction models from electronic health records (EHRs), a key concern should be a model's ability to sustain performance over time when deployed, even as care practices, database systems, and population demographics evolve. Due to de-identification requirements, however, current experimental practices for public EHR benchmarks (such as the MIMIC-III critical care dataset) are time agnostic, assigning care records to train or test sets without regard for the actual dates of care.
