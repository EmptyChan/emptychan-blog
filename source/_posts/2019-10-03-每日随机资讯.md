---
title: 2019-10-03-每日随机资讯
tags: 资讯
thumbnail: /images/周四.png
date: 2019-10-03 16:39:17
---

# 娱乐
### [生活重心不同！凯莉詹娜与Travis Scott正式分手](https://ent.sina.com.cn/s/u/2019-10-03/doc-iicezzrq9851336.shtml)
> 概要: 新浪娱乐讯  10月3日，据外国媒体报道，知名真人秀女星凯莉·詹娜Kylie Jenner与嘻哈歌手男友Travis Scott正式分手。随后外媒拍到凯莉·詹娜和Travis Scott分手之后，出现在前任Tyga工作的录音室。(责编：Blue)。
### [组图：蜜月日常！比伯新婚后和娇妻海莉甜蜜看电影](http://slide.ent.sina.com.cn/y/slide_4_704_323140.html)
> 概要: 组图：蜜月日常！比伯新婚后和娇妻海莉甜蜜看电影
### [组图：王思聪带两美女游成都 黄发粉衣女子酷似昆凌](http://slide.ent.sina.com.cn/star/hr/slide_4_704_323149.html)
> 概要: 组图：王思聪带两美女游成都 黄发粉衣女子酷似昆凌
### [组图：温碧霞国外游玩笑靥如花 身材婀娜心情大好](http://slide.ent.sina.com.cn/star/w/slide_4_704_323144.html)
> 概要: 组图：温碧霞国外游玩笑靥如花 身材婀娜心情大好
### [吴世勋空降粉丝群聊 晒自拍仍被当成是假账号](https://ent.sina.com.cn/s/j/2019-10-03/doc-iicezzrq9834437.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，韩国粉丝圈中不少人是透过“孤独房”交流，本来只是通讯软件建立聊天室功能，许多粉丝开房只为了进来群聊、晒图，却也偶尔会吸引明星本尊现身。EXO成员吴世勋日前就空降“孤独房”聊天室，甚至同时不只进去了一个，但他也因为这样被当作骗子，遭到粉丝踢出聊天室。(责编：Blue)。
### [女爱豆父亲发帖揭露CJ的恶行，小孩的梦想怎么办！](https://new.qq.com/omn/20191003/20191003A05S8N00.html)
> 概要: 韩网热议，李海印父亲在李海印gall发的帖子(刚揭露的CJ的恶行)。拍摄偶像学校期间让她签专属合同，因为担心不签合同的话会落选，所以签了。但因为暗箱操作被淘汰，之后说要在2018年10月之前让她和落选的其他几个孩子出道，以此为由把她留在公司，然后一直不管，据说偶像学校结束后，其他经纪公司也多次联系过她，但是因为出道约定+专属合约都拒绝了。最终被搁置，今年夏天离开所属公司。
### [40岁吴佩慈怀四胎出行 带两儿子参加生日聚会四肢纤细身轻如燕](https://new.qq.com/omv/video/o3003uhn8ia)
> 概要: 40岁吴佩慈怀四胎出行 带两儿子参加生日聚会四肢纤细身轻如燕
### [欧阳娜娜生图曝光，没了美颜跟滤镜，这颜值你能打几分？](https://new.qq.com/omv/video/n3003mvdbmi)
> 概要: 欧阳娜娜生图曝光，没了美颜跟滤镜，这颜值你能打几分？
### [一个时代落幕，AKB48播了11年的节目正式完结](https://new.qq.com/omn/20191003/20191003A00QTD00.html)
> 概要: 对于AKB48的粉丝来说，《AKBINGO！》是一档有着重要意义的节目，而这档节目在开播11年后，本周正式迎来了完结。因为总制作人秋元康与电视台的良好关系，AKB48在还没有走红的2008年就拥有了自己的第一个冠名节目，就是开播于2008年1月24日的《AKBINGO！
### [电视里四十岁的女性都去哪儿了？我们跟制片人徐晓鸥聊了聊](https://new.qq.com/omn/20191003/20191003A03LEL00.html)
> 概要: 2019年是柠萌影业的大年，既有当之无愧的爆款《小欢喜》，也有探索新题材的电竞大IP《全职高手》。准备了两年多的大剧《九州缥缈录》在经历“介质问题”的曲折之后终于也播出了，虽然收视不如预期——毕竟这个夏天更火的是《长安十二时辰》和《陈情令》。相比之下，临时改档的《九州缥缈录》表现只能说是不温不火。
# 动漫
### [《我们的七日战争》正式预告、海报、主题歌解禁](https://news.dmzj.com/article/64849.html)
> 概要: 剧场版动画《我们的七日战争》的正式预告、正式海报和主题歌都已经解禁了。
### [板垣巴留监制《BEASTARS》雷格西手办开始预约！](https://news.dmzj.com/article/64848.html)
> 概要: 根据板垣巴留原作改编的TV动画《BEASTARS》中大灰狼雷格西的手办将由Megahouse制作发售。
### [fate：金闪闪，娘闪闪，幼闪闪，哪个才是你的菜？](https://new.qq.com/omn/20191002/20191002A08SHD00.html)
> 概要: fate：金闪闪，娘闪闪，幼闪闪，哪个才是你的菜？
### [鬼灭之刃：赫刀对鬼造成的伤害无法恢复，击败鬼舞辻的伏笔已埋下](https://new.qq.com/omn/20191003/20191003A0AB2300.html)
> 概要: 鬼灭之刃：赫刀对鬼造成的伤害无法恢复，击败鬼舞辻的伏笔已埋下
### [5个动漫中一米五以下的萌女，个个娇小玲珑，你喜欢谁？](https://new.qq.com/omn/20190926/20190926A0EBKN00.html)
> 概要: 5个动漫中一米五以下的萌女，个个娇小玲珑，你喜欢谁？
### [食戟之灵最终卷发售，四位著名漫画家送上贺图，尾田继续放飞自我](https://new.qq.com/omn/20191003/20191003A09U7I00.html)
> 概要: 《食戟之灵》原本是一部讲述美食的动漫作品，也就是普通中学生幸平创真制霸美食界的故事，由于角色品尝美食时的夸张举动，这部番也有了另外的别称，如今这部作品已经发售了最终36卷，四位知名漫画家也纷纷送上了……
### [远古超赛番外：远古赛亚人那么强大，为何现在懦弱不堪？](https://new.qq.com/omn/20191003/20191003A0AKQQ00.html)
> 概要: 赛亚人号称战斗民族，族人中传说中的超级战士更是厉害无比，号称宇宙最强。然而超级赛亚人的出现似乎并没有想象中那么优秀，依旧有很多种族的战士能够击败赛亚人。那么为什么赛亚人会号称宇宙最强呢？寻找原因还得……
### [我的英雄学院：关键女孩惠理“个性”，为终结埋下了伏笔？](https://new.qq.com/omn/20191003/20191003A08GK800.html)
> 概要: 在过去，超能力是架空的，直至一名“发光的婴儿”出生，世界各地就陆续有“超常”的现象，架空的超能力变成了现实。根据轰君“个性婚姻”的说法，个性的发展方向肯定是一代比一代强。当然了，这并不是特指每个儿子……
# 科技
### [A tensorflow reproducing of paper “Editing Text in the wild”](https://www.ctolib.com/youdao-ai-SRNet.html)
> 概要: A tensorflow reproducing of paper “Editing Text in the wild”
### [flutter_mp的目标是把Flutter扩展到类小程序平台](https://www.ctolib.com/areslabs-flutter_mp.html)
> 概要: flutter_mp的目标是把Flutter扩展到类小程序平台
### [gitamine是Git的现代图形用户界面。](https://www.ctolib.com/pvigier-gitamine.html)
> 概要: gitamine是Git的现代图形用户界面。
### [一个基于django、nodejs、vue的websocket实时点对点通讯项目](https://www.ctolib.com/aeasringnar-chat-app.html)
> 概要: 一个基于django、nodejs、vue的websocket实时点对点通讯项目
### [扎克伯格：抵触巨头拆分，开发抖音竞品](https://www.tuicool.com/articles/u2a6Vjz)
> 概要: 【猎云网（微信号：）】10月3日报道（编译：郑意）。扎克伯格还认真谈论了公司现在面临的挑战，包括Facebook今年异常长的低谷期，以及公司对其3万名合同工所负有的责任。虽然会议中讨论的许多问题都非常严肃，但扎克伯格也在试图缓和气氛。
### [谷歌的胜利:微软Surface Duo依靠其系统和浏览器技术](https://www.tuicool.com/articles/RRVRVbU)
> 概要: 微软今天发布了双屏Android智能手机Surface Duo。这款手机的浏览器和移动操作系统所基于的技术均由谷歌提供，因而这堪称谷歌的一场重大胜利。当被问及对微软将会推出的Surface Duo有何看法时，谷歌的发言人表示，“我们非常欢迎微软加入Android生态系统”。
### [自动驾驶近期行为预测和规划的一些文章介绍（附录）](https://www.tuicool.com/articles/N32Y3ua)
> 概要: 继续讨论一些驾驶行为规划和预测的文章，作为附录吧。这里强化学习、生成对抗网络和LSTM的概念和方法会频繁出现。对多代理的交互行为建模时，未来轨迹的协调识别（Coordination recognition）和精细模式预测（subtle pattern prediction）将发挥重要作用。
### [只需5美元，谷歌就可以使用你的人脸数据五年，专找流浪汉采集](https://www.tuicool.com/articles/aiINnaA)
> 概要: 在面部识别饱受争议，并逐渐被法规所限制的情况下，谷歌是如何寻找数据的？近日，《纽约每日新闻》爆出的调查结果让人们感到有些惊讶：谷歌的有偿「扫脸」计划包括很多流浪汉，而一些大学生在不知情的情况下把自己的面部特征提供给了谷歌。在接受采访时，谷歌项目的工作人员却承认，因为谷歌对于数据的追求，他们正在使用一些可疑、具有误导性的方法。
### [清洗前CEO核心圈子 WeWork任命日本业务新负责人](https://www.tuicool.com/articles/MVjaYrE)
> 概要: 新浪科技讯 北京时间10月3日早间消息，WeWork本周任命了新的日本业务负责人，取代与前CEO亚当·诺依曼（Adam Neuman）关系密切的公司资深人士。尽管WeWork没有按地区披露营收，但日本是该公司最重要的市场之一，尤其考虑到日本是WeWork最大股东软银集团的所在地。在针对诺依曼核心圈子的管理层调整中，公司副董事长迈克尔·格罗斯（Michael Gross）和房地产投资部门联合负责人温迪·希尔维斯坦（Wendy Silverstein）也已离职。
### [中国2.4亿单身群体中，你“单身经济”了吗？](https://www.tuicool.com/articles/jUBFVjR)
> 概要: 【猎云网（微信号：） 】10月3日报道（文/周晓莉）。这类人中很大一部分是为了享受生活而选择主动单身，在他们看来，“悦人不如悦己”。单身经济快速发展下，潜在问题也应该得到社会注意。
### [涉金融服务数据分析行业的价值与善治](https://www.tuicool.com/articles/zIzAfum)
> 概要: 涉金融服务数据分析行业的现状。由于金融业是数据分析机构的主要市场，对数据分析机构采取适当的监管措施也是必要的，以合规供应商清单的方式促进数据分析行业的发展。
### [爆款APP：我太难了](https://www.tuicool.com/articles/YJjyuuQ)
> 概要: 如今，构建在互联网之上的商业公司改变了商业形态和公司行为，也改变了我们的生活。人们被更深地卷入到一个交织着互联网的消费社会之中。投中网梳理还发现“爆款”应用越来越多出自大公司内部孵化。
# 小说
### [武道圣主](http://book.zongheng.com/book/567517.html)
> 作者：轻浮你一笑

> 标签：奇幻玄幻

> 简介：他是花果山一代妖王！他是一棒打碎凌霄的齐天大圣！他是灵山正果的斗战胜佛！他是百万年来的第六位圣人！第一书友群（423324204）欢迎兄弟姐妹们入群！！   然而当一切的一切都因为一场突如其来的穿越改变，他法力尽失，法宝全无，不灭金身不复，成为了一个游手好闲、臭名昭著的二世祖！   他不甘！他还要重新踏上成圣之路，回到三界，完成自己的夙愿，于是他开始走上了一条异世修真的风云再起之路。

> 章节末：终章感言！

> 状态：完本
### [无敌药神](http://book.zongheng.com/book/415499.html)
> 作者：阙声云舵

> 标签：都市娱乐

> 简介：美女统统来暖床，男人不甘当小弟就统统镇压，这便是彪悍的无敌药神，是一个笑中带泪的故事！（友情提示与免责声明，阅读此书需六根清静，否则出现笑疯与哭晕的男读者，我一概不负责，至于女读者，请放心，我会挨个治疗…）

> 章节末：第306章：大结局

> 状态：完本
# 论文
### [Learning Dynamic Author Representations with Temporal Language Models](https://paperswithcode.com/paper/learning-dynamic-author-representations-with)
> 日期：11 Sep 2019

> 标签：INFORMATION RETRIEVAL

> 代码：https://github.com/edouardelasalles/dar

> 描述：Language models are at the heart of numerous works, notably in the text mining and information retrieval communities. These statistical models aim at extracting word distributions, from simple unigram models to recurrent approaches with latent variables that capture subtle dependencies in texts.
### [Minimum Delay Object Detection From Video](https://paperswithcode.com/paper/minimum-delay-object-detection-from-video)
> 日期：29 Aug 2019

> 标签：OBJECT DETECTION

> 代码：https://github.com/donglao/mindelay

> 描述：We consider the problem of detecting objects, as they come into view, from videos in an online fashion. We provide the first real-time solution that is guaranteed to minimize the delay, i.e., the time between when the object comes in view and the declared detection time, subject to acceptable levels of detection accuracy.
