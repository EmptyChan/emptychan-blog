---
title: 2019-08-22-每日随机资讯
tags: 资讯
thumbnail: /images/周四.png
date: 2019-08-22 22:47:54
---

# 娱乐
### [涉嫌猥亵张紫妍记者一审被判无罪 法院不认其嫌疑](https://ent.sina.com.cn/s/j/2019-08-22/doc-ihytcern2707118.shtml)
> 概要: 新浪娱乐讯 8月22日，首尔中央地方法院对因涉嫌强制猥亵而被起诉的前记者A某宣判无罪。虽然A某在时隔10年之后的2018年被起诉，但法院不认可A某的嫌疑。
### [外国网友追《九州》 路透社赞“优秀的东方史诗”](https://ent.sina.com.cn/v/m/2019-08-22/doc-ihytcern2674626.shtml)
> 概要: 在人人唱衰“周播剧场”的时下，《九州缥缈录》凭借高燃剧情冲破了周播壁垒，收视一度超过黄金档。“《九州缥缈录》是中国古装剧在基于本土文化的基础上，在制作、故事、人物塑造上，进行了一次全面的升级，是一部优秀的东方史诗剧”，这是路透社提到该剧给出的评价。，以往一直是古装剧更容易在海外圈粉，所以《九州缥缈录》是首先就占了古装剧的“天时”吗？
### [视频：何洁黑白套装裙身材有点虐？怀抱玩具熊少女心不减](http://video.sina.com.cn/p/ent/2019-08-22/detail-ihytcern2631082.d.html)
> 概要: 视频：何洁黑白套装裙身材有点虐？怀抱玩具熊少女心不减
### [组图：朱丹踩平底鞋衣着宽松现身机场 素颜带娃获家人贴心陪护](http://slide.ent.sina.com.cn/z/v/slide_4_704_320017.html)
> 概要: 组图：朱丹踩平底鞋衣着宽松现身机场 素颜带娃获家人贴心陪护
### [组图：海莉系红色高跟鞋帅酷出街 穿黑色西装露事业线又美又飒](http://slide.ent.sina.com.cn/y/slide_4_704_320053.html)
> 概要: 组图：海莉系红色高跟鞋帅酷出街 穿黑色西装露事业线又美又飒
### [实力派演员辛芷蕾拍戏吃路边摊 拍完马上全部吐出](https://new.qq.com/omv/video/f09173d0t71)
> 概要: 实力派演员辛芷蕾拍戏吃路边摊 拍完马上全部吐出
### [乔碧萝之后，斗鱼又一名“坦克级”女主播火了，直播2周赚10万](https://new.qq.com/omn/20190822/20190822A0PIX400.html)
> 概要: 要按理说，现在的人看直播都会追求游戏技术、搞笑、才艺、颜值之类的点，就这位主播而言，很显然颜值已经跟她没有了任何关系。跟你聊天的“快乐女孩”说不定是个35岁的姐姐。跟你打游戏的“火箭仔”说不定是个45的中年男人。
### [南湖之畔奏响建国70周年礼赞！听说，这个夏夜“小鲜肉”和音乐更配哦](https://new.qq.com/omn/20190822/20190822A0PMQ700.html)
> 概要: 8月20日晚，“礼赞新中国·青春心向党”云霄县大学生献礼新中国70华诞走基层慰问演出在南湖生态园红砖广场激情唱响。舞蹈《forever young》、《花开盛世》、《礼仪之邦》跳出青春的节拍；诗朗诵《青春不停步、永远跟党走》述说着家乡云霄的发展故事······精彩纷呈的文艺节目轮番上演，让台下观众目不暇接、叫好连连。
### [郑爽重演“楚雨荨”与张翰隔空同框，现男友张恒：这环节我安排的](https://new.qq.com/omn/ENT20190/ENT2019082200800800.html)
> 概要: 对于与男友张恒携手上节目，郑爽表示。之后，郑爽call爸爸及张恒上台，频使眼色，在台上与张恒更是十指紧扣，十分恩爱。谈到在下午的生日会上重演“楚雨荨”，透过大屏幕隔空与当年的张翰同框，是否代表着已经放下时，郑爽还未表态，男友张恒就开启“护花模式”，表示。
### [冯提莫有多不好惹？周传雄因这点得罪其粉丝，网友直呼：你也配！](https://new.qq.com/omn/20190822/20190822A0PMZG00.html)
> 概要: 周传雄因这点得罪其粉丝，网友直呼：你也配！正所谓人红是非多，冯提莫凭借“佛系少女”为自己的音乐路成功打下知名度，随后自己在重庆的演唱会上一口气唱出自己20多首原创歌曲也是让粉丝大饱耳福，所以说，冯提莫在音乐领域上也算得上是个小才女了，而网络所说的青铜评价大神周传雄更是无稽之谈，只不过是被有意之人恶意造谣罢了。随着事件热度上升后，冯提莫的粉丝也出来为其撑腰，纷纷表示：周传雄老师的作品虽好，但不少粉丝跑来骂冯提莫来刷热度的行为实在是让人愤怒！
# 动漫
### [寿屋《盾之勇者成名录》拉芙塔莉雅1/7比例手办开订](https://news.dmzj.com/article/64052.html)
> 概要: 寿屋根据TV动画《盾之勇者成名录》中的拉芙塔莉雅制作的1/7比例手办目前已经开订了。这次采用了长大后的拉芙塔莉雅身着战斗用装备举着剑时的造型。其中剑有魔法铁剑和魔力剑两种可以任意替换。
### [动画《PSYCHO-PASS》第一季舞台剧化！10月25日开始上演](https://news.dmzj.com/article/64053.html)
> 概要: 动画《PSYCHO-PASS》宣布了新舞台剧化决定的消息，真人舞台剧《PSYCHO-PASS Chapter1 犯罪系数》即将于10月25日至11月10日在东京上演。
### [【游戏新鲜事】科隆展大作云集 Origin迎史低折扣](https://news.dmzj.com/article/64057.html)
> 概要: 《游戏新鲜事》是由幕外编辑部出品的游戏资讯快报栏目~如果大家对视频有什么意见或者建议，欢迎大家留言告诉小编，我们一定尽快，尽好的给各位带来最优质的游戏资讯。留下你对视频的建议，比如视频哪里需要改善，你们想要看到什么类的资讯，每周我们都将抽出一个幸...
### [真人电影《地狱少女》公开主要角色定妆照](https://news.dmzj.com/article/64044.html)
> 概要: 真人电影《地狱少女》公开了阎魔爱的全身定妆照与骨女、一目连、轮入道的定妆照。其中由玉城TINA饰演的阎魔爱，有身着校服和和服两种服装时的照片。
### [火影忍者：月光疾风和夕颜](https://new.qq.com/omn/20190822/20190822A0PH4D00.html)
> 概要: 火影忍者：月光疾风和夕颜
### [不存在的天九17 韩非单挑叫爹局 老谋深算翡翠虎](https://new.qq.com/omv/video/m0917tje01m)
> 概要: 不存在的天九17 韩非单挑叫爹局 老谋深算翡翠虎
### [《非人哉》白泽调侃哪吒“思想简单”，自己根本不用看他的日记！](https://new.qq.com/omn/20190822/20190822A0PEI100.html)
> 概要: 《非人哉》白泽调侃哪吒“思想简单”，自己根本不用看他的日记！
### [2019霜月祭CM，最后再燃烧一次吧!](https://new.qq.com/omv/video/e0917ienx6b)
> 概要: 2019霜月祭CM，最后再燃烧一次吧!
### [八年更了二十七集，没有人弃番，这个动漫为什么如此优秀？](https://new.qq.com/omn/20190822/20190822A0PDS000.html)
> 概要: 八年更了二十七集，没有人弃番，这个动漫为什么如此优秀？
### [破纪录的为什么是它？《哪吒之魔童降世》打破偏见创造历史](https://new.qq.com/omn/20190822/20190822A0PIWF00.html)
> 概要: 破纪录的为什么是它？《哪吒之魔童降世》打破偏见创造历史
# 科技
### [具有连接支持和QQ绑定API的Spring社交扩展](https://www.ctolib.com/school-days-spring-social-qq.html)
> 概要: 具有连接支持和QQ绑定API的Spring社交扩展
### [如何从头开始编写编译器](https://www.ctolib.com/DQNEO-HowToWriteACompiler.html)
> 概要: 如何从头开始编写编译器
### [本代码使用python的pynput库实现"键盘控制鼠标"](https://www.ctolib.com/ywsswy-mouseless.html)
> 概要: 本代码使用python的pynput库实现"键盘控制鼠标"
### [从命令行设置Unsplash的最新壁纸](https://www.ctolib.com/deepjyoti30-QuickWall.html)
> 概要: 从命令行设置Unsplash的最新壁纸
### [十年铲码，八大体系超千篇数百万字技术笔记系列汇总（GitBook 悦享版）](https://www.tuicool.com/articles/INjQ3mR)
> 概要: 知识体系：《. Rust 实战. Web 与大前端：《. 
### [Fossil: Fossil Versus Git](https://www.tuicool.com/articles/Zn2eEbu)
> 概要: Fossil: Fossil Versus Git
### [Nature解析中国AI现状，2030年能引领全球吗？](https://www.tuicool.com/articles/biEFR3E)
> 概要: 中国对于人工智能领域的追求更像是和美国竞争的形式化表现，一些科学家说。除了中国的学术影响力在日益增长外，中国人工智能产业也在蓬勃发展。另一方面，有 53% 的中国公司已经在开展人工智能应用的试点，这一数据也大大领先第二名美国（29%）。
### [一线丨李斌内部信：9月底前蔚来裁员1200人 将聚集核心业务](https://www.tuicool.com/articles/7nEvmeu)
> 概要: 按照进一步的精益运营计划，九月底前公司在全球范围内将减少1200个工作岗位。我们新增用户中近一半来自车主的推荐，在今天蔚来面临这么多质疑的情况下，用户的认可和信任是我们前进的动力。即使这样，我们在这些城市也都有上百用户。
### [Apple removes ‘Designed by Apple in California’ book from US online store](https://www.tuicool.com/articles/ymMBVvV)
> 概要: The text on that webpage invites users to buy the book , even though it is seemingly no longer available .It is possible that Apple printed a certain number of the books and has since run out of stock .
### [Apple Card can be damaged by wallets and jeans](https://www.tuicool.com/articles/3eeQBzE)
> 概要: The Apple Card is a relatively plain matt white credit card made of titanium , which was designed to stand out compared to other credit cards .End of Twitter post by @KeepUpWAngel. 
### [Ray tracing in Excel](https://www.tuicool.com/articles/nmu2ayV)
> 概要: This wild person who goes by the username s0lly has made a ray tracing demo inside Microsoft Excel .To avoid getting bogged down in the technicalities here , I will let. It seems that Excel is not the most efficient way to render ray traced graphics .
### [谁是下一个流量黑洞？](https://www.tuicool.com/articles/2eyM7vu)
> 概要: 我们尝试以社交和信息流两类App的流量变化为主线，来描述整个移动互联网流量搅动。当然，在这一过程中，去中心化加上内容的爆炸，用户可选择的信息呈几何级数爆炸，大量信息反而形成了干扰，这又成为各大平台需要解决的问题。可以发现，除了新兴的信息流广告在大幅放量，搜索广告、品牌图形广告、视频贴片广告等形式都在缩量，尤其是一直强势的搜索广告缩量的速度让人惊讶，难怪百度要All in信息流。
# 小说
### [虫临暗黑](https://m.qidian.com/book/1010945706/catalog)
> 作者：猎魔猫

> 标签：游戏异界

> 简介：地狱之内，三魔神带着四小弟缩到熔岩河的角落边瑟瑟发抖，在它们面前，无穷无尽的虫群分开了一条道路，一个身穿印花睡衣的家伙睡眼蓬松的走到他们面前。“嗯，不错，一觉睡醒，该搞定的都搞定了...那么接下来......”他的目光转向天空，脸上露出了不怀好意的笑容。高阶天堂之上，英普瑞斯突然浑身一抖，由心底子里发出了一丝寒意。“不好！莫非那万界之灾要上天？!”（新书新人~求关爱~求照顾~求包养~）

> 章节总数：共1814章

> 状态：完本
### [我在天堂等你](http://book.zongheng.com/book/681846.html)
> 作者：裘山山

> 标签：历史军事

> 简介：离休将军欧战军在得知三女儿有外遇闹离婚，小儿子经营的超市被查封的消息后，召开了一次家庭会议。会议不欢而散，欧战军突发脑溢血，不治身亡。欧家陷入混乱。沉默寡言的母亲白雪梅终于开口，讲述了五十年前，那群进藏的女兵的真实故事，解开了萦绕在六个子女心中的身世之谜。半个世纪的时空交错，三代人生存环境和观念的巨大落差，世界屋脊瑰丽奇异的自然风光，以及那遥远的、轰鸣在进藏女兵身上纯真而动人的爱情圣歌，构成了色彩斑谰的时代画面……

> 章节末：第十五章

> 状态：完本
# 论文
### [FSS-1000: A 1000-Class Dataset for Few-Shot Segmentation](https://paperswithcode.com/paper/fss-1000-a-1000-class-dataset-for-few-shot)
> 日期：29 Jul 2019

> 标签：FEW-SHOT IMAGE SEGMENTATION

> 代码：https://github.com/HKUSTCV/FSS-1000

> 描述：Over the past few years, we have witnessed the success of deep learning in image recognition thanks to the availability of large-scale human-annotated datasets such as PASCAL VOC, ImageNet, and COCO. Although these datasets have covered a wide range of object categories, there are still a significant number of objects that are not included.
> Can we perform the same task without a lot of human annotations? In this paper, we are interested in few-shot object segmentation where the number of annotated training examples are limited to 5 only. To evaluate and validate the performance of our approach, we have built a few-shot segmentation dataset, FSS-1000, which consists of 1000 object classes with pixelwise annotation of ground-truth segmentation. Unique in FSS-1000, our dataset contains significant number of objects that have never been seen or annotated in previous datasets, such as tiny daily objects, merchandise, cartoon characters, logos, etc. We build our baseline model using standard backbone networks such as VGG-16, ResNet-101, and Inception. To our surprise, we found that training our model from scratch using FSS-1000 achieves comparable and even better results than training with weights pre-trained by ImageNet which is more than 100 times larger than FSS-1000. Both our approach and dataset are simple, effective, and easily extensible to learn segmentation of new object classes given very few annotated training examples. Dataset is available at https://github.com/HKUSTCV/FSS-1000.
### [Modulation of early visual processing alleviates capacity limits in solving multiple tasks](https://paperswithcode.com/paper/modulation-of-early-visual-processing)
> 日期：29 Jul 2019

> 标签：OBJECT DETECTION

> 代码：https://github.com/novelmartis/early-vs-late-multi-task

> 描述：In daily life situations, we have to perform multiple tasks given a visual stimulus, which requires task-relevant information to be transmitted through our visual system. When it is not possible to transmit all the possibly relevant information to higher layers, due to a bottleneck, task-based modulation of early visual processing might be necessary.
> In this work, we report how the effectiveness of modulating the early processing stage of an artificial neural network depends on the information bottleneck faced by the network. The bottleneck is quantified by the number of tasks the network has to perform and the neural capacity of the later stage of the network. The effectiveness is gauged by the performance on multiple object detection tasks, where the network is trained with a recent multi-task optimization scheme. By associating neural modulations with task-based switching of the state of the network and characterizing when such switching is helpful in early processing, our results provide a functional perspective towards understanding why task-based modulation of early neural processes might be observed in the primate visual cortex
