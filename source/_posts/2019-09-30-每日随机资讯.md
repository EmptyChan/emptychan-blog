---
title: 2019-09-30-每日随机资讯
tags: 资讯
thumbnail: /images/周一.png
date: 2019-09-30 13:22:12
---

# 娱乐
### [肖战成立工作室了，X玖少年团还好吗？](https://new.qq.com/omn/20190930/20190930A009IB00.html)
> 概要: 29日，肖战工作室发布首支营业视频，正式宣布成立。视频中肖战的剪影帅气出镜，据悉，结尾处的logo更是由本人亲自设计的。
### [组图：45岁郭羡妮穿一袭水晶裙风韵犹存 自曝忘记结婚纪念日](http://slide.ent.sina.com.cn/star/slide_4_704_322992.html)
> 概要: 组图：45岁郭羡妮穿一袭水晶裙风韵犹存 自曝忘记结婚纪念日
### [视频：五月天巡唱门票9分钟售罄 黄牛炒价10万台币一张](http://video.sina.com.cn/p/ent/2019-09-30/detail-iicezueu9313906.d.html)
> 概要: 视频：五月天巡唱门票9分钟售罄 黄牛炒价10万台币一张
### [组图：霍思燕黑色镂空蕾丝短裙显曼妙身材 挥手打招呼超亲切](http://slide.ent.sina.com.cn/star/slide_4_704_322979.html)
> 概要: 组图：霍思燕黑色镂空蕾丝短裙显曼妙身材 挥手打招呼超亲切
### [胡歌为电影海报签名 章子怡似小粉丝表情多多](https://ent.sina.com.cn/m/c/2019-09-30/doc-iicezzrq9305969.shtml)
> 概要: 昨天（9月29日）章子怡“现身”，她在微博上载多张跟胡歌的合照，宣传两人合作、明天在内地上映的电影《攀登者》。“古月哥哥到底在海报上写了啥？(责编：Blue)。
### [英伦型男迈克尔·辛50岁再得女儿 女友小他25岁](https://ent.sina.com.cn/m/f/2019-09-30/doc-iicezueu9309525.shtml)
> 概要: 新浪娱乐讯 9月30日消息，刚在大热剧集《好兆头》中出演天使的演员迈克尔·辛，迎来了自己的又一个小天使：他的二女儿出生了。他与贝金赛尔分手后关系依旧不错，去年圣诞节他俩是和女儿一起过的。(责编：加缪)。
### [全天候看紧？龚慈恩女儿美貌在外被觊觎，拿个外卖都全程监管](https://new.qq.com/omn/20190930/20190930A06Q6300.html)
> 概要: 9月30日港媒报道称，昨晚（29日）在金钟附近拍摄到离婚后的女神龚慈恩母女俩同行画面。大约7点多龚慈恩掐准时间开车前往金钟，到达目的地后女儿林恺铃（Ashley）刚好拍摄完棚内的广告工作，于是无缝连接地载上女儿驱车返回寓所！一代大美人龚慈恩也是心酸，当年（1990年）与第一任丈夫建筑商人霍振华结婚，却被传出拍戏回家后发现老公有婚外情，一年不到短命婚姻宣告结束。
### [杭十四中学生自编自导自演 激情高歌《不灭的信仰》](https://new.qq.com/omn/UIA20190/UIA2019093000161900.html)
> 概要: 最近，钱江晚报·小时新闻记者的朋友圈被一首歌刷屏——杭州第十四中学6名高中生的原创RAP歌曲《不灭的信仰》。《不灭的信仰》最初是由杭十四中学的7位学长学姐为庆祝新中国成立70周年而创作、填词的RAP歌曲。舞台上灯光闪烁，在一唱一和与轻盈舞步之间，全场的气氛被推向了高潮。
### [张柏芝开店与谢霆锋百米之隔，当听到前夫名字时，她的反应太意外](https://new.qq.com/omv/video/v30022lfw3h)
> 概要: 张柏芝开店与谢霆锋百米之隔，当听到前夫名字时，她的反应太意外
### [影视频道：它是一部灾难电影，一部爱情电影，一部励志电影！](https://new.qq.com/omn/20190930/20190930A0709900.html)
> 概要: 影视频道：它是一部灾难电影，一部爱情电影，一部励志电影！
# 动漫
### [动画《中之人基因组【实况中】》新作内容制作决定](https://news.dmzj.com/article/64822.html)
> 概要: TV动画《中之人基因组【实况中】》宣布了将要制作在TV版中没有播出的完全新作内容。新作内容将由原作者“おそら”负责剧本，更多详细情况还将于日后公开。
### [千值练《风之谷》王虫可动手办开订](https://news.dmzj.com/article/64836.html)
> 概要: 竹谷隆之与吉卜力合作制作的《风之谷》中的王虫目前已经开订了。本作是在吉卜力全面监修下，以造型作家竹谷隆之为中心制作的零件数超过300个的可动手办。
### [《花牌情缘3》排第一！日媒调查10月番动画视听意向](https://news.dmzj.com/article/64833.html)
> 概要: 株式会社Gzbrain面向企业提供的调查消费者在电影、电视、游戏、漫画、音乐等娱乐领域的消费动向的服务eb-i Xpress公开了日本全国5至69岁男女的10月番动画的视听意向。
### [【同人】三日间的幸福同人——《在那之后的故事》](https://news.dmzj.com/article/64837.html)
> 概要: 我的人生即将走到结局，在人生开始逐渐拥有了意义后，我的寿命，只剩下了短短的三天。
### [博人在家中不受鸣人关注，其实与他的身世有关，舍人：被发现了？](https://new.qq.com/omn/20190930/20190930A0EZOB00.html)
> 概要: 博人在家中不受鸣人关注，其实与他的身世有关，舍人：被发现了？
### [这只鸭子实在太动漫了，可说出价格就会让你们死心](https://new.qq.com/omn/20190930/20190930A0EX6800.html)
> 概要: 最近鸭子在网络上真的非常的火爆，《精灵宝可梦：剑/盾》最新登场了新神奇宝贝“葱游兵”频频登上全网热搜。人们对于这只117公斤自带配菜的鸭子，只有一个又大胆又馋嘴的想法，那就是做成鸭子料理。的确，鸭子……
### [铅笔人们在跑步，小蓝总是一马当先他有一个特殊能力！](https://new.qq.com/omv/video/i3001uhrrrp)
> 概要: 铅笔人们在跑步，小蓝总是一马当先他有一个特殊能力！
### [最美二次元：回眸一笑百媚生](https://new.qq.com/omn/20190930/20190930A0EX4I00.html)
> 概要: 最美二次元：回眸一笑百媚生
### [最美二次元：深爱之人藏心不挂嘴，久念之人在梦不在眼](https://new.qq.com/omn/20190930/20190930A0EZ3M00.html)
> 概要: 最美二次元：深爱之人藏心不挂嘴，久念之人在梦不在眼
### [中美合拍动画片《雪人奇缘》登顶北美周末票房榜](https://new.qq.com/omn/20190930/20190930A0FF8P00.html)
> 概要: 中美合拍动画片《雪人奇缘》登顶北美周末票房榜
# 科技
### [h265webplayer是金山云的Web端H.265视频播放器](https://www.ctolib.com/ksvc-h265webplayer.html)
> 概要: h265webplayer是金山云的Web端H.265视频播放器
### [XMall商城微信小程序前端 共计20多个页面 页面联动 精美细节 含SKU设计](https://www.ctolib.com/Exrick-xmall-weapp.html)
> 概要: XMall商城微信小程序前端 共计20多个页面 页面联动 精美细节 含SKU设计
### [模仿 Java 的 Spring 全家桶实现的一套 GoLang 的应用程序框架 🚀](https://www.ctolib.com/didi-go-spring.html)
> 概要: 模仿 Java 的 Spring 全家桶实现的一套 GoLang 的应用程序框架 🚀
### [常用的Java工具类的封装](https://www.ctolib.com/guokai1229-gk-tools.html)
> 概要: 常用的Java工具类的封装
### [炒鞋市场，“全民皆贩”](https://www.tuicool.com/articles/RNFVVzn)
> 概要: 球鞋的入手价并非高不可攀，如果再加上点运气，即使是没有经济来源的学生，都可入手，这让“全民皆贩”成为可能。对切克来说，炒鞋热的坏影响并不多，“平台实际是给大家建立一套标准和基本玩法的中间方，”而且“今年五月份我们才入场，所以，对于平台来说没有什么特别坏的影响。”。 球鞋媒体人Alex入行已有10年，在他看来，炒鞋热是“圈外人对圈内人的一次冲击，圈内人对圈外人的一次教育”。
### [留在一线城市发展的外地人，如何让梦想照进现实](https://www.tuicool.com/articles/6FNbI3U)
> 概要: 本文来自微信公众号：买房除了要努力存钱，还要敢于下手——市区的老破小就比远郊的新全大小区更适合。在大城市打拼的年轻人要像存钱一样存人脉：先不求多，但求能像雪球一样滚起来。
### [电子烟风口托不住太多想飞天的“猪”](https://www.tuicool.com/articles/u6VRjqm)
> 概要: 作者：左岸 编辑 ：秦言。关于行业的准入门槛，某电子烟品牌联合创始人对懂懂笔记表示： “外界说电子烟行业准入门槛低，其实还是要看具体的角度。“之前都在传电子烟行业的准入门槛是500万元，但如今根本要不了这么多，几十万元甚至十几万元元就能搞定。
### [无产品岗位实习，我是如何从拿到腾讯offer？](https://www.tuicool.com/articles/AjqER3a)
> 概要: 本文主要通过对腾讯面试的详细分析与总结来聊聊，应聘秋招产品经理岗位这场仗究竟应该如何漂亮地赢。校招总共历时一个月，但由于人在海外，很多宣讲都错过了，并且从技术转产品，一些产品所需技能点并没有完全点亮，已经做好了打持久战的准备，甚至有想过放弃秋招参加春招。简历是重中之重，包括如何将做过的技术用产品思维转化输出，具体设计方式我会在下篇文章中写。
### [大型系统如何做一体化监控？](https://www.tuicool.com/articles/aI7nUfj)
> 概要: 目前系统监控的手段比较多，大致可以分为三类：业务监控，应用监控和系统监控。本人基于实际的监控痛点，通过一体化地监控手段，比较高效地解决大型系统的监控问题，这里抛砖引玉，和大家探讨一下。然后要针对系统做整体监控，把各个部分放一起，能够迅速识别哪些部分是好的，哪些部分不好。
### [热点 | 百度Estaff成员王路将离职](https://www.tuicool.com/articles/6fIvEjY)
> 概要: 百度副总裁王路。王路于2016年9月加入百度，此前他曾任沃尔玛电商亚洲区总裁兼CEO，协助沃尔玛全资收购1号店，并领导1号店在华业务。加入百度后，王路负责业务有公共关系、市场、政府关系、职业道德建设等。
### [香侬读 | Transformer中warm-up和LayerNorm的重要性探究](https://www.tuicool.com/articles/InI3MnF)
> 概要: 论文标题：On Layer Normalization in the Transformer Architecture。作者发现，Post-LN Transformer在训练的初始阶段，输出层附近的期望梯度非常大， 所以，如果没有warm-up，模型优化过程就会炸裂，非常不稳定。对Pre-LN Transformer，我们去掉warm-up，在IWSLT14 De-En中保持5e-4然后从第8个epoch开始下降。
### [成立近 6 年，粉丝经济下「Owhat」的转型之路](https://www.tuicool.com/articles/vMrAf2Q)
> 概要: 如果你是饭圈女孩，一定不会陌生一个叫“OWhat”的App。但Owhat做电商生意比起其他粉丝社区较有优势的一点是，它从一开始就不是流量型或者工具型产品，而是切入交易环节，所以粉丝有一种“来这花钱”的基本认知。明星的流量可以带来高话题度，郑爽与Jalouse最近在巴黎的拍摄甚至一天占据8个热搜位。
# 小说
### [新平家物语（壹）](http://book.zongheng.com/book/681796.html)
> 作者：日吉川英治

> 标签：历史军事

> 简介：《新平家物语》是日本文学巨擘吉川英治以现代文学笔法重写的有日本“三国演义”之称、日本古典文学双璧之一《平家物语》的史诗巨著，详述了日本中世纪源氏和平家两大武士集团争夺权力的全过程。《新平家物语？壹》讲述平氏首领平清盛的崛起过程。 平清盛天性豪放，幼年因家境贫困常常不得不奔走借贷，看尽亲友的冷脸。潦倒的境况并未浇灭心中骄傲的志向。清盛不甘于做“殿上人”公卿贵族的看家狗，立志改变自己“地下人”的身份，做“堂堂天地一男儿”。 跟随父亲效力于鸟羽上皇后，面对连上皇都无可奈何的武装僧团的滋扰，年轻勇武的清盛迎面而上，做出一件惊天动地的大事，解除了僧团对宫廷的冲击。但因身份卑下，无论怎样卖力都得不到报偿。

> 章节末：现世报

> 状态：完本
### [八十天环游地球（凡尔纳漫游者系列·第3辑）](http://book.zongheng.com/book/681910.html)
> 作者：JulesVerne

> 标签：历史军事

> 简介：《八十天环游地球》是“科幻小说之父”凡尔纳的著名作品，叙述了英国人福克先生和朋友打赌，用八十天的时间，历尽千难万险，环游地球一周的故事。沉着、寡言、机智、勇敢、充满人道主义精神的福克，活泼、好动、易冲动的仆人等给人留下了深刻的印象。

> 章节末：Chapter 37 斐莱亚·福克这次环游地球，除了幸福，别的一无所得

> 状态：完本
# 论文
### [Edge-Informed Single Image Super-Resolution](https://paperswithcode.com/paper/edge-informed-single-image-super-resolution)
> 日期：11 Sep 2019

> 标签：IMAGE INPAINTING

> 代码：https://github.com/knazeri/edge-informed-sisr

> 描述：The recent increase in the extensive use of digital imaging technologies has brought with it a simultaneous demand for higher-resolution images. We develop a novel edge-informed approach to single image super-resolution (SISR).
### [Zero-Shot Grounding of Objects from Natural Language Queries](https://paperswithcode.com/paper/zero-shot-grounding-of-objects-from-natural)
> 日期：20 Aug 2019

> 标签：OBJECT DETECTION

> 代码：https://github.com/TheShadow29/zsgnet-pytorch

> 描述：A phrase grounding system localizes a particular object in an image referred to by a natural language query. In previous work, the phrases were restricted to have nouns that were encountered in training, we extend the task to Zero-Shot Grounding(ZSG) which can include novel, "unseen" nouns.
