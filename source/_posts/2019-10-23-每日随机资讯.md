---
title: 2019-10-23-每日随机资讯
tags: 资讯
thumbnail: /images/周三.png
date: 2019-10-23 22:19:18
---

# 娱乐
### [组图：昔日搭档同框！潘粤明与张彤机场同行曾同演《大唐情史》](http://slide.ent.sina.com.cn/star/slide_4_704_324765.html)
> 概要: 组图：昔日搭档同框！潘粤明与张彤机场同行曾同演《大唐情史》
### [视频：实火！肖战零宣传活动吸引大批粉丝 主办方无奈取消](http://video.sina.com.cn/p/ent/2019-10-23/detail-iicezuev4369562.d.html)
> 概要: 视频：实火！肖战零宣传活动吸引大批粉丝 主办方无奈取消
### [现场获奖者为江一燕发声：建筑设计是team work](https://ent.sina.com.cn/s/m/2019-10-23/doc-iicezuev4309660.shtml)
> 概要: 新浪娱乐讯 10月23日，就近日受到热议的江一燕获美国建筑大师奖一事，有网友发长文称自己也是获奖者，现在的情况对江一燕太不公平！她表示自己在当天认识并且非常欣赏江一燕，还说：“建筑设计本身就是tea......
### [组图：张艺兴被续聘共青湖南宣传大使 用实际行动传递正能量](http://slide.ent.sina.com.cn/y/w/slide_4_86512_324774.html)
> 概要: 组图：张艺兴被续聘共青湖南宣传大使 用实际行动传递正能量
### [组图：韩歌手Zico登时尚杂志拍写真 展不羁嘻哈范儿](http://slide.ent.sina.com.cn/y/k/slide_4_704_324753.html)
> 概要: 组图：韩歌手Zico登时尚杂志拍写真 展不羁嘻哈范儿
### [戚薇李承铉对唱《月亮代表我的心》，刘宪华站在一旁实力抢镜](https://new.qq.com/omn/20191023/20191023V0IDWO00.html)
> 概要: 戚薇李承铉对唱《月亮代表我的心》，刘宪华站在一旁实力抢镜
### [江一燕是什么群嘲体质吗？薛凯琪于正都刚过她](https://new.qq.com/omn/20191023/20191023A0PA5200.html)
> 概要: 因为一个建筑大师奖，江一燕突然成了众人群嘲的对象。简单说就是演员江一燕庆祝自己在美国得了一个专业的建筑奖，结果被网友质疑。后来被证实她获奖的建筑首席设计师另有其人，不过她也是设计团队中的一员，虽然作......
### [景甜露香肩秀身材，一袭红裙太惊艳，不愧是人间富贵花](https://new.qq.com/omn/20191023/20191023A0PDLY00.html)
> 概要: 景甜的最新一组大片曝光，引起了网友们的围观。这次景甜挑战了抹胸红裙的造型，饱和度不高的酒红色礼服裙，更显高级感不说，还展现了她雪白的肌肤，整个人美出了新高度。和之前较为保......
### [定档影片票房预测、复盘及解读](https://new.qq.com/omn/20191023/20191023A0PDMJ00.html)
> 概要: 泛娱乐顶尖自媒体 《沉睡魔咒2》、《双子杀手》、《航海王：狂热行动》首周票房仅相差千万，不分伯仲。但《沉睡魔咒2》后发制人预计后续票房会开始上扬。《沉睡魔......
### [刘诗诗最新动态曝光，撞脸杨紫显疲态，精修都遮不住法令纹](https://new.qq.com/omn/20191023/20191023A0F49B00.html)
> 概要: 《亲爱的自己》剧组给四川电视节送祝福啦！刘诗诗、朱一龙、阚清子等人纷纷录制了ID，视频里刘诗诗皮肤白皙、声音甜美，可是却面部浮肿，粉丝精修之后还是看得到法令纹，苹果肌也很僵硬，乍一看有些撞脸杨紫......
# 动漫
### [【原创】日常青春剧——《夕阳下殉情》完结篇](https://news.dmzj.com/article/65101.html)
> 概要: “就是因为这样啊，搞不好他迷恋断的太干净，马上就会忘了我呢。这可不要啊。”
### [动画《虫笼的卡伽斯特尔》将于2020年登陆Netflix](https://news.dmzj.com/article/65095.html)
> 概要: 根据桥本花鸟原作制作的动画《虫笼的卡伽斯特尔》公开了多张图片，本作将由GONZO制作，预计将于2020年在Netflix播出。
### [动画《齐木楠雄的灾难》新作正式预告片](https://news.dmzj.com/article/65098.html)
> 概要: 根据麻生周一原作制作的动画《齐木楠雄的灾难 再始动篇》公开了新宣传图与正式预告视频。在这段视频中，可以看到除了系列前作的角色们以外，还将有新的角色登场。
### [声优逢坂良太和声优沼仓爱美结婚](https://news.dmzj.com/article/65093.html)
> 概要: 声优逢坂良太和声优沼仓爱美宣布了成婚的消息。二人表示已经于前些日子进行了登记，在今后依然将会为了为大家献上更好的作品而精进。
### [野蛮纪源：一句台词没有，却饱含深意的动画](https://new.qq.com/omn/20191023/20191023A0PUP500.html)
> 概要: 一个动画作品台词的重要性自然不用说，《野蛮纪源》仅仅只靠画面和配乐就能抓住观众们的眼球，这样的作品绝对是神级，这部动画虽然只有短短的五集，但是内容却很精彩，每集都会有许多恐怖的史前巨兽出现，每个画面……
### [刺客伍六七2动画：伍六七再次救了梅花十三，却被甩了一巴掌](https://new.qq.com/omn/20191023/20191023A0POV200.html)
> 概要: 伍六七原本是玄武国排名第一的刺客，引来了很多的刺客想要杀他，落水流落到小鸡岛之后，虽然被救了却失忆了。梅花十三本来奉命来小鸡岛调查岛上的最强者，然后按照情报制作暗杀令，把岛上的强者刺杀了之后，玄武国……
### [电影风格打开西行纪：猪八戒被欺骗了二十六年，兄弟对决一触即发！](https://new.qq.com/omv/video/e3012v0nr7c)
> 概要: 电影风格打开西行纪：猪八戒被欺骗了二十六年，兄弟对决一触即发！
### [冰海战记：维京时代是否如剧情所描述，理想乡文兰是否真的存在](https://new.qq.com/omn/20191023/20191023A0PHLY00.html)
> 概要: 有一部持续着不温不火的动漫作品持续受到漫迷们的讨论，这部作品就是《冰寒战记》讲述着维京时期奴隶制社会的故事。主角是身为“尤姆战鬼”之子的托儿芬为了给父亲报仇跟随着海盗仇人阿谢拉特奔赴航海的故事，在目……
### [奥特曼：诺亚打得过贝利亚，也打得过黑暗迪迦，唯独对他毫无办法！](https://new.qq.com/omn/20191023/20191023A0HWWS00.html)
> 概要: 奥特曼：诺亚打得过贝利亚，也打得过黑暗迪迦，唯独对他毫无办法！《奥特曼》中诺亚就如同神一般的存在，他的真正实力没几个人知道，但却都知道他有多么的强大。而诺亚他打得过贝利亚，也同样打得过黑暗迪迦，却唯……
### [刀剑神域：幸和优吉欧，2人经历如此相似，为何漫迷却只喜欢幸？](https://new.qq.com/omn/20191023/20191023A0HGLI00.html)
> 概要: 《刀剑神域 爱丽丝篇》已经开始播放了，从前几集的播放来看，感觉还是比较不错的，至少经费是不用担心的。不过，看到爱丽丝和桐人的互动，总是还能想到优吉欧这个角色。其实分析一下可以发现，幸和优吉欧，2人经……
# 科技
### [用于预测三级蛋白质结构的PyTorch框架](https://www.ctolib.com/OpenProtein-openprotein.html)
> 概要: 用于预测三级蛋白质结构的PyTorch框架
### [基于SSD框架在建筑工地上检测安全帽的佩戴并识别安全帽的相应颜色](https://www.ctolib.com/wujixiu-helmet-detection.html)
> 概要: 基于SSD框架在建筑工地上检测安全帽的佩戴并识别安全帽的相应颜色
### [有助于提高机器学习编程效率的 Python (>= 3.6) 特性集锦](https://www.ctolib.com/chiphuyen-python-is-cool.html)
> 概要: 有助于提高机器学习编程效率的 Python (>= 3.6) 特性集锦
### [cli-visualizer：命令行音频可视化工具](https://www.ctolib.com/dpayne-cli-visualizer.html)
> 概要: cli-visualizer：命令行音频可视化工具
### [我从阿里巴巴辞职,到创业这凤凰涅槃的100天感受和总结！](https://www.tuicool.com/articles/yeqQrmf)
> 概要: 剑已出鞘，任戎马倥偬，亦百折不挠，仗剑长歌，砥砺前行！我出来创业这段时间，一直没有正式宣布，知道的人其实并不多，今天我正式宣布一下：类类创业了。今年6月初，我从蚂蚁金服辞职，6月26号正式开始创业......
### [从 OpenAI 的魔方机械手，看元学习的崛起](https://www.tuicool.com/articles/EbyaAbe)
> 概要: 元学习（Meta-Learning）描述了设计与训练深度神经网络有关的更高级别组件的抽象。 当涉及神经网络架构的自动化设计时，元学习这个术语常常出现引用“自动机器学习（AutoML）”、“小样本学习（......
### [开发一个operator扩展kubernetes的能力](https://www.tuicool.com/articles/3qQFnyN)
> 概要: 正文 Operator 是 CoreOS 推出的旨在简化复杂有状态应用管理，它是一个感知应用状态的控制器，通过扩展 Kubernetes API 来自动创建、管理和配置应用实例。 Operator ......
### [核桃编程获5000万美元B轮融资，华兴新经济基金与高瓴资本联合领投](https://www.tuicool.com/articles/MzauiuY)
> 概要: 【猎云网（微信：）北京】10月23日报道  猎云网近日获悉，少儿编程教育品牌核桃编程宣布获得5000万美元B轮融资，由华兴新经济基金与高瓴资本联合领投，众源资本、微影资本、源码资本、XVC跟投......
### [这2招配色诀窍，零基础的小白也能轻松学会！](https://www.tuicool.com/articles/VzMbaae)
> 概要: 今天将要分享的是一个我们在设计中经常会用到的「配色」。很多设计师工作几年后还是对配色摸不着头脑，每次做设计依旧没有头绪，不知道如何下手，不知道选择什么颜色搭配自己相关的设计，看完今天的教程分享后希......
### [集群版ZooKeeper 多个端口监听的建立逻辑解析](https://www.tuicool.com/articles/eAFfAzf)
> 概要: ZooKeeper 作为优秀的分布系统协调组件，值得一探究竟。它的启动类主要为：1. 单机版的zk 使用 ZooKeeperServerMain 2. 集群版的zk 使用 QuorumPeerM......
### [三步走！基于 Knative Serverless 技术实现一个短网址服务](https://www.tuicool.com/articles/7N3aU37)
> 概要: 短网址顾名思义就是使用比较短的网址代替很长的网址。维基百科上面的解释是这样的：短网址又称网址缩短、缩短网址、URL 缩短等，指的是一种互联网上的技术与服务，此服务可以提供一个非常短小的 URL 以......
### [收益 or 挑战？Serverless 究竟给前端带来了什么](https://www.tuicool.com/articles/uQ7zUfV)
> 概要: 作者 | 黄子毅（紫益） 阿里前端技术专家 导读：前端开发者是最早享受到 “Serverless” 好处的群体，因为浏览器就是一个开箱即用、甚至无需为计算付费的环境！Serverless 把前端......
# 小说
### [左道](http://book.zongheng.com/book/164279.html)
> 作者：一念乱天机

> 标签：奇幻玄幻

> 简介：三千大道无上法，八百旁门先天道，古往今来谁能证，左道七术撼乾坤。芸芸众生中的一个普通小职员，机缘之下得到了造化残片，降临异世，附体重生，依仗左道七大术踏上了了纵横天下，统帅诸天，证道无上的艰辛道路。左道群1：223206441

> 章节末：第六百章    左道大成，打完收官

> 状态：完本
### [两界搬运工](https://m.qidian.com/book/1003713126/catalog)
> 作者：石闻

> 标签：时空穿梭

> 简介：自由穿行在现实世界和玄幻世界之间，安静的做一个两界搬运工。PS：新书《南山隐》求支持！！

> 章节总数：共1351章

> 状态：完本
# 论文
### [Tuning parameter calibration for prediction in personalized medicine](https://paperswithcode.com/paper/tuning-parameter-calibration-for-prediction)
> 日期：23 Sep 2019

> 标签：CALIBRATION

> 代码：https://github.com/LedererLab/personalized_medicine

> 描述：Personalized medicine has become an important part of medicine, for instance predicting individual drug responses based on genomic information. However, many current statistical methods are not tailored to this task, because they overlook the individual heterogeneity of patients.
### [Leveraging 2-hop Distant Supervision from Table Entity Pairs for Relation Extraction](https://paperswithcode.com/paper/leveraging-2-hop-distant-supervision-from)
> 日期：13 Sep 2019

> 标签：RELATION EXTRACTION

> 代码：https://github.com/sunlab-osu/REDS2

> 描述：Distant supervision (DS) has been widely used to automatically construct (noisy) labeled data for relation extraction (RE). Given two entities, distant supervision exploits sentences that directly mention them for predicting their semantic relation.
