---
title: 2019-10-17-每日随机资讯
tags: 资讯
thumbnail: /images/周四.png
date: 2019-10-17 20:27:03
---

# 娱乐
### [萧煌奇半夜被门铃吵醒 同事拿摄像机侧录没人影](https://ent.sina.com.cn/y/ygangtai/2019-10-17/doc-iicezzrr2815176.shtml)
> 概要: 新浪娱乐讯 据台湾媒体报道，萧煌奇日前刚结束在大陆长达一个月的舞台剧演出，除了11月的小巨蛋演唱会，新专辑《候鸟》的宣传又接踵而至，10月16日唱片公司特别为他打造了一个“候鸟”装置艺术，于信义商圈街头展出三天，为新专辑暖身。他透露先前密集的舞台剧演出，导致他感冒、过敏大发作，整个脖子又红又痒，一回到台湾立刻痊愈。(责编：漠er凡)。
### [组图：“惊鸿仙子”俞飞鸿优雅入骨 皮衣配碎花裙摇曳生姿](http://slide.ent.sina.com.cn/star/slide_4_704_324238.html)
> 概要: 组图：“惊鸿仙子”俞飞鸿优雅入骨 皮衣配碎花裙摇曳生姿
### [组图：宋妍霏英伦风装扮现身机场 头戴南瓜帽手拿奶茶心情极佳](http://slide.ent.sina.com.cn/y/slide_4_704_324190.html)
> 概要: 组图：宋妍霏英伦风装扮现身机场 头戴南瓜帽手拿奶茶心情极佳
### [《太太请小心轻放》明年公开 讲述日剧后续故事](https://ent.sina.com.cn/m/f/2019-10-17/doc-iicezzrr2856624.shtml)
> 概要: 新浪娱乐讯 据日本媒体报道， 女星绫濑遥首次在日剧中饰演家庭主妇，与演员西岛秀俊饰演夫妻的日本台人气日剧《太太请小心轻放》宣布电影化，电影版延续2017年12月播出的日剧最终回，引起极大反响的“史上最强夫妻吵架”之后的故事。电影版中绫濑遥与西岛秀俊的动作戏依然是看点，而且他们还要面对更强大的敌人，剧场版还到葡萄牙拍摄外景，还邀请了追加演员。西岛秀俊说。
### [组图：木村拓哉新剧举办制作发表会 众演员出席活动](http://slide.ent.sina.com.cn/tv/jp/slide_4_704_324260.html)
> 概要: 组图：木村拓哉新剧举办制作发表会 众演员出席活动
### [黄磊孙莉罕见同时晒女儿，两姐妹格外娇艳，颇有文艺气息](https://new.qq.com/omv/video/n30094g9k14)
> 概要: 黄磊孙莉罕见同时晒女儿，两姐妹格外娇艳，颇有文艺气息
### [曾经意气风发琼男郎，马景涛如何变成挺着啤酒肚的中年油腻男？](https://new.qq.com/omn/20191017/20191017A0B7S900.html)
> 概要: 10月13日，马景涛前妻吴佳尼在社交平台晒出一组与儿子的同框合影照，照片中的吴佳尼陪两个儿子在电玩城玩游戏，晒出背影照大秀美腿，一点都看不出来她今年已经36岁了。但随后，马景涛立刻被爆出和吴佳尼离婚是因为家暴。此后马景涛就开始了放飞自我之路，总是以夸张不着边际的形象出现在公众的视野里。
### [谢娜再添新身份，引网友热议：成为央视主持人？](https://new.qq.com/omv/video/q3009eksmc8)
> 概要: 谢娜再添新身份，引网友热议：成为央视主持人？
### [刘涛新造型美翻了！41岁演绎80年代复古风太绝美，不装嫩被猛夸](https://new.qq.com/omv/video/k30090i8d43)
> 概要: 刘涛新造型美翻了！41岁演绎80年代复古风太绝美，不装嫩被猛夸
### [阿宝妻子，刘大成妻子，大衣哥妻子，差距也太大了](https://new.qq.com/omv/video/b3009yjns23)
> 概要: 阿宝妻子，刘大成妻子，大衣哥妻子，差距也太大了
# 动漫
### [【原创】日常青春剧——《夕阳下殉情》4](https://news.dmzj.com/article/65017.html)
> 概要: 心中充斥着这种单方面的奢望，万一石门赞同我的做法呢。但看着只剩残骸的现场，我什么都说不出口，从生物教室的另一扇门逃走了。
### [制作过《染红的街道》等作品的美少女游戏公司feng破产](https://news.dmzj.com/article/65018.html)
> 概要: 运营美少女游戏品牌feng的有限会社WhiteRose，宣布了开始破产的消息。
### [【每日话题】10月新番陆续播出 这季番你最喜欢哪一部？](https://news.dmzj.com/article/65010.html)
> 概要: 不知不觉间，10月已经过半，大部分10月番也都已经陆续播出。那么在本季新番中你都在追哪些作品？最喜欢的又是哪一部呢？
### [没觉得是性骚扰！日本红十字会回复联动海报问题](https://news.dmzj.com/article/65019.html)
> 概要: 在之前曾为大家介绍过，律师太田启子等人在看到《宇崎酱想要玩耍》与日本红十字会进行献血联动活动的海报，被人指责有性骚扰的嫌疑。有日本媒体就此问题询问了日本红十字会。日本红十字会也进行了回应。
### [都是冰公主，国漫却比不过迪士尼，因为细节决定成败](https://new.qq.com/omn/20191017/20191017A0HQJD00.html)
> 概要: 在二次元的世界里面。存在两位冰公主。一位是国漫《精灵梦叶罗丽》里面的冰公主，另一位则是迪士尼《冰雪奇缘》里面的艾莎。都是动漫中美丽、高冷的女神，她们两位图强孰弱？虽然都是冰公主，但是国漫却比不过迪士……
### [不良人：想对李星云出手，也得看看他背后有多少势力](https://new.qq.com/omn/20191017/20191017A0N17G00.html)
> 概要: 《不良人》第三季的结尾，其实意味着天下将进入最混乱的时代。如今没有不良帅这个威胁在，各路人马都想成为这天下的共主。对于他们来说，李星云和传国玉玺都很重要。有一部分人会选择以大唐血脉为幌子，来笼络人心……
### [鬼灭之刃漫画179话情报：无一郎和玄弥确定阵亡](https://new.qq.com/omn/20191017/20191017A0FFQX00.html)
> 概要: 期待已久的鬼灭之刃漫画179话情报在近日有所透露和公开，这次的新漫画剧情不得不说太过于虐心，业界堪称最无情的鳄鱼老师开始行动了，玄弥和无一郎确定阵亡，来看看具体的情报吧。目前没有公开具体的文字预告内……
### [海贼王：今天就来告诉你，白胡子二世为什么会得到黄猿的认可](https://new.qq.com/omv/video/d3009pibuzl)
> 概要: 海贼王：今天就来告诉你，白胡子二世为什么会得到黄猿的认可
### [西行纪：四个角色可以伤害到罗侯，小羽的灼伤效果很强](https://new.qq.com/omn/20191017/20191017A0H6RB00.html)
> 概要: 西行纪是一部非常优秀的国产动漫，这部动漫刻画的人物非常多，今天我们来聊一聊可以伤害到罗侯的四个角色。罗侯是阿修罗界的狂王，从他的名字就能看出他是一个放荡不羁的角色。罗侯的实力极强，他仅凭自己一人之力……
### [自来也是谁，博人的好奇心又被激发，穿越过去找他拜师？](https://new.qq.com/omn/20191017/20191017A0F8J600.html)
> 概要: 博人传的热播又进入了新的阶段，自来也的大名意外被博人这个小毛头知晓，又将引起怎样的冒险之旅呢？让我们来剧透一番。在博人的母亲眼里他的父亲拜自来也为师学习作为一名忍者的必修课。这位忍者中的强者留下了一……
# 科技
### [Label Objects and Save Time (LOST) - Web版半自动图像标注框架](https://www.ctolib.com/l3p-cv-lost.html)
> 概要: Label Objects and Save Time (LOST) - Web版半自动图像标注框架
### [用 PyTorch 1.2 实现的文本检测与识别研究项目](https://www.ctolib.com/Megvii-CSG-MegReader.html)
> 概要: 用 PyTorch 1.2 实现的文本检测与识别研究项目
### [Pyflow一个用于管理Python安装和依赖关系的工具](https://www.ctolib.com/David-OConnor-pyflow.html)
> 概要: Pyflow一个用于管理Python安装和依赖关系的工具
### [dapr: 一种可移植的，事件驱动的运行时，用于跨云和边缘构建分布式应用](https://www.ctolib.com/dapr-dapr.html)
> 概要: dapr: 一种可移植的，事件驱动的运行时，用于跨云和边缘构建分布式应用
### [手把手教程：如何从零开始训练 TF 模型并在安卓系统上运行](https://www.tuicool.com/articles/nMVFNri)
> 概要: 本教程介绍如何使用 tf.Keras 时序 API 从头开始训练模型，将 tf.Keras 模型转换为 tflite 格式，并在 Android 上运行该模型。我将以 MNIST 数据为例介绍图像分类，并分享一些你可能会面临的常见问题。本教程着重于端到端的体验，我不会深入探讨各种 tf.Keras API 或 Android 开发。
### [VIPKID林陈斌：利用AI技术深度切入各教学场景，实现在线教育可量化、可视化](https://www.tuicool.com/articles/m67FNz2)
> 概要: 【猎云网（微信：）北京】10月17日报道（文/吕梦）。比如，VIPKID在课件研发、师生匹配上进行了深入探索。第三个应用的比较有成果的是在教学内容的迭代上面，我们知道传统的教学迭代内容通常是年级，我们现在迭代周期是两周，每周都会分析从一千节课件里面分析，按照刚才好课的评判标准，统计学上哪节课出现问题最多。
### [使用Docker和WSL体验信息安全工具](https://www.tuicool.com/articles/IfM7J3e)
> 概要: 多年以来，每当我在一些会议、用户组和客户演示会上发言时，我都会谈到一些关于帮助我们学习信息安全工具和技术的“新方法”。而我最推荐大家的就是使用Docker容器和WSL（Windows Subsystem for Linux）来快速体验这些工具，而无需管理虚拟机或其他基础架构。
### [量子力学治痛经？起底微博假流量事件的骗子公司](https://www.tuicool.com/articles/m2Y7F3m)
> 概要: 本文来自。在那条 Vlog 的微博中可以看到，这是一种名为“E飞养宫宝”的产品，工作原理是“靠光能技术，导入振动光波能量，加速血液循环”，实现“预防和缓解痛经”。从这件事说开去，为什么流量造假难以打掉？
### [Material Design图标规范——产品图标](https://www.tuicool.com/articles/RJJRjqb)
> 概要: UI中的图标是用户第一次接触和后续使用的门面担当，随着移动化趋势的普及，如今说起图标浮想起的是移动端上的图标。移动端图标根据系统平台的不同有两种标准：IOS标准和Material Design标准（Android）。把谷歌邮箱中包含的M要素拆开，就得到了形状“I”V“I”，我们先来绘制一下“形状I”。
### [焦点分析 | 亚马逊关停最后一个甲骨文数据库：是作秀，更是示威](https://www.tuicool.com/articles/UVFzqyJ)
> 概要: 在一阵倒计时中，亚马逊电商副总裁 Dave Treadwell 敲下几行代码，屏幕上显示“stopped oracle（终止甲骨文）”，围观的人们爆发出一阵欢呼，还有人开了香槟。上述人士进一步谈到，亚马逊完成数据库的迁移，意味着向业界展示，如何用云的方式解决企业业务级要求的数据库，当中传递了“我自己就是个超大型企业，我能迁移，你也可以”的信号。甲骨文则在云计算领域持续推进，但它的表现却不尽如人意。
### [淘集集商家为什么”心甘情愿“达成了和解？](https://www.tuicool.com/articles/ZB7zMnI)
> 概要: 编者按：本文来自微信公众号“。但这一份协议与前几日网上曝光的淘集集提供给商家的协议一样，都让商家们不太敢签。谈判结束后，依然有部分商家选择留守在淘集集公司总部楼下，以及淘集集公司总部所在的12楼，部分商家前往市政府，准备向相关部门反应情况。
### [带你涨姿势的认识一下 Kafka](https://www.tuicool.com/articles/3MjMbyN)
> 概要: 带你涨姿势的认识一下 Kafka
# 小说
### [古城七日](http://book.zongheng.com/book/829733.html)
> 作者：文书童

> 标签：奇幻玄幻

> 简介：此文章是我的首部小说，也是一部带着悲情色彩但结局还算圆满的短篇穿越小说，小说以男主人公从现代生活的城市穿越到清朝末年的同一个城市为背景，期间经历的七天见闻为故事线索，叙述了两个时代的不同与相同以及那短暂而又意味深长的爱情悲欢，内容环环相扣，引人入胜，弘扬正能量，值得诸位读者朋友一览。

> 章节末：第十二章：回归平淡

> 状态：完本
### [不夜烛](http://book.zongheng.com/book/661883.html)
> 作者：骑着驴子逛超市

> 标签：奇幻玄幻

> 简介：大道有空缺，由他来补。

> 章节末：第三百八十六章  不夜烛（大结局）

> 状态：完本
# 论文
### [HARE: a Flexible Highlighting Annotator for Ranking and Exploration](https://paperswithcode.com/paper/hare-a-flexible-highlighting-annotator-for)
> 日期：29 Aug 2019

> 标签：DOCUMENT RANKING

> 代码：https://github.com/OSU-slatelab/HARE

> 描述：Exploration and analysis of potential data sources is a significant challenge in the application of NLP techniques to novel information domains. We describe HARE, a system for highlighting relevant information in document collections to support ranking and triage, which provides tools for post-processing and qualitative analysis for model development and tuning.
### [Recurrent Neural Networks for Time Series Forecasting: Current Status and Future Directions](https://paperswithcode.com/paper/recurrent-neural-networks-for-time-series-1)
> 日期：2 Sep 2019

> 标签：TIME SERIES

> 代码：https://github.com/HansikaPH/time-series-forecasting

> 描述：Recurrent Neural Networks (RNN) have become competitive forecasting methods, as most notably shown in the winning method of the recent M4 competition. However, established statistical models such as ETS and ARIMA gain their popularity not only from their high accuracy, but they are also suitable for non-expert users as they are robust, efficient, and automatic.
