---
title: 2019-07-29-每日随机资讯
tags: 资讯
thumbnail: /images/周一.png
date: 2019-07-29 23:32:53
---

# 动漫

### [古代婚礼上除了常见的三叩首，居然还有这一拜？](https://new.qq.com/omv/video/a0905xecz7i)

> 概要: 古代婚礼上除了常见的三叩首，居然还有这一拜？

### [33岁我想找好男人！日漫版欢乐颂，都市单身女性们的白日梦](https://news.dmzj.com/article/63421.html)

> 概要: 33岁我想找好男人！日漫版欢乐颂，都市单身女性们的白日梦。

### [假面骑士：逢魔和崇皇决战阵容曝光！王小明神主形态升级来袭！](https://new.qq.com/omn/20190729/20190729A0PGMH00.html)

> 概要: 大家好，北冥特摄漫评，带你看不一样的骑士资讯。假面骑士时王最终回将在八月底正式结束，目前最后决战的阵容已经有消息显露了，据杂志爆料，两方的实力还是非常接近的，首先我们来看一下崇皇这边的阵容，首先就是……

### [天行九歌，红莲卫庄一共拉过几次手？盖聂：让我数数看](https://new.qq.com/omn/20190719/20190719A0949O00.html)

> 概要: 在天行九歌中，卫庄和红莲这对CP应该是很多人吃的最欢的一对儿吧，因为不同于秦时明月中卫庄的冷漠，天行九歌里的卫庄对红莲可以说是挺热情的（外冷内热）。短短七十多集的天行九歌卫庄就和小红花拉过三次手，那……

### [抄袭反拉黑原画师？国产Galgame涉嫌抄袭](https://news.dmzj.com/article/63415.html)

> 概要: 前几日，微博上的一名画师@X_I_N-画图佣兵在微博上控诉国产Galgame《游物语》美术素材盗用自己的作品。作者本人已经多次与游戏官方交涉，但是最终对方并不打算与自己和解，反而将自己拉黑。

### [全网COS魔童哪吒，本以为是位青铜，实际却是最强王者](https://new.qq.com/omn/20190729/20190729A0OIFV00.html)

> 概要: 最近，国漫电影《哪吒之魔童降世》自上映以来，短短几天就收获了几亿票房，关于它的好评也是铺天盖地，成为暑期档最佳国漫。于是也掀起了一场COS热潮，不少粉丝纷纷拿出自己的看家本领来挑战魔童哪吒，下面就一……

### [斗罗大陆说原著61 小舞为了发型不睡觉？唐三惊呆](https://new.qq.com/omv/video/i0906gf1nk3)

> 概要: 斗罗大陆说原著61 小舞为了发型不睡觉？唐三惊呆

### [新海诚《天气之子》高人气！连续两周观影人数排首位](https://news.dmzj.com/article/63422.html)

> 概要: 日本的“兴行通讯社”公开了7月27日至28日日本电影观影人数排行榜TOP10。其中由新海诚导演的动画电影《天气之子》延续着上一周的高人气，连续两周力压《玩具总动员4》排名观影人数排行榜第一位。

### [乔碧萝引发宅男恐慌：“中国第一coser”被围观，会翻车吗](https://new.qq.com/omn/20190729/20190729A0QIH400.html)

> 概要: 网红时代再也不能相信美女了！屏幕前娇艳欲滴的绝世容颜和你各种开车，谁曾想背后可能是个58岁的黄脸大妈？近日，斗鱼萝莉二次元主播"乔碧萝殿下"一不小心露出真容可把粉丝吓得不轻，原来10级美颜竟能让网络……

### [斗破苍穹：前期能与“药尘”抗衡的人，加码帝国只一位，风系斗宗](https://new.qq.com/omn/20190729/20190729A0RN0Z00.html)

> 概要: 在斗破苍穹中，前期出现的一个神秘人物，最令大家匪夷所思的，想必就是附身于萧炎，纳戒里的一位灵魂老者，名为“药老”，而其实啊，他的本名并非“药老”，而是“药尘”，前世背景强大，实则为萧炎母亲的师傅，同……

# 科技

### [elCanvas 一种轻量级动画引擎，可在Canvas上制作动画](https://www.ctolib.com/luckyde-elCanvas.html)

> 概要: elCanvas 一种轻量级动画引擎，可在Canvas上制作动画

### [Remove distractions from the new Twitter layout. Extension for Chrome and Firefox.](https://www.ctolib.com/brunolemos-simplified-twitter.html)

> 概要: Remove distractions from the new Twitter layout. Extension for Chrome and Firefox.

### [机器学习在高德起点抓路中的应用实践](https://www.tuicool.com/articles/IVVfUrf)

> 概要: 无法单类迭代：由于交通标志出现的频率和重要性不等，业务上对于部分类型 (如电子眼、限速牌等) 的准召率要求更高。效果方面：将检测目标根据外形特征分为 N 大类 (如圆形、三角形、方形，以及高宽比异常的人行横道等)，再为每一类配置专属的 RPN 网络，各个 RPN 根据对应的尺寸特性设计 Anchor 的 Ratio 和 Scale。交通标志检测技术已经在高德地图内部得到应用，有效提升了高德地图的数据制作效率，达成地图数据更新速度接近 T+0（时间差为零）的目标。

### [zkSNARK 合约「输入假名」漏洞致众多混币项目爆雷](https://www.tuicool.com/articles/m2iAva6)

> 概要: 为了防止「双花」发生，该函数还读取「废弃列表」，检查该证明的一个指定元素是否被标记过。A 出示一个 zkproof，证明自己知道一个 hash (HashA) 的 preimage，且这个 hash 在以 root 为标志的 tree 的叶子上，且证明这个 preimage 的另一种 hash 是 HashB。都与 Solidity 里的 uint256 类型有关.

### [🍒记录您的膳食卡路里，采用Next.js 9+Node+PostgreSQL开发](https://www.ctolib.com/VincentCordobes-the-green-meal.html)

> 概要: 🍒记录您的膳食卡路里，采用Next.js 9+Node+PostgreSQL开发

### [用Go编写的黑盒函数的贝叶斯优化框架](https://www.ctolib.com/c-bata-goptuna.html)

> 概要: 用Go编写的黑盒函数的贝叶斯优化框架

### [蚂蚁金服希望破解的两个问题：持续创新和企业边界](https://www.tuicool.com/articles/2amEF3I)

> 概要: 但从蚂蚁森林这个案例来看，蚂蚁金服的创新是自下而上的。胡晓明说，“蚂蚁金服不会服务全产业链的客户，我们只专注于长尾。”。所以当蚂蚁森林一热起来，很多人都觉得阿里是奔着游戏去了，他们甚至质疑马云在2008年说过的话，“饿死也不做游戏”。

### [一个 ZIO + http4s + Circe + Quill + Tapir giter8 模板](https://www.ctolib.com/pandaforme-ultron-g8.html)

> 概要: 一个 ZIO + http4s + Circe + Quill + Tapir giter8 模板

### [Introduction to Decentralisation](https://www.tuicool.com/articles/I3Qj2q2)

> 概要: In a nutshell , decentralisation is a system which allows a group of participants with common goals to have an equal share in responsibility , accountability and decision making ; there is no single authority or control . The Meaning of Decentralisation. 

### [一个基于openlayer6的一整套标绘SDK](https://www.ctolib.com/worlddai-plot_ol.html)

> 概要: 一个基于openlayer6的一整套标绘SDK

# 小说

### [大唐风月系列（全4本）](http://book.zongheng.com/book/792810.html)

> 作者：忧然

> 标签：历史军事

> 简介：本书讲述了长孙公主与李世民之间缠绵绯恻的爱情故事。情节感人，历史知识丰富，可读性强。本小说在网上发表以来，受到读者的一致好评，拥有很高的点击率与排行榜。

> 章节总数：大唐风月续：徐贤妃_番外：万丈雄心难为尼——武则天

> 状态：完本