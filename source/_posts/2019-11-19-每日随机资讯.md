---
title: 2019-11-19-每日随机资讯
tags: 资讯
thumbnail: https://cn.bing.com/th?id=OHR.ZionBirthday_EN-CN8486469709_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp
date: 2019-11-19 20:39:20
---


![Bing 每日壁纸](https://cn.bing.com/th?id=OHR.ZionBirthday_EN-CN8486469709_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp)
# 娱乐
### [组图：聂远亲自为宋轶示范亲密戏 与男导演接吻考拉抱引爆笑](http://slide.ent.sina.com.cn/tv/slide_4_704_327136.html)
> 概要: 组图：聂远亲自为宋轶示范亲密戏 与男导演接吻考拉抱引爆笑
### [组图：李现微卷短发率真随性散发迷人魅力 抓拍图被赞有质感](http://slide.ent.sina.com.cn/z/v/slide_4_704_327084.html)
> 概要: 组图：李现微卷短发率真随性散发迷人魅力 抓拍图被赞有质感
### [组图：《半是蜜糖半是伤》开机 罗云熙"奶奶裤"白鹿大衣造型惹眼](http://slide.ent.sina.com.cn/tv/slide_4_86512_327090.html)
> 概要: 组图：《半是蜜糖半是伤》开机 罗云熙"奶奶裤"白鹿大衣造型惹眼
### [组图：吴磊再登封复古高跟鞋穿搭吸睛 示范冬日熟男时尚](http://slide.ent.sina.com.cn/z/v/slide_4_704_327086.html)
> 概要: 组图：吴磊再登封复古高跟鞋穿搭吸睛 示范冬日熟男时尚
### [组图：李昇基鸟窝头造型拍时尚大片 半卧床气质撩人](http://slide.ent.sina.com.cn/star/k/slide_4_704_327094.html)
> 概要: 组图：李昇基鸟窝头造型拍时尚大片 半卧床气质撩人
### [港星向佐现场祭奠遭暴徒袭击遇难的清洁工罗伯：感谢他为香港做的事](https://new.qq.com/omn/20191119/20191119V0EJUI00.html)
> 概要: 港星向佐现场祭奠遭暴徒袭击遇难的清洁工罗伯：感谢他为香港做的事
### [39岁蔡依林霸气告白：“阿信，我要嫁给你”](https://new.qq.com/omn/20191118/20191118A0MV6E00.html)
> 概要: 文 | 小绿也五月天的演唱会上，蔡依林作为嘉宾出场，她对阿信说：听到台下突然变大声的尖叫，说真的，我被甜到了。然而甜的又岂止这句话。在这场演唱会中，他们不只牵手了。还上演了深情......
### [马思纯，被香奈儿选中的人](https://new.qq.com/omn/20191119/20191119A0LT3U00.html)
> 概要: 时尚不止是女明星的红毯专利，它更是每个平凡人追求美的权利。一种更具普遍性、包容度、永不过时的风格，才能在当下屹立长青。以年销售额超百亿美元位居全球奢侈品牌第一阵营的香奈儿，一直是现代独立女性主义的投影......
### [杨超越最新路透照曝光，一袭白衣仙气飘飘](https://new.qq.com/omn/20191119/20191119A0LW8B00.html)
> 概要: 11月19日杨超越新的一组路透照曝光，当天她身穿一件白紫古装，造型简单依旧难掩美貌，从路透照来看杨超越脸很小，皮肤状态也很在线。提到杨超越很多人觉得她是一个十分幸运的女孩，去年一档选秀节目让她的名字迅......
### [郭帆出席金鸡奖开幕论坛，透露《流浪地球》续集至少要等四年才拍](https://new.qq.com/omn/20191119/20191119A0LW8Z00.html)
> 概要: 2019年11月19日，第32届中国电影金鸡奖开幕论坛已经召开。此次开幕论坛郭帆也出席了，并且在开幕论坛上说出了一些关于电影《流浪地球》续集的消息......
# 动漫
### [【每日话题】当宗教遇上二次元 这样的观音你喜欢吗？](https://news.dmzj.com/article/65486.html)
> 概要: 最近，日本爱知县净泉寺的御朱印再一次吸引了人们的目光。这次净泉寺的印章着实令人大吃一惊，因为这实在是emmm尺度略大......
### [动画《人间失格》公开本篇前7分钟视频！](https://news.dmzj.com/article/65492.html)
> 概要: 根据太宰治的小说《人间失格》制作的剧场版动画《HUMAN LOST 人间失格》在日本视频平台“GYAO！”上公开了前7分钟的正片内容。
### [P站美图推荐——狼与香辛料特辑](https://news.dmzj.com/article/65491.html)
> 概要: 应求，本期主题为狼与香辛料。距离小说完结已过去了8年，贤狼赫萝与罗伦斯的行商（骗）之旅
### [Phat!《全金属狂潮 Invisible Victory》特蕾莎手办](https://news.dmzj.com/article/65490.html)
> 概要: Phat!根据《全金属狂潮 Invisible Victory》中的特蕾莎制作的1/7比例手办目前已经开订了。本作采用了特蕾莎身着女仆装手持P90时的造型。
### [进击的巨人：艾伦是否改变还是自导自演的本质，灭世如何获得自由](https://new.qq.com/omn/20191119/20191119A0LNQP00.html)
> 概要: 进击的巨人123话更新后，许多巨人粉丝们都在围绕着艾伦这次利用超大型巨人灭世决定展开了各种各样的猜测和分析。然而123话的剧情谏山老师采用三笠的回忆来阐述艾伦的举动，三笠的疑惑艾伦如今变成现在这个样……
### [汤姆和杰瑞的友谊，猫和老鼠](https://new.qq.com/omn/20191119/20191119A0823U00.html)
> 概要: 汤姆，你这只坏肥猫，可怜的小猫咪又要倒霉了。汤姆每次都有杰瑞的事，因为每次汤姆抓杰瑞都会把家里搞得乱七八糟的，可是主人每次都只看见汤姆，人类怎么会想到一只老鼠这么厉害，人类也不会往这想啊，所以每次倒……
### [叶罗丽第七季：新的圣级仙子即将登场，疑似罗丽公主的恋人](https://new.qq.com/omn/20191119/20191119A0GH4R00.html)
> 概要: 自火领主登场之后，叶罗丽这部动画已经许久没有新人物出现了，说实话一直看着这几位老熟人，多多少少还是有些枯燥的，但是今天喵娘有个好消息要告诉，在本周即将播出的叶罗丽第七季16集中将会有一位新的圣级仙子……
### [这3个奥特曼无人敢惹，每个都很厉害，第一名没人敢叫他的名字](https://new.qq.com/omn/20191119/20191119A0HBQK00.html)
> 概要: 这3个奥特曼无人敢惹，每个都很厉害，第一名没人敢叫他的名字。1、虐兽狂雷欧奥特曼。他是狮子座l77行星王国的大少爷，可以说是一个富二代，但是自从被星球毁灭之后，他身上就背负了一种巨大的责任，所以他从……
### [因为一个汉堡王子爱上灰姑娘，突破重重困难，最终寻回真爱！](https://new.qq.com/omv/video/e3023v72j91)
> 概要: 因为一个汉堡王子爱上灰姑娘，突破重重困难，最终寻回真爱！
### [《MEGALO BOX》要出第二季，网友吐槽：故事不是都讲完了吗！](https://new.qq.com/omn/20191119/20191119A0M6PY00.html)
> 概要: 在2018年4月新番里，有一部相当热血的动漫，名字叫《MEGALO BOX》，这部番主要讲拳击比赛，这可不像大家想象的那种拳击，而是穿上装备后两人互博，尤其是比赛中那种拳拳到肉的刺激感，让人汹涌澎湃……
# 科技
### [A laravel wrapper to use instagram API and to cache posts](https://www.ctolib.com/retinens-laravel-instagram.html)
> 概要: A laravel wrapper to use instagram API and to cache posts
### [GNES：通用神经网络弹性搜索 - 基于深度神经网络的原生云语义搜索系统](https://www.ctolib.com/hanxiao-gnes.html)
> 概要: GNES：通用神经网络弹性搜索 - 基于深度神经网络的原生云语义搜索系统
### [Sourcetrail - 免费和开源的跨平台源代码浏览器](https://www.ctolib.com/CoatiSoftware-Sourcetrail.html)
> 概要: Sourcetrail - 免费和开源的跨平台源代码浏览器
### [​ Appium Desktop是一款Appium更为优化的图形界面和appium相关的工具的组合](https://www.ctolib.com/appium-appium-desktop.html)
> 概要: ​ Appium Desktop是一款Appium更为优化的图形界面和appium相关的工具的组合
### [2019年11月设计圈超实用干货大合集](https://www.tuicool.com/articles/jmueQbZ)
> 概要: 接下来，恐怕要进入设计师最为忙碌的季节了！年底大量的工作需要完成，所以这次的设计圈干货合集当中，不仅汇集了一波最新的工具，而且还藏了一个有趣的设计游戏在里面，希望你能喜欢！屏幕尺寸工具 Screen ......
### [从“红利”到“实力”，感觉到难的不止你自己](https://www.tuicool.com/articles/6BRNfqR)
> 概要: 笔者写下自己参加2019北京产品经理大会的感触，也许此时的你正在深陷失落，也许你感觉前路艰辛，希望大家不要被眼前的困难打倒，多思多变，终能踏上属于自己的康庄大道。刚刚过去的这个周末，参加了北京站的20......
### [什么是超级符号？](https://www.tuicool.com/articles/yaAn6by)
> 概要: 一旦形成了超级符号，意味着品牌已经深入用户心智，其在用户心中的地位无法撼动，尤其是与竞品在市场中相遇时，用户往往会选择脑中已有的品牌。这几天在微信群看到一个好玩的东西，微信群聊天机器人，这个机器人有什......
### [如何设计真正高性能高并发分布式系统（万字长文）](https://www.tuicool.com/articles/jMZBRrA)
> 概要: “世间可称之为天经地义的事情没几样，复杂的互联网架构也是如此，万丈高楼平地起，架构都是演变而来，那么演变的本质是什么？”—1—引子软件复杂性来源于几个方面：高并发高性能、高可用、可扩展、低成本、低规模......
### [长征八号进入产品生产总装测试阶段，预计2020年首飞](https://www.ithome.com/0/457/971.htm)
> 概要: IT之家11月19日消息 据央视新闻报道，在中国国际商业航天高峰论坛上了解到，为满足商业航天发展需要，我国正在研制的新型中型运载火箭长征八号，目前已进入产品生产总装测试阶段，预计在2020年实施首飞......
### [淘宝店用马云做模特，相关产品被官方下架](https://www.ithome.com/0/457/963.htm)
> 概要: IT之家11月19日消息 据悉，近日一家淘宝店用马云做模特，相关产品被官方下架。原来，一家名为“化腾传奇旗舰店”的淘宝店铺，使用与马云相似的模特拍服装图，其相关产品均已被下架。该店铺称接受处罚，但也很......
### [《英雄联盟》2019年度颁奖盛典奖项提名正式公布：FPX战队获多项提名](https://www.ithome.com/0/457/937.htm)
> 概要: IT之家11月19日消息 《英雄联盟》赛事官微今日正式公布了《英雄联盟》2019年度颁奖盛典奖项提名名单，其中斩获S9全球总决赛冠军的FPX战队在多个奖项上均有提名。以下为各个奖项提名候选名单：• 2......
### [诺基亚在日本仙台沿海测试无人机海啸预警](https://www.ithome.com/0/457/968.htm)
> 概要: 诺基亚官方消息，近日，诺基亚基于数字自动化云的LTE专网，在日本仙台沿海地区成功进行了无人机的试飞，测试了在海啸或其他自然灾难中使用无人机进行预防和缓解灾害的可能性。该试验证明，基于LTE专网控制无人......
# 小说
### [随身红警玩修仙](http://book.zongheng.com/book/178417.html)
> 作者：咆哮的路灯

> 标签：武侠仙侠

> 简介：带着红警来到了修仙的世界。基地变成了洞府，采矿场变成了丹炉……地面部队我有超时空兵、海豹部队、狙击手、天启坦克、幻影坦克、光陵坦克……防守部队我有光陵塔、裂缝产生器、爱国者……海军我有好，航空母舰、潜水艇……空军我有飞行兵、夜鹰……修仙模式下，或变身法宝，或变身法术，随身红警玩修仙，我玩动整个修仙界！买断作品，放心收藏，多多投票，点击也要，嘿嘿嘿……

> 章节末：第三百零四章   天道我也杀

> 状态：完本
### [某废柴的召唤之门](http://book.zongheng.com/book/612355.html)
> 作者：你可以叫我老金

> 标签：奇幻玄幻

> 简介：在某一天，一个废柴穿越到了异界，立志依靠金手指成王成神，过上数钱数到梦醒，做梦做到手抽筋的日子，他会成功么？注：本故事纯属虚构如有雷同纯属巧合！书友群已经建立QQ：469701491

> 章节末：尚未完结的故事

> 状态：完本
# 游戏
### [《足球经理2020》IGN 8.7 M站85分 最佳入坑作](https://www.3dmgame.com/news/201911/3776152.html)
> 概要: 今天（11月19日）游戏媒体 IGN 为《足球经理2020》打出了8.7的评分，并表示《足球经理2020》依旧如系列前作一样让人上瘾，游戏规模也依旧庞大，并且本作也更加适合于新玩家入坑，是这个精彩系列......
### [《P5》29日公开动画特别篇 《P5S》新信息届时发布](https://www.3dmgame.com/news/201911/3776183.html)
> 概要: 今天（11月19日）《女神异闻录5》官方宣布，《P5》动画剧集将于11月29日公开特别篇【純喫茶ルブラン屋根裏放送局Ｓ】，同时《女神异闻录5：乱战 幻影打击者》届时也将公开最新消息。《女神异闻录5》动......
### [新主机PS5、Xbox Scarlett的CPU频率提高了约50%](https://www.3dmgame.com/news/201911/3776145.html)
> 概要: 次世代主机PS5和Xbox Scarlett均在2020年年末发售，两者都旨在提供4K分辨率游戏（也有可能会有更高的8K分辨率），并且两者在功耗方面都在竞争。但是，它们都使用了AMD的Zen 2 CP......
### [国产《光明记忆：无限》脸部捕捉预告 女主新形象](https://www.3dmgame.com/news/201911/3776151.html)
> 概要: 近日国产FPS《光明记忆：无限》官方公布了PC版在2019年末的更新计划，并表示正全力开发PC版。更新计划为11月19日：《光明记忆：无限》女主重新设计形象公布，脸部捕捉技术展示预告放出。11月21日......
### [形式不同各有寄托！盘点世界各国如何怀念故人](https://www.3dmgame.com/bagua/2206.html)
> 概要: 生老病死乃人之常情，虽然世界各国风土人情都不同，不过怀念已故旧人的仪式总是有的，比如刚刚过去的万圣节，一起来看看世界各国都是怎么怀念逝去的死者的。·墨西哥的梅里达。每年10月底“死者之日”到来，众人参......
# 论文
### [A novel centroid update approach for clustering-based superpixel method and superpixel-based edge detection](https://paperswithcode.com/paper/a-novel-centroid-update-approach-for)
> 日期：18 Oct 2019

> 标签：EDGE DETECTION

> 代码：https://github.com/ProfHubert/ICASSP

> 描述：Superpixel is widely used in image processing. And among the methods for superpixel generation, clustering-based methods have a high speed and a good performance at the same time.
### [Asymmetric Non-local Neural Networks for Semantic Segmentation](https://paperswithcode.com/paper/asymmetric-non-local-neural-networks-for)
> 日期：21 Aug 2019

> 标签：SEMANTIC SEGMENTATION

> 代码：https://github.com/MendelXu/ANN

> 描述：The non-local module works as a particularly useful technique for semantic segmentation while criticized for its prohibitive computation and GPU memory occupation. In this paper, we present Asymmetric Non-local Neural Network to semantic segmentation, which has two prominent components: Asymmetric Pyramid Non-local Block (APNB) and Asymmetric Fusion Non-local Block (AFNB).
