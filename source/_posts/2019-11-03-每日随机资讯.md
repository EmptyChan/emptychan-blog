---
title: 2019-11-03-每日随机资讯
tags: 资讯
thumbnail: /images/周日.png
date: 2019-11-03 20:01:56
---

# 娱乐
### [组图：倪萍盘发穿墨绿丝绸裙尽显优雅 表演朗诵情到浓时潸然泪下](http://slide.ent.sina.com.cn/z/v/slide_4_704_325709.html)
> 概要: 组图：倪萍盘发穿墨绿丝绸裙尽显优雅 表演朗诵情到浓时潸然泪下
### [梁静发长文谈参加综艺：这就是我要的重启与激活](https://ent.sina.com.cn/tv/zy/2019-11-03/doc-iicezzrr6876543.shtml)
> 概要: 梁静发长文谈参加综艺：这就是我要的重启与激活......
### [费玉清时隔六年香港再开唱 退休后不敢养狗想养鸡](https://ent.sina.com.cn/y/ygangtai/2019-11-03/doc-iicezzrr6908845.shtml)
> 概要: 新浪娱乐讯 北京时间11月3日消息，据香港媒体报导，现年64岁的“金钟歌王”费玉清昨晚（11月2日）在红馆举行演唱会，相隔6年没有在香港开演唱会，他以歌声跟香港歌迷说再见，这是巡回告别演唱会的倒数第三......
### [组图：郑容和退伍后现身 精神抖擞露招牌微笑气质硬朗](http://slide.ent.sina.com.cn/star/k/slide_4_704_325711.html)
> 概要: 组图：郑容和退伍后现身 精神抖擞露招牌微笑气质硬朗
### [组图：妮基·希尔顿穿小香风外套名媛范儿足 笑容甜气质出众](http://slide.ent.sina.com.cn/star/h/slide_4_704_325729.html)
> 概要: 组图：妮基·希尔顿穿小香风外套名媛范儿足 笑容甜气质出众
### [关晓彤当年拍摄广告未P图照曝光 终于知道鹿晗为什么这么爱她](https://new.qq.com/omv/video/r3016f4z0dt)
> 概要: 关晓彤当年拍摄广告未P图照曝光 终于知道鹿晗为什么这么爱她
### [于小彤夺冠后亲陈小纭，网友集体喊话周震南：这么明目张胆不好吧](https://new.qq.com/omn/20191102/20191102A0M2AI00.html)
> 概要: 在舞台上，周震南是毋庸置疑的“王者”，然而在上网冲浪这方面，他的确是个“弟弟”，号称“村里刚通网”的他，在参加“超新星全运会”时，就闹了出笑话。因为并不知道陈小纭是于小彤的女朋友，所以还以为自己掌握了......
### [柳岩示范给领导敬酒：说一句话洒一半，还没喝到酒杯就空了](https://new.qq.com/omn/20191103/20191103V0BD6A00.html)
> 概要: 柳岩示范给领导敬酒：说一句话洒一半，还没喝到酒杯就空了
### [靳东新剧女主角被易角，宁赔巨额违约金也要辞演，网友：太酷了](https://new.qq.com/omv/video/z3016wdzcat)
> 概要: 靳东新剧女主角被易角，宁赔巨额违约金也要辞演，网友：太酷了
### [他被海水泡到不成人样，飞鱼王子出走半生，归来仍是少年](https://new.qq.com/omn/20191103/20191103A07BYF00.html)
> 概要: 近日天气转凉，走在路上明显感觉到秋风刺骨，对于很多人而言是倍感凉爽，而对于方力申而言，可能是增加了不少烦恼。 因为下周二（11月5日），方力申将参加“香港环岛45公里慈善挑战”，为公益组织“点滴是生命......
# 动漫
### [日本网友投票2019年秋番动画女性声优人气榜](https://news.dmzj.com/article/65265.html)
> 概要: 日本网站“秋叶原总研”公开了网友投票的“2019年秋番动画女性声优人气榜”的最终结果。这次投票共有6998票，声优内田彩以1193票的成绩排名第一。
### [动画《某科学的超电磁炮T》2020年1月10日开播](https://news.dmzj.com/article/65266.html)
> 概要: 根据电击文库（KADOKAWA）的人气轻小说《魔法禁书目录》的外传《某科学的超电磁炮》制作的TV动画《某科学的超电磁炮T》宣布了将于2020年1月10日起，开始在TOKYO MX等电视台依次播出的消息。
### [儿子解除父亲iPhoneX的面部识别锁 在游戏中氪金1千万韩元](https://news.dmzj.com/article/65264.html)
> 概要: 根据韩国媒体报道，使用iPhoneX的金先生在一天被自己的信用卡账单吓到了。账单上面莫名其妙的多出了在Apple store中向游戏支付1000万韩元（约合人民币6万元左右）的消费记录。
### [迷你世界吃鸡动画第8集：熊孩子陷入沼泽迷梭梭登场](https://new.qq.com/omv/video/w3016d7rcj4)
> 概要: 迷你世界吃鸡动画第8集：熊孩子陷入沼泽迷梭梭登场
### [龙珠：未来篇中，为何没人寻找娜美克星的龙珠呢？悟空：不是不想，是做不到](https://new.qq.com/omn/20191103/20191103A0F10000.html)
> 概要: 大家好，我是桃月！《龙珠》中，最令人疼惜的战士当属未来世界的特南克斯了，这个温暖的少年从小背负着人类的兴衰存亡。肩挑万钧，以世界和平为己任；舍命前行，带领人类走向黎明。一方面，未来的大特让人感到疼惜……
### [最疼阿羡的师姐，为保护阿羡惨死，羡羡当场失控](https://new.qq.com/omv/video/m3016cz5kzf)
> 概要: 最疼阿羡的师姐，为保护阿羡惨死，羡羡当场失控
### [《魁拔》真人版电影已经默默开机，粉丝：5毛特效等着差评吧！](https://new.qq.com/omn/20191103/20191103A02DUQ00.html)
> 概要: “白落提，丰和，英宋，桓泽金，广秀，与魁拔1026年参加神圣联盟东部战区夜战小队，与魁拔英勇作战，共计参战171次，杀敌526名，与五天前在魁拔营地阵亡，我是他们的长官雾妖族妖侠幽弥狂！”在《魁拔》……
### [《绝命响应周周见》22 黑云压顶！临极岛起纷争！](https://new.qq.com/omv/video/r30168uw729)
> 概要: 《绝命响应周周见》22 黑云压顶！临极岛起纷争！
### [海贼王：凯多秒路飞已经一年，再相遇路飞还会被秒吗](https://new.qq.com/omn/20191103/20191103A0CZFU00.html)
> 概要: 前两天是万圣节，不知道大家过的还开心吗？但海贼王中的主角路飞过得肯定不开心，为什么这么说呢？因为按照国内海贼王漫画情报发出的时间推测，前两天正好也是路飞被凯多一击揍飞的一周年纪念日。你说路飞能开心的……
# 科技
### [pod-merge是一个Cocoapods插件，用于合并Xcode项目使用的依赖项（或pod）](https://www.ctolib.com/grab-cocoapods-pod-merge.html)
> 概要: pod-merge是一个Cocoapods插件，用于合并Xcode项目使用的依赖项（或pod）
### [提供对YOLOv3及Tiny的多种剪枝版本，以适应不同的需求](https://www.ctolib.com/coldlarry-YOLOv3-complete-pruning.html)
> 概要: 提供对YOLOv3及Tiny的多种剪枝版本，以适应不同的需求
### [模仿axios实现自定义网络请求拦截器,支持微信小程序拦截,ajax拦截,支付宝小程序等](https://www.ctolib.com/Weibozzz-http-interceptor.html)
> 概要: 模仿axios实现自定义网络请求拦截器,支持微信小程序拦截,ajax拦截,支付宝小程序等
### [Bayard是用Rust编写的全文本搜索和索引服务器](https://www.ctolib.com/mosuka-bayard.html)
> 概要: Bayard是用Rust编写的全文本搜索和索引服务器
### [损失函数串讲](https://www.tuicool.com/articles/fUvEBz3)
> 概要: 本来是不想起这么大而空的名字的，但是今天确实特别自闭，所以想来个刺激一点标题刺激一下自己，也刺激一下别人。  时至今日，距离上一篇blog更新已经过去快九个月了，距离我在MSRA实习已经快八个月了，这......
### [截胡未来，或稳固当下：在教育市场缠斗的谷歌与苹果](https://www.tuicool.com/articles/R3Ybqan)
> 概要: 编者按：本文来源创业邦专栏脑极体。  当中国家长还在纠结如何让孩子远离iPad时，苹果与谷歌已经在教育市场中打得风声四起了。  或许提起笔记本电脑或平板电脑，大多数人的反应还是它们属于成年人的办公娱乐......
### [在图像生成领域里，GAN这一大家族是如何生根发芽的](https://www.tuicool.com/articles/mIRNBzU)
> 概要: 生成对抗网络这一 ML 新成员目前已经枝繁叶茂了，截止今年 5 月份，目前 GAN 至少有 300+的论文与变体。而本文尝试借助机器之心 SOTA 项目梳理生成对抗网络的架构与损失函数发展路径，看看 ......
### [23 个重难点突破，带你吃透 Service 知识点「长达 1W+ 字」](https://www.tuicool.com/articles/NF7Zrmb)
> 概要: 前言  学  Android  有一段时间了，想必不少人也和我一样，平时经常东学西凑，感觉知识点有些凌乱难成体系。所以趁着这几天忙里偷闲，把学的东西归纳下，捋捋思路。  这篇文章主要针对  Servi......
### [阿里零售通的渠道与物流变革](https://www.tuicool.com/articles/R3yaMzQ)
> 概要: 2016年11月，马云在杭州云栖大会上提出未来五大新趋势，其中第一个就是新零售。他认为，纯电商时代已经过去，未来只有新零售这一说法，也就是说线上线下和物流必须结合在一起，才能诞生真正的新零售。快消B2......
### [对话蚂蚁金服蒋国飞：蚂蚁区块链的激励机制并不需要依靠空气币](https://www.tuicool.com/articles/vYrMvyj)
> 概要: 编者按：本文为创业邦原创，作者邓双琳，未经授权不得转载。  “做一个简单的区块链应用很简单，但要做一个有深度、专业级的应用却很难。”外界对区块链行业的猜测纷至沓来，蚂蚁金服集团副总裁、蚂蚁区块链老大蒋......
### [头脑风暴没你想的那么万能！](https://www.tuicool.com/articles/fm6bYfI)
> 概要: 本文分析了头脑风暴可能存在的问题，以及如何通过横向思维对创意进行评价，并介绍了6种不同的横向思维方法。  大约十年前，我参加了一个销售和营销课程。这个历时数周的课程不仅名声在外，而且费用昂贵。尽管当时......
### [Flutter中展示Base64图片](https://www.tuicool.com/articles/zeU3AnM)
> 概要: 在flutter中的  Base64Decoder().convert  方法的的参数是base64图片中;后面的部分。所以我们需要进行截取，实现的主要代码如下：  import dart:conve......
# 小说
### [宰执天下](http://book.zongheng.com/book/69507.html)
> 作者：cuslaa

> 标签：历史军事

> 简介：宰者宰相，执者执政。    上辅君王，下安黎庶，群臣避道，礼绝百僚，是为宰相。    佐政事，定国策，副署诏令，为宰相之亚，是为执政。    因为一场空难，贺方一迈千年，回到了传说中‘积贫积弱’同时又‘富庶远超汉唐’的北宋。一个贫寒的家庭，一场因贪婪带来的灾难，为了能保住自己小小的幸福，新生的韩冈开始了向上迈进的脚步。    这一走，就再也无法停留。逐渐的，他走到了他所能达到的最高峰。在诸多闪耀在史书中的名字身边，终于寻找到了自己的位置。读者第一群：127748272（高级群，已满）读者第二群：31711604（招募中）读者第三群：177233103（招募中）

> 章节末：第329章 宰制

> 状态：完本
### [阵地繁星](http://book.zongheng.com/book/705274.html)
> 作者：肖冀平

> 标签：评论文集

> 简介：忆往昔，志存高远，自有风雨话沧桑。看今朝，厚德载物，更续辉煌写华章。进入新世纪以来，为适应巴州跨越式的大发展思路，巴州党校从2002年开始以“六化”为标准（教学精品化、科研务实化、队伍年轻化、管理规范化、服务人性化、环境园林化），以“八大文化”为核心（三从严、三要三不要、三风建设、三和谐、教学三原则、师德要义、六化标准、三大目标），形成了校园风清气正，教学百舸争流，科研硕果累累，队伍能打硬仗，服务以人为本，管理规范有序，环境优美宜人。党校人用“艰苦奋斗、敢为人先”的精神，用“事业至上、追求一流”的态度，用“脚踏实地、实实在在”的行动成就了巴州党校鲜明的办学特色，形成了严谨科学的学风、重在育人的校风、脚踏实地的作风，在巴州的大地上不断书写属于党校人的责任与荣光。

> 章节末：附录：改革探索二题

> 状态：完本
# 论文
### [TripleNet: Triple Attention Network for Multi-Turn Response Selection in Retrieval-based Chatbots](https://paperswithcode.com/paper/triplenet-triple-attention-network-for-multi)
> 日期：24 Sep 2019

> 标签：

> 代码：https://github.com/wtma/TripleNet

> 描述：We consider the importance of different utterances in the context for selecting the response usually depends on the current query. In this paper, we propose the model TripleNet to fully model the task with the triple <context, query, response> instead of <context, response> in previous works.
### [Policy Poisoning in Batch Reinforcement Learning and Control](https://paperswithcode.com/paper/policy-poisoning-in-batch-reinforcement)
> 日期：13 Oct 2019

> 标签：

> 代码：https://github.com/myzwisc/PPRL_NeurIPS19

> 描述：We study a security threat to batch reinforcement learning and control where the attacker aims to poison the learned policy. The victim is a reinforcement learner / controller which first estimates the dynamics and the rewards from a batch data set, and then solves for the optimal policy with respect to the estimates.

