---
title: 2019-10-29-每日随机资讯
tags: 资讯
thumbnail: /images/周二.png
date: 2019-10-29 22:39:31
---

# 娱乐
### [组图：麦莉和男友科迪外出约会 穿着休闲时尚边走边聊](http://slide.ent.sina.com.cn/y/slide_4_704_325217.html)
> 概要: 组图：麦莉和男友科迪外出约会 穿着休闲时尚边走边聊
### [赖声川：倪妮是可以打满分的好学生](https://ent.sina.com.cn/j/drama/2019-10-29/doc-iicezzrr5616361.shtml)
> 概要: 羊城晚报记者 艾修煜 　　10月25日至11月3日，为期10天的第七届乌镇戏剧节（以下简称“乌戏”）在浙江乌镇举行。今年，乌戏以“涌”为题，共有141场特邀剧目演出、18出青年竞演、1800余场古镇嘉......
### [视频：杨紫为拍戏不要形象 冲镜头对眼吐舌头征求导演意见](http://video.sina.com.cn/p/ent/2019-10-29/detail-iicezzrr5772394.d.html)
> 概要: 视频：杨紫为拍戏不要形象 冲镜头对眼吐舌头征求导演意见
### [徐锦江曝会用自己的表情包 明年会复出拍戏](https://ent.sina.com.cn/tv/zy/2019-10-29/doc-iicezzrr5668890.shtml)
> 概要: 新浪娱乐讯，随着综艺 的热播，“拜月教主”徐锦江 在节目中的表现可谓是成功引起了网友的注意。这个曾出演过杀伐果断的鳌拜、阴险毒辣的拜月教主、性烈如火的金毛狮王的徐锦江，在综艺节目中不仅没有霸气威武的性......
### [组图：吴宣仪穿“小香风”温婉大方 眼神楚楚动人似小鹿](http://slide.ent.sina.com.cn/star/slide_4_704_325263.html)
> 概要: 组图：吴宣仪穿“小香风”温婉大方 眼神楚楚动人似小鹿
### [这位以烧脑著称的导演，刚刚被奥斯卡“封神”](https://new.qq.com/omn/20191029/20191029A0OBHE00.html)
> 概要: 第92届奥斯卡金像奖将于北京时间2020年2月9日颁出，在此之前，奥斯卡终身成就奖已于近日先行颁发。这个奖项为国人所关注还是在2016年，这一年成龙获此殊荣。 今年的奥斯卡终身成就奖颁给了导演大卫·林......
### [高云翔案女当事人现身曝光细节，律师称案件发展或受女方颜值影响](https://new.qq.com/omn/20191029/20191029A0HQMG00.html)
> 概要: 历时一年零七个月的高云翔案终于进入了关键阶段，在开启了10月28日的庭审之后，29日该案继续进行了第二日庭审。而且在当天的庭审中，除了被告高云翔和王晶现身之外，女当事人也通过视讯与检控官进行问答，并借......
### [从玉檀到顾初，观众提名的古装女神必有她的名字！](https://new.qq.com/omn/20191029/20191029A0NRJB00.html)
> 概要: 正在热播的《我知道你的秘密》是一部法医视角的刑侦剧，通过蛛丝马迹为观众剥去层层案件疑云，还原事件真相。喜欢看刑侦剧的小伙伴一定不要错过了！《我......
### [从音乐角度看泛文娱发展，由你音乐榜是如何做到的？](https://new.qq.com/omn/20191029/20191029A0OEW700.html)
> 概要: 由你音乐榜正在一点点记录我们当下音乐市场的变化，以及透过音乐观察到泛文娱的发展。 读娱 | yiqiduyu 文 | 林不二子 随着各文娱产业跨领域融合的脚步愈加频繁，泛文娱的多种玩法也愈加成熟，而其......
### [被传与鹿晗领证后关晓彤现身街头，身材遭吐槽：肚子太大用腰带勒](https://new.qq.com/omn/20191029/20191029A08X9A00.html)
> 概要: 日前，有爆料称关晓彤和鹿晗会在第二天官宣领证结婚，结果热闹了一整夜加上第二天一白天……结果神马事儿都没发生。粉丝和一些吃瓜群众的期待落空了，纷纷跑到爆料者的V博评论区进行......
# 动漫
### [索尼在日本注册PS6到PS10的商标！](https://news.dmzj.com/article/65177.html)
> 概要: 根据日本商标查询网站的消息，索尼互动娱乐注册了“PS6”、“PS7”、“PS8”、“PS9”和“PS10”五项商标。
### [峰奈由佳基于自身经验创作半自传漫画《AV女优酱》](https://news.dmzj.com/article/65181.html)
> 概要: ​在本日（29日）发售的周刊SPA！11月5·12日合并号（扶桑社）上，由峰奈由佳创作的新漫画《AV女优酱》开始连载了。
### [2020年版“想去的日本动画圣地88”名单公开](https://news.dmzj.com/article/65178.html)
> 概要: 由一般社团法人动画旅游业协会选定发布的《想去的日本动画圣地88》公开了最新的2020年版。
### [宫本茂和萩尾望都被日本政府评为文化功劳者](https://news.dmzj.com/article/65175.html)
> 概要: ​日本政府在本日（29日）发布了2019年度文化勋章的6名获得者和21名文化功劳者名单，其中任天堂的游戏制作人宫本茂（66岁）和创作过《波族传奇》的漫画家萩尾望都（70岁）被评为文化功劳者。
### [你所知的动漫里，哪些角色可以算得上是精明的老板？](https://new.qq.com/omn/20191029/20191029A0OK5200.html)
> 概要: 动漫角色中精明的老板多数都是反派，当然也不全是，反派老板算计主角的时候才更能显现出老板的精明。我比较喜欢的《海贼王》中就有三位很精明的老板。第一位是阿拉巴斯坦的克洛克达尔，不仅是精明还非常的小心，甚……
### [吃货部分国界，甚至不分次元，动漫中那些年和我们一起成长的吃货](https://new.qq.com/omn/20191029/20191029A0O6FG00.html)
> 概要: 对吃的追求是人们刻在DNA中的渴求，这一点就连身在次元另一边的人物们也不曾例外，对于能吃是福他们也有一套属于自己的解释，那么，那些年动漫中有哪些和我们一起成长的吃货呢？《魔法禁书目录》茵蒂克丝茵蒂克……
### [国动有个传说叫上美制片厂，曾创造无数回忆，如今境遇如何？](https://new.qq.com/omn/20191029/20191029A0O09800.html)
> 概要: 在国产动画片中一直有个名叫上海美术制片厂的传说，这个曾经标志着国动巅峰的机构产出了如《哪吒闹海》、《大闹天宫》、《葫芦兄弟》、《舒克贝塔》、《宝莲灯》、《邋遢大王》、《我为歌狂》等等上千部作品，屡获……
### [《鬼灭之刃》主角团长大了，善逸扎起马尾，香奈乎很帅](https://new.qq.com/omn/20191029/20191029A0O46A00.html)
> 概要: 《鬼灭之刃》是一部人类对抗鬼的动画，按理说，杀鬼是大人的事情。可在动画中，主角团却是几个孩子。他们年纪不大，却有很强的实力，天赋也非常不错，而且十分团结。从漫画来看，无惨被打败后，故事很可能会完结，……
### [进击的巨人：细数作品中那些富有哲理的“名句”，析析咩，踏踏开！](https://new.qq.com/omn/20191029/20191029A0NTJS00.html)
> 概要: 1.人如果不沉醉于某些东西，估计都撑不下的吧！所有人都是某些东西的奴隶！2.什么都无法舍弃的人什么都改变不了——阿尔敏3.就算相信自己的实力，相信值得信赖的同伴的选择，但还是没有人知道结果会怎样。所……
### [还记得我们可爱的血小板吗？](https://new.qq.com/omn/20191029/20191029A0NV8E00.html)
> 概要: 还记得我们可爱的血小板吗？还记得我们可爱的血小板吗？还记得我们可爱的血小板吗？还记得我们可爱的血小板吗？还记得我们可爱的血小板吗？还记得我们可爱的血小板吗？
# 科技
### [基于Spring Boot+Spring Security+JWT+Vue前后端分离的基础项目](https://www.ctolib.com/zhengqingya-xiao-xiao-su.html)
> 概要: 基于Spring Boot+Spring Security+JWT+Vue前后端分离的基础项目
### [imgcrypt: 为容器提供API扩展以支持加密的容器映像](https://www.ctolib.com/containerd-imgcrypt.html)
> 概要: imgcrypt: 为容器提供API扩展以支持加密的容器映像
### [code-server：在远程服务器上运行的VS Code，可通过浏览器访问](https://www.ctolib.com/cdr-code-server.html)
> 概要: code-server：在远程服务器上运行的VS Code，可通过浏览器访问
### [PyTorch概率模型强化学习算法库](https://www.ctolib.com/mcgillmrl-prob_mbrl.html)
> 概要: PyTorch概率模型强化学习算法库
### [3000 万人摆地摊，小买卖，大生意](https://www.tuicool.com/articles/Q7JFBnQ)
> 概要: 摆地摊，竟然这么赚钱！  来源 |  十里村（ID：  shilipxl）  文 |  天涯住在十里村  头图来源 |  视觉中国  一提到流动商贩（街边地摊），很大一部分人的刻板反应是：小生意、......
### [快手推出“篮球光合计划”，宣布50亿流量扶持篮球视频创作者](https://www.tuicool.com/articles/FfAFza2)
> 概要: 速途网10月29日消息（报道：李楠）今日，“国民篮球生态论坛”在北京举办，快手科技创始人兼CEO宿华与中国篮协主席、CBA联盟董事长姚明共同开启快手篮球光合计划。快手宣布将与CBA携手在内容生产者激励......
### [一文看懂 NLP 里的模型框架 Encoder-Decoder 和 Seq2Seq](https://www.tuicool.com/articles/jEfMbeb)
> 概要: Encoder-Decoder 和 Seq2Seq  本文将详细介绍 Encoder-Decoder、Seq2Seq 以及他们的升级方案Attention。什么是 Encoder-Decoder......
### [“油条大王”王冬：要让中国快餐遍布世界](https://www.tuicool.com/articles/byU3Mfr)
> 概要: 从豆浆油条起家，直到打造出誉满京城的知名“中国饭”品牌，王冬花了19年。在餐饮界摸爬滚打，而关于创业王冬只有一句总结：“坚持是胜利的唯一标准”。 正在刷屏的连续剧《在远方》以中国快递行业“草根创业”......
### [新版“人人”重回校园，但错过的青春可能回不来了](https://www.tuicool.com/articles/2yI3YbY)
> 概要: 图片来源@视觉中国  “你的青春在这里，人人的明天你决定。”  带着这样的标语，新版“人人”在App store悄然上线，一些老粉丝感慨道，自己的青春又回来了。 产品介绍页写道，“这是中国最悠久的校......
### [Cassandra实践|如何通过火焰图快速定位Cassandra性能瓶颈](https://www.tuicool.com/articles/2Qj6Vn3)
> 概要: Cassandra赠书 | Cassandra SASI Index 技术解密  运维大规模分布式系统的比较重要的一个挑战是可以有能力指出关键问题所在。在没有证据支持某种说法的情况下，当故障出现时，总......
### [弹幕、留言、互动，虾米电台为何一改往常？](https://www.tuicool.com/articles/zIj26vv)
> 概要: 电台是音乐 APP 基于算法为用户个性化推荐歌曲的功能。今年以来，市面上主流的音乐产品均针对该功能进行集中升级。  被 VFine Music 收购后，豆瓣 FM 于今年 6 月上线了 6.0 版本 ......
### [Axure教程：两个原件实现“验证码倒计时”效果](https://www.tuicool.com/articles/bQ7NvqN)
> 概要: 今天给大家带来的教程内容是，怎样用两个原件去实现“验证码倒计时”的设计，一起来看看~  接下来正式开始教学：实现这一效果我们只运用到了两个原件（没错，就是两个！），“按钮”和......
# 小说
### [梦回武唐春](http://book.zongheng.com/book/416121.html)
> 作者：轮舞

> 标签：历史军事

> 简介：一觉睡醒起来，李遥莫名其妙穿越到了唐朝，成为了中国历史上第一个女皇帝，一生中的第一个男宠，从此李遥过上了水深火热的生活，女皇，公主，女官，女侍等等，纷至踏来，oh！mygod！谁来拯救李遥，让他逃离这……这幸福的男宠生活！

> 章节末：第416章 我叫吕蒙（彩蛋）

> 状态：完本
### [独步昆仑](http://book.zongheng.com/book/640375.html)
> 作者：真雨

> 标签：奇幻玄幻

> 简介：新书《截教真仙》以发，本书已签约请放心收藏。

> 章节末：第四百二十五章  开新书了

> 状态：完本
# 论文
### [SpatialNLI: A Spatial Domain Natural Language Interface to Databases Using Spatial Comprehension](https://paperswithcode.com/paper/spatialnli-a-spatial-domain-natural-language)
> 日期：28 Aug 2019

> 标签：READING COMPREHENSION

> 代码：https://github.com/VV123/SpatialNLI

> 描述：A natural language interface (NLI) to databases is an interface that translates a natural language question to a structured query that is executable by database management systems (DBMS). However, an NLI that is trained in the general domain is hard to apply in the spatial domain due to the idiosyncrasy and expressiveness of the spatial questions.
### [Residual Reactive Navigation: Combining Classical and Learned Navigation Strategies For Deployment in Unknown Environments](https://paperswithcode.com/paper/residual-reactive-navigation-combining)
> 日期：24 Sep 2019

> 标签：

> 代码：https://github.com/krishanrana/2D_SRRN

> 描述：In this work we focus on improving the efficiency and generalisation of learned navigation strategies when transferred from its training environment to previously unseen ones. We present an extension of the residual reinforcement learning framework from the robotic manipulation literature and adapt it to the vast and unstructured environments that mobile robots can operate in.
