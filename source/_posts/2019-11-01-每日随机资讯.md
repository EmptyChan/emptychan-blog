---
title: 2019-11-01-每日随机资讯
tags: 资讯
thumbnail: /images/周五.png
date: 2019-11-01 20:31:08
---

# 娱乐
### [组图：郭富城妻子方媛晒万圣节小丑妆 抹胸黑裙一字锁骨吸睛](http://slide.ent.sina.com.cn/star/slide_4_704_325636.html)
> 概要: 组图：郭富城妻子方媛晒万圣节小丑妆 抹胸黑裙一字锁骨吸睛
### [组图：热依扎穿格纹裙外搭披风变女巫 对镜甜笑状态好](http://slide.ent.sina.com.cn/star/slide_4_704_325635.html)
> 概要: 组图：热依扎穿格纹裙外搭披风变女巫 对镜甜笑状态好
### [组图：马剑越产后晒母子合照气色好 眼神满是爱意望向小三条](http://slide.ent.sina.com.cn/star/w/slide_4_86512_325633.html)
> 概要: 组图：马剑越产后晒母子合照气色好 眼神满是爱意望向小三条
### [组图："忘川夫妇"未曝光超美定妆照放出 东宫女孩直呼"看不够"](http://slide.ent.sina.com.cn/tv/slide_4_704_325637.html)
> 概要: 组图："忘川夫妇"未曝光超美定妆照放出 东宫女孩直呼"看不够"
### [章子怡晒东京电影节vlog 评审工作忙收工后陪醒醒](https://ent.sina.com.cn/m/c/2019-11-01/doc-iicezzrr6548902.shtml)
> 概要: 新浪娱乐讯 11月1日，章子怡 在微博晒出东京电影节之行的vlog，并配文称：“愿我们工作时永存无尽的光芒，愿我们都活成自己想要的模样。” 视频里不仅有章子怡忙碌的评审工作，出席各个论坛现场，发表讲话......
### [48岁瞿颖舞台秀美腿，献唱经典歌《沉默加速度》，魅力不减当年](https://new.qq.com/omn/20191101/20191101V05M7B00.html)
> 概要: 48岁瞿颖舞台秀美腿，献唱经典歌《沉默加速度》，魅力不减当年
### [赵丽颖生娃后拍新戏，高反严重不停吸氧，职场女性也太拼了！](https://new.qq.com/omv/video/z00323zmafe)
> 概要: 赵丽颖生娃后拍新戏，高反严重不停吸氧，职场女性也太拼了！
### [邓超送翡翠耳环，孙俪一脸嫌弃，吐槽如“刮痧板”](https://new.qq.com/omn/20191101/20191101A0DF9W00.html)
> 概要: 邓超送翡翠耳环，孙俪一脸嫌弃，吐槽如“刮痧板”
### [周杰伦昆凌庆万圣节，甜蜜cosplay大秀恩爱，萧敬腾你太抢镜了！](https://new.qq.com/omn/20191101/20191101A0MSPI00.html)
> 概要: 不知道小伙伴们今年的万圣节是怎么庆祝的呢？一起来看看我们的“小公举”周杰伦的万圣节吧！周董可以说是童心满满，居然约着好友cosplay玩具总动员。周杰伦在社交软件上大晒万圣节合影，连节日也不忘双手搂住......
### [熬不过是崔雪莉，熬过去了就是李孝利](https://new.qq.com/omn/20191101/20191101A0D1UR00.html)
> 概要: 雪莉事件终于告一段落了。 无他杀可能、遗体已经安葬，亲朋好友和粉丝都去陪伴了最后一程。               之前网上轰轰烈烈的猜测，也慢慢归于平静。 关于这一事件延伸的最新讨论是：在“吃人”的......
# 动漫
### [P站美图推荐——南瓜特辑](https://news.dmzj.com/article/65255.html)
> 概要: 鉴于万圣节美图已经有了，所以本期的主题是万圣节必不可少的......南瓜- -
### [游戏《碧蓝航线Crosswave》的Steam版2020年春发售](https://news.dmzj.com/article/65254.html)
> 概要: Compile Heart根据手机游戏《碧蓝航线》制作的3D动作类《碧蓝航线Crosswave》，宣布了将于2020年春在Steam平台上发售。
### [11月1日万圣节！献上万圣节主题图片](https://news.dmzj.com/article/65250.html)
> 概要: 今天（11月1日）是万圣节，为大家准备了从游戏、杂志、同人本里选出的20张与万圣节相关的图片。
### [动画《精灵宝可梦》新系列预告片与新宣传图公开](https://news.dmzj.com/article/65256.html)
> 概要: TV动画《精灵宝可梦》的新系列公开了新的宣传图与预告视频。在新的宣传片中，收录了由After the Rain的OP主题曲《1·2·3》。
### [《火影忍者》实力强劲的组合，木叶三忍和金角银角哪队更厉害？](https://new.qq.com/omn/20191101/20191101A0NC2F00.html)
> 概要: 《火影忍者》中忍者都有分等级的，大概分为下忍、中忍、上忍、暗部、影级再上就是神级了。而每个人在提升实力的过程中，不可能都像鸣人这样进步飞快，有的因为自身能力或者天赋的限制，最多只能到达上忍，但是他们……
### [国产动画中的“四大天王”丝毫不比日漫差，国漫雄起](https://new.qq.com/omn/20191101/20191101A0NH6W00.html)
> 概要: 日漫中有一个称号，那就是“四大天王”，它是日漫中当之无愧的NO.1，凡是被授予“四大天王”称号的日漫，就都是日漫中的无可争议的佼佼者。如今日漫新“四大天王”大家肯定都有所耳闻，无论是《鬼灭之刃》还是……
# 科技
### [使用 Graphviz 绘画 UML 图](https://www.ctolib.com/miloyip-graphvizuml.html)
> 概要: 使用 Graphviz 绘画 UML 图
### [LaTeX_OCR_PRO数学公式识别增强版：中英文手写印刷公式、支持初级符号推导](https://www.ctolib.com/LinXueyuanStdio-LaTeX_OCR_PRO.html)
> 概要: LaTeX_OCR_PRO数学公式识别增强版：中英文手写印刷公式、支持初级符号推导
### [超实用新手指南！零基础如何自学UI设计？](https://www.tuicool.com/articles/QZrmyam)
> 概要: 第一点：学习准备——启蒙  学习一项技能，尤其是已经有一定沉淀并在各行各业有广泛应用的技能，就一定要对它先有充分的认知。在开始正式学习前，你需要花足够的经历去了解和查阅它的起源、发展、应用、未来......
### [阿里巴巴 2020 财年 Q2 财报：总营收 1190.2 亿元，超预期 2%](https://www.tuicool.com/articles/Ib2YNrQ)
> 概要: 11 月 1 日，阿里巴巴发布截至 2019 年 9 月 30 日的 2020 财年第二财季财报。  财报显示，阿里巴巴第二季度营收 1190.2 亿元人民币，市场预估 1166.9 亿元人民币， 去......
### [Apache ServiceComb 服务网格与微服务开发框架融合实践](https://www.tuicool.com/articles/y6Vv2qN)
> 概要: 读者需具备基础：  已对微服务有一定实践经验，使用过一种以上的微服务开发框架。  对 Service Mesh 有一定理解，知道他是什么，运作机制，可以通过我过去的分享来了解Service Mesh ......
### [语音版BERT？滴滴提出无监督预训练模型，中文识别性能提升10%以上](https://www.tuicool.com/articles/3u6juyJ)
> 概要: 论文链接：https://arxiv.org/pdf/1910.09932.pdf  Masked 预测编码（MPC）  当前的工业端到端自动语音识别（automatic speech recogni......
### [B2B企业，如何向服务要利润？](https://www.tuicool.com/articles/FJvm6vE)
> 概要: 随着中国增量市场红利的逐渐消亡，市场份额的扩大和客户数的增长变得非常艰难而且珍贵。B2B企业越来越强调客户的终身价值，通过更开放的倾听客户抱怨、管理客户的预期、处理客户的问题、了解促使他们转向竞争对手......
### [【JS 口袋书】已整理完毕,下个系列【TS 演化史】](https://www.tuicool.com/articles/be2uuiN)
> 概要: 阿里云服务器很便宜火爆，今年比去年便宜，10.24~11.11购买是1年86元，3年229元，可以点击 下面链接进行参与：  https://www.aliyun.com/1111/2...  【JS......
### [日本推出了一款木头做的汽车](https://www.tuicool.com/articles/bMJfAjV)
> 概要: 为了可持续发展的未来，日本已经不满足于电动汽车了。  日本环境部（Ministry of Environment）正在探索汽车设计的下一个发展趋势——  木制汽车。  在日本环境部的支持下，由京都......
### [最前线 | “京喜”正式接入微信一级入口 “京喜日”24小时卖货6000万件](https://www.tuicool.com/articles/QR7jqyz)
> 概要: 微信购物一级入口正式完成更换。  10月31日，京东旗下社交电商平台“京喜”正式接入微信一级入口，替换之前“京东购物”页面。在相继接入手机QQ、京喜小程序、京喜APP、京喜M站、粉丝群后，京东新社交电......
# 小说
### [同心共筑中国梦](http://book.zongheng.com/book/881029.html)
> 作者：王炳林

> 标签：评论文集

> 简介：本书系统、深入地阐释了中国梦的内涵、中国梦与中国道路、中国梦的实现路径和实现中国梦与城镇化道路、破除城乡壁垒、国企监管、收入分配等经济体制改革的内在联系，科学总结出在新形势下实现中华民族伟大复兴的中国梦的政策选择和努力方向。指出实现中国梦必须走中国道路、弘扬中国精神、凝聚中国力量，这对广大领导干部在改革攻坚期进一步深化重要领域改革、促进中国梦实现具有重要的指导意义。

> 章节末：后记

> 状态：完本
### [幻山](http://book.zongheng.com/book/161466.html)
> 作者：四角钱

> 标签：武侠仙侠

> 简介：幻山望山际兮云似柔，漫路长兮莫无犹天真道兮为谁修，无比山兮我心愁 ………………………………………………………………………………

> 章节末：反反复复，一切都会回来

> 状态：完本
# 论文
### [diffGrad: An Optimization Method for Convolutional Neural Networks](https://paperswithcode.com/paper/diffgrad-an-optimization-method-for)
> 日期：12 Sep 2019

> 标签：IMAGE CATEGORIZATION

> 代码：https://github.com/shivram1987/diffGrad

> 描述：Stochastic Gradient Decent (SGD) is one of the core techniques behind the success of deep neural networks. The gradient provides information on the direction in which function has the steepest rate of change.
### [Two-stage Image Classification Supervised by a Single Teacher Single Student Model](https://paperswithcode.com/paper/two-stage-image-classification-supervised-by)
> 日期：26 Sep 2019

> 标签：IMAGE CLASSIFICATION

> 代码：https://github.com/zengsn/research

> 描述：The two-stage strategy has been widely used in image classification. However, these methods barely take the classification criteria of the first stage into consideration in the second prediction stage.
